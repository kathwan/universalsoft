## Acciones Preliminares
_Primero y antes que nada debe proporcionar la direccion Ip del servidor origen
Ejemplo_

```

UniversalDomain=https://api.universalrace.net/
IP=xxx.xxx.xxx.xxx 
```
### Consultar monedas disponibles y obtener el id de cada moneda
```
POST	https://[universalDomain]/bet_api.php
Body Params: {
 accion:'monedas_API'
}

Response:
{ 
 id:numeirc,  --> necesario para crear el usuario
 abre:string,  --> Abreviatura de la moneda
 moneda:string, --> nombre o descripcion de la moneda
 pais, -->  pais a quien pertenece la moneda,
 simbolo, --> simbolo o identificador de la moneda
 cod_pais --> codigo pais
}

```
### Crear Usuario
_Es necesario crear el usuario, entes de realizar cualquier operación_
```
POST	https://[universalDomain]/bet_api.php
Body Params: {
 accion:'API_crear_usuario',
 user_n:string(20),
 nombre: string(20),
 pais: int (numero telefónico internacional, ejm 58 venezuela,51 Peru)
 moneda: integer
 id:string(50)  (Es un número único o password para cada usuario, debe contener minimo 15 caracteres alfanumerico )
}

//El usuario fue creado
Response:
{ 
 resp:'ok', 
 id:numeirc,  
 Msg:'Usuario Creado'
}

//Si el Usuario ya Existe
{ 
 resp:'Err', 
 id:integer,  (1=El usuario exite) (3=Ip No registrado)
 Msg: string ("Usuario ya existe")(Ip no Encontrado)
}
```

### Iniciar Session
```
POST	https://[universalDomain]/ betapi/bet_api.php
Body Params: {
 accion:'API_Login',
 user_n:string(20),
 id:string(50)   (Id o password con el cual creo el usuario)
}
Response:
Respuesta Afirmativa
{
  resp:'OK',
  token: string,
}
//Abrir el enlace:

token= es el token proporcionado en el inicio de session
saldo= es el saldo actual del cliente
*** El enlace debe aperturar dentro de un iframe en caso contrario mostrara una pagina en blanco
https://universalrace.net/new/cliente_api/a_home.php?tk={token}&ba={saldo}


//Realiza Jugada

//Cuando el jugador realiza una jugada se realiza un request al (cliente_Api) 

POST	https://[yourserver_ip]/[you_end_point]

Body Params	{
 accion:' venta_tck_API',
 token:string,
 id_tran: string,
 monto: float
}
Response:
Negativa (El jugado no tiene Saldo)
{
resp:'fo',
id:'fo', 
Mgs:string (*Motivo del rechazo)
}

//Afirmativa
{
resp:'ok',
balance:float, //El Balance actual del cliente
saldo:float,   //El Saldo que tenía antes de realizar la jugada
idtx: string  //identificador único o Id del proceso por el cliente API para su control, este Valor se retornara al cliente api para que culmine el proceso *OPCIONAL
}
//Al ser afirmativa la respuesta se procede a guardar la jugada realizada y se le informa al cliente Api la culminación del proceso

POST	https://[yourserver_ip]/[you_end_point]

Body Params	{
 accion:'final_tck_API',
 serial:string,
 monto:float 
 id_tran: string,
 hora: format unix o epoch
 id_us=numeric
 idtx:string (el idtx proporcionado por el cliente) 
}

Response:
{
resp:'ok',
serial:string, 
idtx:string,  
id_us: numeric
}
```

### Notificacion Cambio de estatus Tickets
// 1.0 Se envia notificacion al End Point que tiene nuevos tickets

GET	https://[yourserver_ip]/[you_end_point]?accion=status_tck&id=XXXXXXXXXXXXX

// 1.1 El cliente Api debe Realizar un Request al servidor

```
POST	https://[universalDomain]/ betapi/bet_api.php
Body Params: {
 accion:"status_tck_API",
 id_operacion:id   (El id que se envio en el paso 1.0 )
}
Response:
Respuesta Afirmativa
{
  resp:'OK',
  data:[ 
        {
            id: Numeric,
            id_us:numeric,  (Id Usuario UniversalHorse)
            monto:float,     
            estatus: Character ("G" Ganador, "D" Devolucion Jugada, "A" Anulado),
            fecha:numeric (Formato Unix o Epoch),
            serial:string (Serial del ticket) 
        }
      ]
}

// 1.2 El cliente Api confirma la recepcion 

```
POST	https://[universalDomain]/ betapi/bet_api.php
Body Params: {
 accion:"confirma_status_API",
 id_operacion:id   (El id que se envio en el paso 1.0 )
}

Response:
{ 
 resp:'ok', 
 id:numeirc,  
 Msg:'Confirmado'
}


### Ver detalle de un Ticket

```
POST	https://[universalDomain]/betapi/bet_api.php
Body Params: {
 accion:"ver_ticket_API",
 serial:string   
}

Response:
{ 
 resp:'ok', 
 serial:string,  
 total:float
 moneda:int
 tck:[{
     apuesta:character, ("w" win, "p" place,"s" show,"ex" exacta,"tr" trifecta,"su" superfecta,"Pk2" Pick2, "Pk3" Pick3
     pista:string,
     monto:float,
     tipo_estatus: chracter ("G" Ganador,"P" Perdida,"D" Devolucion,"A" Anulada,"J" En juego)
     dividendo:float ("Si es una apuesta SP, o Dividendo Fijo),
     carrera:integer,
     caballo: string (Caballo o Caballos involucrados en la apuesta)
     }]   
}

### Tipos de modedas disponibles

```
POST	https://[universalDomain]/betapi/bet_api.php
Body Params: {
 accion:"monedas_API"
}

Response:
[{ 
 id:integer,  Id de la moneda
 abre:string,  Abreviatura de la modena
 moneda:string,  Descripcion del tipo de la moneda
 simbolo:string  Simbolo que representa la moneda
 pais:string  Abreviatura del pais
 cod_pais:numeric cod pais segun el codigo telefonico internacional
}]

### Informacion Adional Sobre la Realizacion de la Apuesta

* El cliente Api es quien valida si el jugador tiene o no saldo para realizar la jugada.

* La consulta y finalización de la jugada se realiza en dos pasos:
* 1.- Se hace una llamada al endpoint del cliente con el monto de la jugada para validar si tiene saldo suficiente para la jugada.
* 2.- De ser afirmativo se guarda el tck y se envia otro reuqest al end point para la confirmacion de que fue generado el tck.

```
//Respuesta 
//El tiempo de respuesta por parte del cliente debe ser menor a 1.5 segundos de lo contraior se anula la jugada
//Debe devolver si el cliente tiene o no saldo para ejecutar o culminar la jugada.
//El proceso de validacion de si el cliente que realiza la jugada tiene saldo o no, queda de parte del cliente_api

//Solicitud POST al end point del cliente
accion = "venta_tck_API"   //Accion que se va a ejecutar
id_tran =  "XXXXXXXXXXX"   //Id unico del UniversalHorse
monto =  0000000000        //Monto total de la jugada 

//Respuestas que debe brindar el cliente_API

//Respuesta Afirmativa json  (El cliente tiene saldo, El cliente API Ya debio descontar el saldo)

"resp":"ok"               //El cliente tiene saldo para realizar la jugada
"balance":""              //El Balance actual del cliente
"saldo_ant":""            //El Saldo que tenia antes de realizar la jugada
"idtx":"alkks87238493"    //Identificador unico o Id del proceso por el cliente API para su control, este Valor se retornara al cliente api para que culmine el proceso *OPCIONAL

// Al ser afirmativo se procede a guardar el tck y se le envia la respuesta de confirmacion al cliente por API 

// Solicitud  POST al end point del cliente  

accion="final_tck_API" //La accion que se esta tomando o funcion
serial = "XXXXXXXXX"  //El Serial unico de cada tck
monto=0000000.00      //El total de tck
id_tran="XXXXX"       //Id de la transaccion o id_tck de UniversalHorse enviado en la consulta anterior
hora = 1234567899     // La hora en formato UNIX o epoch
id_us = 0000000       //El Id del usuario registrado en UniversalHorse
idtx = "XXXXXXXXX"    //El Id enviado por el cliente API para su control

//Respuesta de Confirmacion JSON
"resp":"ok"              
"serial":""              
"idtx":""            
"id_us":""

//Respuesta Negativa o No tiene saldo json
"resp":"fo"                 //fo  el cliente no tiene saldo suficiente
"Id":"fo"                   //El id o identificador del error es fo
"Msg":"Saldo Insuficiente"  //Es opcional, puede enviar cualquier texto   
//Al ser negativa la respuesta no se guarda el tck y se anula la jugada, no se envia confirmación
```
