##***Creación de Pagina Novobets Casino a nivel Frontend***

###***Importación de dependencias***

####***Importación de Google Icon Font***

En el documento HTML, en la sección del encabezado `<head>` se realizan las importaciones respectivas de Google Icon Font como se muestra:

```
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
```

####***Importación de estilos CSS***

En la división `<head>`, se proceden con la importación de los archivos CSS, los cuales son útiles para darle estilo a la página. 

```
<link type="text/css" rel="stylesheet" href="assets/css/style.css">
<link type="text/css" rel="stylesheet" href="assets/css/bootstrap.min.css">
<link type="text/css" rel="stylesheet" href="assets/css/owl.carousel.min.css">
<link type="text/css" rel="stylesheet" href="assets/css/style-casinov2.css">
<link type="text/css" rel="stylesheet" href="assets/css/animate.css">
<link type="text/css" rel="stylesheet" href="assets/css/geral.css">
```

####***Agregación de etiquetas meta***

En este caso, se agrega una etiqueta meta para poder indicar al navegador que la pagina esta optimizada para dispositivos móviles.

```
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
```

###***Creación del encabezado de la pagina web***

####***Encabezado de navegación***

Dentro de la sección de encabezado `<section class="nav-header">`, se coloca el logo de la empresa, un botón de registro y un formulario de inicio de sesión.

```
<div class="d-flex-center bg-header container pt-3">
    <div class="w-20">
        <img src="assets/img/logo-169px-66px.png" alt="">
    </div>
    <div class="w-40 text-right">
        <button class="btn-novo">!Registrese ahora mismo!! <i class="icon-menor">18+</i></button>
    </div>
    <div class="d-flex-right w-40 text-right">
        <input type="text" class="btn-input-login" name="" id="" placeholder="Usuario">
        <input type="text" class="btn-input-login" name="" id="" placeholder="Contraseña">
        <button class="btn-login">INICIO DE SESION</button>
    </div>
</div>
```

####***Subencabezado***

El subencabezado constituye la caja de búsqueda, las distintas opciones de navegación, redes sociales y selección de idioma.

#####***Caja de busqueda***

La caja de búsqueda se construye como un elemento de una lista, la cual contiene ciertos elementos del subencabezado.

```
<li class="searchmy">
    <div class="palillo"></div>
    <form class="p-relative" style="margin-top: 0.2rem;" action="">
        <input type="search" class="btn-input-login btn-search-top" placeholder="Buscar...">
        <button class="btn-search-peq"><i class="fa fa-search"></i></button>
    </form>
    <div class="palillo2"></div>
</li>
```

#####***Opciones de navegación***

En este caso, cada opción de navegación es un elemento de la lista como se muestra:

```
<li onclick="ActivarTab(this);" class="NavbarTab"><div class="palillo"></div>Deportes <div class="palillo2"></div></li>
<li onclick="ActivarTab(this);" class="NavbarTab"><div class="palillo"></div>En Vivo<div class="palillo2"></div></li>
<li onclick="ActivarTab(this);" class="NavbarTab active"><div class="palillo"></div>Casino<div class="palillo2"></div></li>
<li onclick="ActivarTab(this); ActivarFiltros('block');" class="NavbarTab"><div class="palillo"></div>Casino En vivo<div class="palillo2"></div></li>
<li onclick="ActivarTab(this); ActivarHipica('none', 'block');" class="NavbarTab"><div class="palillo"></div>Hipica<div class="palillo2"></div></li>
```

Cada opción de navegación permite visualizar diferentes cosas en la página. Para ello, a partir de la propiedad `onclick`, se asignan funciones para cada opción. Cuando se ejecutan estas funciones, permiten mostrar y ocultar diferentes elementos en la página.  

```
function ActivarFiltros(display_val) { 
    document.getElementById("filtro-botones-envoltura-id").style.display = display_val; 
}
function ActivarHipica(display_val, display_val2) {
    document.getElementById("filtro-tipo-section-id").style.display = display_val;
    document.getElementById("iframehipica-id").style.display = display_val2;
    document.getElementById("sliders-div-id").style.display = display_val;
    for (i = 0; i < document.getElementsByClassName("body-casino").length; i++) { 
        document.getElementsByClassName("body-casino")[i].style.display = display_val;
    }
}
function ActivarTab(obj) {
    ActivarFiltros("none"); ActivarHipica("block", "none");
    navbartabs = document.getElementsByClassName("NavbarTab");
    for (i=0; i < navbartabs.length; i++) { 
        navbartabs[i].classList.remove("active"); 
    } 
    obj.classList.add("active");
}
```

#####***Redes sociales***

Los enlaces de las redes sociales respectivas son colocados dentro de un contenedor, el cual se alinea a la derecha como se muestra: 

```
<div class="w-5 text-right p-relative">
    <a href="javascript:void()" class="socialmedia"><i class="fab fa-instagram"></i></a>
    <a href="javascript:void()" class="socialmedia"><i class="fab fa-facebook-square"></i></a>
    <div class="palillo"></div>
    <div class="palillo2"></div>
</div>
```

#####***Selección de idioma***

La selección del lenguaje se implementa a partir de una lista con todas las opciones de idioma disponibles.

```
<div class="w-10 p-relative2">
    <div id="select-flat" class="d-flex-center select-novo-flat">
        <div class="w-30 text-center flat-novo"><img class="img-fluid" src="assets/img/paises/es.png" alt=""></div>
        <div class="w-40 text-center"><span>ES</span></div>
        <div class="w-30 text-right arrow-novo"><i class="fa fa-chevron-down"></i></div>
    </div>
    <ul id="select-flat-ul" class="list-flat" style="display: none;">
        <li class="d-flex-center">
            <div class="w-30 text-center flat-novo"><img src="assets/img/paises/en.png" alt=""></div>
            <div class="w-40 text-center"><span>EN</span></div>
            <div class="w-30 text-right"></div>
        </li>
        <li class="d-flex-center">
            <div class="w-30 text-center flat-novo"><img src="assets/img/paises/es.png" alt=""></div>
            <div class="w-40 text-center"><span>ES</span></div>
            <div class="w-30 text-right"></div>
        </li>
        <li class="d-flex-center">
            <div class="w-30 text-center flat-novo"><img src="assets/img/paises/de.png" alt=""></div>
            <div class="w-40 text-center"><span>DE</span></div>
            <div class="w-30 text-right"></div>
        </li>
    </ul>
</div>
```

###***Banner y submenu de juegos***

En esta parte, se implementa el banner, los indicadores de Jackpot y la barra de selección de juegos.

####***Banner***

Para la construcción del banner, se planteó este como un carrusel. Ello con el objetivo de obtener un banner interactivo y animado.

```
<div class="owl-carousel">
    <div class="item">
        <video class="slider-video" width="100%" preload="auto" loop="" autoplay="" muted="" style="visibility: visible; width: 100%;">
            <source src="assets/img/slider1.mp4" type="video/mp4">
        </video>
    </div>
    <div class="item">
        <img class="img-fluid" src="assets/img/bannercasino2.png" atl="promo"></img>
    </div>
    <div class="item">
        <img class="img-fluid" src="assets/img/bannercasino3.png" atl="promo"></img>
    </div>
</div>
```

Para animar el carrusel del banner, se hizo uso de JQuery como se muestra en el siguiente código:

```
var owl = $('.owl-carousel');
owl.owlCarousel({
    margin: 0, loop: true,
    items:1, autoplay:true,
    autoplayTimeout:5000, dots:false
})
```

####***Submenu de juegos***

El submenú de juegos contiene información respecto a indicadores de jackpot y una barra de selección de juegos.

#####***Indicadores de Jackpot***

Cada indicador de jackpot está envuelto dentro de un contenedor. En ese sentido, se tiene una lista de contenedores de forma horizontal.

```
<div class="jackpot-div">
    <div class="jackpot-list">
        <span class="j-p-title">JACKPOT : </span>
        <span class="j-p-sub">$6.623.150,25</span>
    </div>
    <div class="jackpot-list">
        <img class="img-fluid" src="assets/img/aog.png" alt="">
        <span class="j-p-sub">$75.256,05</span>
    </div>
    <div class="jackpot-list">
        <img class="img-fluid" src="assets/img/aog.png" alt="">
        <span class="j-p-sub">$1.089.110,10</span>
    </div>
    <div class="jackpot-list">
        <img class="img-fluid" src="assets/img/aog.png" alt="">
        <span class="j-p-sub">$58.251,73</span>
    </div>
</div>
```

#####***Barra de selección de juegos***

La barra de selección de juegos incluye distintos apartados como el formulario de búsqueda, el selector de tipo de despliegue, cuadro de lista de proveedores, cuadro de lista de juegos y lista de favoritos. A continuación, se muestra un extracto de código donde se construye el formulario de búsqueda.

```
<form class="search-div search-casino">
    <input id="searchcasino" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
    <button class="btn btn-outline-danger btn-search my-2 my-sm-0" type="submit"><i class="fas fa-search" aria-hidden="true"></i></button>
</form>
```

###***Sección de Filtros***

En esta sección, se constituyen los filtros por tipo. Estos filtros solo se muestran cuando esta activada la opción casino en vivo. Se trata de un grupo de botones alineados de forma horizontal. 

```
<section class="filtro-tipo-section" id="filtro-tipo-section-id">
    <div style="display:none;" class="filtro-botones-envoltura" id="filtro-botones-envoltura-id">
        <button type="button" class="btn shadow-none filtro-boton filtro-boton-all" data-filter="all-online">
            <i class="fas fa-coins filtro-boton-icono"></i>
            <h6 class="filtro-boton-label">Todos</h6>
        </button>
        <button type="button" class="btn shadow-none filtro-boton" data-online="online1">
            <i class="fab fa-empire filtro-boton-icono"></i>
            <h6 class="filtro-boton-label">Roulette</h6>
        </button>
        <button type="button" class="btn shadow-none filtro-boton" data-online="online2">
            <i class="fab fa-black-tie filtro-boton-icono"></i>
            <h6 class="filtro-boton-label">BlackJack</h6>
        </button>
        <button type="button" class="btn shadow-none filtro-boton" data-online="online3">
            <i class="fas fa-dharmachakra filtro-boton-icono"></i>
            <h6 class="filtro-boton-label">MegaWheel</h6>
        </button>
        <button type="button" class="btn shadow-none filtro-boton" data-online="online4">
            <i class="fas fa-mask filtro-boton-icono"></i>
            <h6 class="filtro-boton-label">Baccarat</h6>
        </button>
        <button type="button" class="btn shadow-none filtro-boton" data-online="online5">
            <i class="fas fa-dice filtro-boton-icono"></i>
            <h6 class="filtro-boton-label">Sicbo</h6>
        </button>
        <button type="button" class="btn shadow-none filtro-boton" data-online="online6">
            <i class="fab fa-centos filtro-boton-icono"></i>
            <h6 class="filtro-boton-label">Loteria</h6>
        </button>
    </div>
</section>
```

Al presionarse uno de los botones, los juegos que se muestran en la página deberían filtrarse de acuerdo al tipo seleccionado. Para lograr ello, se implementó el siguiente código haciendo uso de JQuery.

```
$(".filtro-boton").click(function(){
    $("#ys_secSLIV .col-item").hide(200);
    $("#ys_secSLIV .col-item[data-filter=" + $(this).data('online') + "]").delay(200).show(200);
});
$(".filtro-boton-all").click(function(){
    var value = $(this).attr('data-filter');
    if(value == "all-online"){
        $('.filter-all-online').delay(200).show(200);
    }
});
```

###***Sección de Juegos***

En esta parte, se muestran los juegos disponibles en el sistema. Cuando el usuario haga clic sobre uno de los contenedores respectivos, el juego se abrirá en una modal para que el usuario pueda acceder a este. A continuación, se muestra el código HTML para la implementación del contenedor de un juego.

```
<div class="col-item sm filter-all-online" data-filter="online1">
    <div class="row no-gutters">
        <div class="col-12 effect-padd2">
            <div class="effect-padd">
                <div class="new-text-div">
                  <p class="title-game">1st of the Irish</p>
                </div>
                <div class="game_inner game-sm my-effect" style="background-image:url(https://www.lobbyuniversalsoft.net/images/games/redrake-1st-of-the-irish.png)">
                    <div class="fav_holder">
                        <div class="like-game">
                            <i class="far fa-heart" style="display:block" id="redrake-1st-of-the-irish" aria-hidden="true"></i>
                            <span id="f-redrake-1st-of-the-irish" class="span-casino">0</span>
                        </div>
                    </div>
                    <div class="game_overlay">
                        <div class="play">
                            <div class="circle-play"></div>
                            <!-- URL DEL JUEGO -->
                            <a href="javascript:void(0)" onclick="abrir_modal('https://test.apiuniversalsoft.com/api/launch?gameid=redrake-1st-of-the-irish&amp;p=ms&amp;b=Red Rake&amp;m=wb&amp;sessionid=d45792796a018361c64f1f5da83324bca42dc939de95cb5e79250438fd7f961e342a1a6d0a88d23e3ac1bdbef7f402c9c8953152a7697fb0e2491dc55e6be33c')"><i class="fas fa-play" aria-hidden="true"></i></a>
                            <!-- FIN DE URL DEL JUEGO -->
                        </div>
                        <div class="game_title"><img class="img-fluid" src="https://www.lobbyuniversalsoft.net/images/brands/Red Rake.png" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
```

Para lograr el efecto de apertura de la modal, se implementa una función llamada `abrir_modal(href)`. En el siguiente extracto de código, se muestra la implementación de dicha función.

```
function abrir_modal(href) {
    const modal_tag = '.modal-slot';
    $(modal_tag).toggleClass('active');
    if( $(modal_tag).find("#iframe").length ) {
        $(modal_tag).find("#iframe").remove();
    }
    $(modal_tag).find('.body-slot').append("<iframe src='"+href+"' id='iframe' frameborder='0' style='flex: 1 1 0%; width: 100%; height: 100%;'></iframe>");
    $('.modal-prog').click(function() {
        if($(modal_tag).find("#iframe").length) {
            $(".modal-slot").removeClass('active');
            $(".modal-slot").find("#iframe").remove();
        }
    });
}
```



















