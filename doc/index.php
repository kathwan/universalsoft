<?php 
session_start();
//session_destroy();
$pass = $_POST["pass"]??false;
$is_valid_pass = false;
if($pass && $pass == '1versa1$0tfd0c') {$_SESSION["pass"] = $pass; $is_valid_pass=true; }
?>
<!DOCTYPE html>
<html lang="en">
<?php
if(! isset($_SESSION["pass"]) ){
?>
<div style="text-align:center; padding:20px;">
<div style="padding:50px; margin:auto; border:1px solid #ccc; border-radius:5px; display:inline-block;">
<h4>Acceso Documentación</h4>
<form method="POST"> <input name="pass" placeholder="password" type="text"> <input type="submit" value="Entrar" > </form>
<?php if($pass && !$is_valid_pass): ?>
<small> Clave Incorrecta </small>
<?php endif; ?>
</div>
</div>

 
<?php }else{ ?>
<!-- HTML for static distribution bundle build -->
  <head>
    <meta charset="UTF-8">
    <title>Swagger UI</title>
    <link rel="stylesheet" type="text/css" href="./swagger-ui.css" />
    <link rel="icon" type="image/png" href="./favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="./favicon-16x16.png" sizes="16x16" />
    <style>
      html
      {
        box-sizing: border-box;
        overflow: -moz-scrollbars-vertical;
        overflow-y: scroll;
      }

      *,
      *:before,
      *:after
      {
        box-sizing: inherit;
      }

      body
      {
        margin:0;
        background: #fafafa;
      }
    </style>
  </head>

  <body>
    <div id="swagger-ui"></div>

    <script src="./swagger-ui-bundle.js" charset="UTF-8"> </script>
    <script src="./swagger-ui-standalone-preset.js" charset="UTF-8"> </script>
    <script>
    window.onload = function() {
      const ui = SwaggerUIBundle({
        url: "./swagger.yaml",
        dom_id: '#swagger-ui',
        deepLinking: false,
        presets: [
          SwaggerUIBundle.presets.apis,
          //SwaggerUIStandalonePreset
        ],
        supportedSubmitMethods: [tryItOutEnabled=false],
        plugins: [
          //SwaggerUIBundle.plugins.DownloadUrl
        ],
        //layout: "StandaloneLayout"
      });
      // End Swagger UI call region

      window.ui = ui;
    };
  </script>
  </body>
<?php } ?>
</html>