<?php 
    
    if(isset($_POST['enviar'])){
        $destino = "ventas@universalsoft.net";
        $ys_headers = "From: ".$_POST["nombre"]." <".$_POST["correo"].">";
        $nombre = $_POST["nombre"];
        $correo = $_POST["correo"];
        $telefono = $_POST["telefono"];
        $mensaje = $_POST["mensaje"];
        $contenido = "Nombre y Apellido: " . $nombre . "\nCorreo Electronico: " . $correo . "\nTeléfono: " . $telefono . "\nMensaje: " . $mensaje;
        $mail = mail($destino,"Pagina UniversalSoft Contactanos", $contenido,$ys_headers);
        header('Location: enviar.php');
        // header('Location: '.$_POST["origen"].'?Univ');
        return $mail;
    }
    
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Universal Soft</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/x-icon" href="images/favicon.png">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">
    <link rel="stylesheet" href="fonts/fontawesome/css/all.css">
    <link rel="stylesheet" href="css/style.css?=v2">

  </head>
  <style>
      .modal2 {
        background: #ffffff;
        border-radius: 4px;
        padding: 48px 0 24px 0;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
    }

    .modal--error {
    .modal__title {
        color: #d91633;
    }
    .modal__btn {
        background-color: #d91633;
    }
    }

    .modal__container {
    max-width: 350px;
    margin: 0 auto;
    }

    .animation__container {
    margin: 0 auto;
    position: relative;
    height: 50px;
    margin-bottom: 48px;
    }

    .mail,
    .check,
    .bubble,
    .warning {
    position: absolute;
    left: 50%;
    transform: translateX(-50%) translateY(-50%);
    }

    .mail,
    .check,
    .warning {
    top: 50%;
    }

    .bubble {
    top: 44%;
    transform-origin: center;
    display: flex;
    justify-content: center;
    align-items: center;
    animation-delay: 0.95s;
    animation-name: Bubbles;
    animation-duration: 0.5s;
    animation-iteration-count: 1;
    opacity: 0;
    }

    .mail {
    animation-delay: 0.5s;
    animation-name: mail-anim;
    animation-duration: 2s;
    animation-iteration-count: 1;
    opacity: 0;
    }

    .check-stroke {
    stroke-dasharray: 150;
    animation-name: check-anim;
    animation-duration: 3s;
    animation-iteration-count: 1;
    }

    .warning {
    rect,
    path {
        stroke: #d91633;
        stroke-dasharray: 200;
    }
    rect {
        animation-name: warning-anim;
        animation-duration: 2s;
        animation-iteration-count: 1;
    }
    path {
        animation-name: warning-anim;
        animation-duration: 6s;
        animation-iteration-count: 1;
    }
    circle {
        fill: #d91633;
        animation-name: appear;
        animation-duration: 3s;
        animation-iteration-count: 1;
        transform-origin: center;
    }
    }

    .modal__container__content-container {
    margin-bottom: 40px;
    }

    .modal__title {
    font-size: 1.5rem;
    color: #219b95;
    font-weight: 300;
    margin-bottom: 24px;
    text-align: center;
    }

    .modal__list {
    padding-left: 12px;
    list-style: none;
    li {
        position: relative;
        &:before {
        content: "•";
        position: absolute;
        width: 8px;
        height: 8px;
        left: -14px;
        color: #219b95;
        top: 2px;
        }
    }
    }

    .modal__text {
    margin-bottom: 16px;
    }

    .modal__text--bold {
    font-weight: 500;
    }

    .modal__btn {
    background-color: #219b95;
    color: #fff;
    text-decoration: none;
    width: 150px;
    display: block;
    text-align: center;
    padding: 16px 0;
    border-radius: 3px;
    font-weight: 400;
    }

    @keyframes Bubbles {
    0% {
        transform: translateX(-50%) translateY(-50%) scale(1.5);
    }
    10% {
        opacity: 1;
    }
    100% {
        opacity: 0;
        transform: translateX(-50%) translateY(-50%) scale(2.5);
    }
    }

    @keyframes mail-anim {
    0% {
        opacity: 1;
        transform: translateX(-50%) translateY(-50%) scale(0);
    }
    20% {
        opacity: 1;
        transform: translateX(-50%) translateY(-50%) scale(1.1);
    }
    30% {
        opacity: 1;
        transform: translateX(-50%) translateY(-50%) scale(1);
    }
    40% {
        opacity: 1;
        transform: translateX(-50%) translateY(-50%) scale(1);
    }
    45% {
        opacity: 1;
        transform: translateX(-55%) translateY(-50%) scale(1);
    }
    100% {
        opacity: 0;
        transform: translateX(200%) translateY(-50%) scale(1);
    }
    }

    @keyframes check-anim {
    0% {
        stroke-dashoffset: 150;
    }
    66% {
        stroke-dashoffset: 150;
    }
    100% {
        stroke-dashoffset: 0;
    }
    }

    @keyframes warning-anim {
    0% {
        stroke-dashoffset: 200;
    }
    25% {
        stroke-dashoffset: 200;
    }
    100% {
        stroke-dashoffset: 0;
    }
    }

    @keyframes appear {
    0% {
        transform: scale(0);
    }
    70% {
        transform: scale(0);
    }
    85% {
        transform: scale(1.1);
    }
    100% {
        transform: scale(1);
    }
    }
  </style>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark no-desktop">
        <a class="navbar-brand" href="index.php"><img class="d-inline-block align-top img-fluid" src="images/logo.png" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menucontrol" aria-controls="menucontrol" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="menucontrol">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <div class="header-line">
                    <a class="header-span" href="about.php">
                        <span>ACERCA</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <div class="header-line">
                    <a class="header-span" href="services.php">
                        <span>SERVICIOS</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <div class="header-line">
                    <a class="header-span" href="product.php">
                        <span>PRODUCTOS</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <div class="header-line">
                    <a class="header-span" href="systems.php">
                        <span>SISTEMAS</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <div class="header-line">
                    <a class="header-span" href="news.php">
                        <span>NOTICIAS</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <div class="header-line">
                    <a id="#" class="header-span" href="#contact">
                        <span>CONTACTO</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <div class="header-line">
                    <select class="header-select" name="" id="">
                        <option value="es">ES</option>
                        <option value="eu">EN</option>
                    </select>
                </div>
            </li>
          </ul>
        </div>
    </nav>
    <nav class="header no-movil">
        <div class="header-div">
            <div class="header-w28">
                <a href="index.php"><img class="img-fluid" src="images/logo.png" alt=""></a>
            </div>
            <div class="header-line">
                <a class="header-span" href="about.php">
                    <span>ACERCA</span>
                </a>
            </div>
            <div class="header-line">
                <a class="header-span" href="product.php">
                    <span>PRODUCTOS</span>
                </a>
            </div>
            <div class="header-line">
                <a class="header-span" href="services.php">
                    <span>SERVICIOS</span>
                </a>
            </div>
            <div class="header-line">
                <a class="header-span" href="systems.php">
                    <span>SISTEMAS</span>
                </a>
            </div>
            <div class="header-line">
                <a class="header-span" href="news.php">
                    <span>NOTICIAS</span>
                </a>
            </div>
            <div class="header-line">
                <!-- <a id="#" class="header-span" href="#" data-toggle="modal" data-target="#modalNoticia"> -->
                <a id="#" class="header-span" href="#contact"> <span>CONTACTO</span>
                </a>
            </div>
            <div class="header-line">
                <select class="header-select" name="" id="">
                    <option value="es">ES</option>
                    <option value="eu">EN</option>
                </select>
            </div>
        </div>
    </nav>
    <section class="body-bg pt-5">
        <div class="modal2">
            <div class="modal__container">
                <div class="animation__container">
                <div class="mail">
                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="26" viewBox="0 0 32 26">
                        <path fill="#219B95" fill-rule="evenodd" d="M296.8,64 L271.2,64 C269.44,64 268.016,65.44 268.016,67.2 L268,86.4 C268,88.1600008 269.44,89.6 271.2,89.6 L296.8,89.6 C298.56,89.6 300,88.1600008 300,86.4 L300,67.2 C300,65.44 298.56,64 296.8,64 Z M296.8,86.4 L271.2,86.4 L271.2,70.4 L284,78.4 L296.8,70.4 L296.8,86.4 Z M284,75.2 L271.2,67.2 L296.8,67.2 L284,75.2 Z" opacity="1" transform="translate(-268 -64)" />
                    </svg>
                </div>

                <div class="bubble">
                    <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 32 32">
                        <path fill="#219B95" fill-rule="evenodd" d="M2.5,11 C2.77614237,11 3,10.7761424 3,10.5 C3,10.2238576 2.77614237,10 2.5,10 C2.22385763,10 2,10.2238576 2,10.5 C2,10.7761424 2.22385763,11 2.5,11 Z M10.5,3 C10.2238576,3 10,2.77614237 10,2.5 C10,2.22385763 10.2238576,2 10.5,2 C10.7761424,2 11,2.22385763 11,2.5 C11,2.77614237 10.7761424,3 10.5,3 Z M28.5,12 C28.2238576,12 28,11.7761424 28,11.5 C28,11.2238576 28.2238576,11 28.5,11 C28.7761424,11 29,11.2238576 29,11.5 C29,11.7761424 28.7761424,12 28.5,12 Z M17.5,4 C17.2238576,4 17,3.77614237 17,3.5 C17,3.22385763 17.2238576,3 17.5,3 C17.7761424,3 18,3.22385763 18,3.5 C18,3.77614237 17.7761424,4 17.5,4 Z M24,6 C23.4477153,6 23,5.55228475 23,5 C23,4.44771525 23.4477153,4 24,4 C24.5522847,4 25,4.44771525 25,5 C25,5.55228475 24.5522847,6 24,6 Z M5.5,7 C4.67157288,7 4,6.32842712 4,5.5 C4,4.67157288 4.67157288,4 5.5,4 C6.32842712,4 7,4.67157288 7,5.5 C7,6.32842712 6.32842712,7 5.5,7 Z M5.5,6.5 C6.05228475,6.5 6.5,6.05228475 6.5,5.5 C6.5,4.94771525 6.05228475,4.5 5.5,4.5 C4.94771525,4.5 4.5,4.94771525 4.5,5.5 C4.5,6.05228475 4.94771525,6.5 5.5,6.5 Z M28.5,22 C28.7761424,22 29,21.7761424 29,21.5 C29,21.2238576 28.7761424,21 28.5,21 C28.2238576,21 28,21.2238576 28,21.5 C28,21.7761424 28.2238576,22 28.5,22 Z M15.5,30 C15.2238576,30 15,29.7761424 15,29.5 C15,29.2238576 15.2238576,29 15.5,29 C15.7761424,29 16,29.2238576 16,29.5 C16,29.7761424 15.7761424,30 15.5,30 Z M2.5,21 C2.22385763,21 2,20.7761424 2,20.5 C2,20.2238576 2.22385763,20 2.5,20 C2.77614237,20 3,20.2238576 3,20.5 C3,20.7761424 2.77614237,21 2.5,21 Z M6,29 C5.44771525,29 5,28.5522847 5,28 C5,27.4477153 5.44771525,27 6,27 C6.55228475,27 7,27.4477153 7,28 C7,28.5522847 6.55228475,29 6,29 Z M24.5,29 C23.6715729,29 23,28.3284271 23,27.5 C23,26.6715729 23.6715729,26 24.5,26 C25.3284271,26 26,26.6715729 26,27.5 C26,28.3284271 25.3284271,29 24.5,29 Z M24.5,28.5 C25.0522847,28.5 25.5,28.0522847 25.5,27.5 C25.5,26.9477153 25.0522847,26.5 24.5,26.5 C23.9477153,26.5 23.5,26.9477153 23.5,27.5 C23.5,28.0522847 23.9477153,28.5 24.5,28.5 Z" transform="rotate(180 15.5 16)" />
                    </svg>
                </div>
                <div class="check">
                    <svg class="check-stroke" xmlns="http://www.w3.org/2000/svg" width="64" height="50" viewBox="0 0 64 50">
                        <polyline fill="none" stroke="#219B95" stroke-width="7" points="2.5 26.5 20.3 44.8 61.5 2.5" />
                    </svg>
                </div>
                </div>
                <div class="modal__container__content-container">
                <p class="modal__title">Tu mensaje se ha enviado correctamente</p>
                <p class="modal__text">En breve nuestro personal se contactara con usted.<br>También puede escribirnos a nuestros correos<br>y teléfonos personales :</p>
                <ul class="modal__list">
                    <li class="modal__list__element">ventas@universalsoft.com</li>
                    <!-- <li class="modal__list__element">+51-900-000-000</li> -->
                </ul>
                </div>
            </div>
            <a href="index.php" class="modal__btn">Regresar</a>
        </div>
    </section>

 <!-- loader -->
 <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-migrate-3.0.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.stellar.min.js"></script>
    <script src="js/jquery.animateNumber.min.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/jquery.timepicker.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/scrollax.min.js"></script>
    <script src="js/mystyles.js"></script>
    <script src="js/main.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js"></script> 
        
  </body>
</html>