<!DOCTYPE html>

<html lang="en">

  <head>

    <title>Universal Soft</title>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    

    <link rel="icon" type="image/x-icon" href="images/favicon.png">

    <link rel="stylesheet" href="css/animate.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/owl.carousel.min.css">

    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="stylesheet" href="css/magnific-popup.css">



    <link rel="stylesheet" href="css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="css/jquery.timepicker.css">

    <link rel="stylesheet" href="fonts/fontawesome/css/all.css">

    <link rel="stylesheet" href="css/style.css">

  </head>

  <body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark no-desktop">
        <a class="navbar-brand" href="index.php"><img class="d-inline-block align-top img-fluid" src="images/logo.png" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menucontrol" aria-controls="menucontrol" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="menucontrol">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <div class="header-line">
                    <a class="header-span" href="about.php">
                        <span>ACERCA</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <div class="header-line">
                    <a class="header-span" href="services.php">
                        <span>SERVICIOS</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <div class="header-line">
                    <a class="header-span" href="product.php">
                        <span>PRODUCTOS</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <div class="header-line">
                    <a class="header-span" href="systems.php">
                        <span>SISTEMAS</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <div class="header-line">
                    <a class="header-span" href="news.php">
                        <span>NOTICIAS</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <div class="header-line">
                    <a id="#" class="header-span" href="#contact">
                        <span>CONTACTO</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <div class="header-line">
                    <select class="header-select" name="" id="">
                        <option value="es">ES</option>
                        <option value="eu">EN</option>
                    </select>
                </div>
            </li>
          </ul>
        </div>
    </nav>
    <nav class="header no-movil">
        <div class="header-div">
            <div class="header-w28">
                <a href="index.php"><img class="img-fluid" src="images/logo.png" alt=""></a>
            </div>
            <div class="header-line">
                <a class="header-span" href="about.php">
                    <span>ACERCA</span>
                </a>
            </div>
            <div class="header-line">
                <a class="header-span" href="product.php">
                    <span>PRODUCTOS</span>
                </a>
            </div>
            <div class="header-line">
                <a class="header-span" href="services.php">
                    <span>SERVICIOS</span>
                </a>
            </div>
            <div class="header-line">
                <a class="header-span" href="systems.php">
                    <span>SISTEMAS</span>
                </a>
            </div>
            <div class="header-line">
                <a class="header-span" href="news.php">
                    <span>NOTICIAS</span>
                </a>
            </div>
            <div class="header-line">
                <!-- <a id="#" class="header-span" href="#" data-toggle="modal" data-target="#modalNoticia"> -->
                <a id="#" class="header-span" href="#contact"> <span>CONTACTO</span>
                </a>
            </div>
            <div class="header-line">
                <select class="header-select" name="" id="">
                    <option value="es">ES</option>
                    <option value="eu">EN</option>
                </select>
            </div>
        </div>
    </nav>

    <section class="body-bg">

        <div class="slider-bg about-us">

            <div class="animation-wrapper">

                <div class="particle particle-1"></div>

                <div class="particle particle-2"></div>

                <div class="particle particle-3"></div>

                <div class="particle particle-4"></div>

            </div>

            <div class="about-us-div">

                <p class="about-us-title wow animated fadeInRight">

                    el sistema de <br>apuestas <br>más 

                    <span class="about-span-title color-mix">confiable</span>

                    <span class="about-span-title">Sólido</span>

                    <span class="about-span-title">avanzado</span>

                    <span class="about-span-title">estable</span>

                    <br> desde 2017</p>

                

                <!-- <button class="btn about-us-btn wow animated fadeInLeft">Conocenos </button> -->

            </div>

        </div>

        <section class="about-info">

            <div class="container">

                <div class="image wow animated fadeInLeftBig">

                    <img class="img-fluid" src="images/107.png" alt="">

                </div>

                <p class="wow animated fadeInUp">“EL PODER DE TU ÉXITO ESTA EN NUESTRAS MANOS”</p>

                <em class="wow animated fadeInUp">Desde tiempo inmemoriables (1910) de Henry Harrison Alcia.</em>

            </div>
            <a href="#" class="btn btn-success btn-circle btn-position-ab btn-lg" data-toggle="modal" data-target="#">
                <i class="fas fa-pen"></i>
            </a>

        </section>

        <section class="about-content">

            <div class="container">

                <div class="row">

                    <div class="col-12 col-md-6 order-2 order-md-1">

                        <div class="about-section wow animated fadeInLeft">

                            <h2>Nuestra visión</h2>

                            <p>
                                
                                Nuestro claro objetivo es seguir ofreciendo soluciones integradas en 
                                todos los rubros de las apuestas deportivas, que nos permita 
                                posicionarnos a nivel global con la misma velocidad del avance 
                                de nuestra expansión en america latina en tan solo 4 años, 
                                nuestras alianzas comerciales y asociaciones internacionales 
                                con empresas Europeas y en el norte de África, son muestra de 
                                nuestra sólida intención de introducirnos al mercado global 
                                con la calidad y tecnología más avanzadas del mercado.
                            </p>

                        </div>

                    </div>

                    <div class="col-12 col-md-6 order-1 order-md-2">

                        <img class="img-fluid wow animated fadeInRight" src="images/vision.png" alt="">

                    </div>

                    <div class="col-12 col-md-6">

                        <img class="img-fluid wow animated fadeInLeft" src="images/mision.png" alt="">

                    </div>

                    <div class="col-12 col-md-6">

                        <div class="about-section wow animated fadeInRight">

                            <h2>Nuestra Misión</h2>

                            <p>
                                Nuestra misión es seguir ofreciendo una plataforma rentable, amplia y confiable,
                                innovando todos nuestros servicios y y productos a todo nivel,
                                ofreciendo experiencias únicas tanto en nuestros clientes comerciales
                                asi como en nuestros clientes finales.

                            </p>

                        </div>

                    </div>

                </div>

            </div>
            <a href="#" class="btn btn-success btn-circle btn-position-ab btn-lg" data-toggle="modal" data-target="#">
                <i class="fas fa-pen"></i>
            </a>
        </section>

    </section>



    <section class="contact-bg">              

        <div class="container">

            <h3 class="pb-5 contact-title">CONTÁCTANOS</h3>

            <form method="post" action="enviar.php">

                <div class="form-row">

                    <div class="col-12 col-md input-effect ml-0">

                        <input name="nombre" class="effect-16" type="text" required>

                        <label>NOMBRE Y APELLIDO</label>

                        <span class="focus-border"></span>

                    </div>

                    <div class="col-12 col-md input-effect ml-0 mr-0">

                        <input name="correo" class="effect-16" type="text" placeholder="" required>

                        <label>CORREO</label>

                        <span class="focus-border"></span>

                    </div>

                    <div class="col-12 col-md input-effect mr-0">

                        <input name="telefono" class="effect-16" type="text" placeholder="" required>

                        <label>TELÉFONO</label>

                        <span class="focus-border"></span>

                    </div>

                    <div class="col-12 input-effect ml-0 mr-0 mt-5">

                        <input name="mensaje" class="effect-16" type="text" placeholder="" required>

                        <label>MENSAJE</label>

                        <span class="focus-border"></span>

                        <input type="submit" class="btn-contact" name="enviar" value="Enviar">

                    </div>

                </div>

            </form>

        </div>

        

        <div class="footer-bottom">

            <div class="if-SocialLinks">

                <a class="if-SocialLink" target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/UniversalSoftOficial/" title="UniversalSoft Facebook" data-track="social/facebook">

                    <svg viewBox="0 0 16 16">

                        <path d="M14.27.006H.835C.374.006 0 .38 0 .84v13.437c0 .46.373.834.834.834h7.234V9.263H6.1v-2.28h1.968v-1.68C8.068 3.35 9.26 2.286 11 2.286c.834 0 1.55.062 1.76.09v2.04H11.55c-.946 0-1.13.45-1.13 1.11V6.98h2.258l-.294 2.28h-1.964v5.85h3.85c.46 0 .833-.374.833-.835V.84c0-.46-.373-.834-.834-.834"></path>

                    </svg>

                </a>

                <a class="if-SocialLink" target="_blank" rel="noopener noreferrer" href="https://www.instagram.com/universal.race/" title="@universal.race Instagram" data-track="social/instagram">    

                    <svg viewBox="0 0 16 16">

                        <path d="M7.906 3.984C5.71 3.984 3.89 5.774 3.89 8c0 2.227 1.788 4.016 4.016 4.016 2.227 0 4.016-1.82 4.016-4.016 0-2.196-1.82-4.016-4.016-4.016zm6.525-2.447c.817.816 1.256 1.945 1.256 3.263v6.463c0 1.35-.44 2.51-1.286 3.325-.816.785-1.945 1.224-3.294 1.224h-6.4c-1.286 0-2.416-.408-3.263-1.224C.565 13.74.125 12.58.125 11.23V4.8c0-2.73 1.82-4.55 4.55-4.55h6.462c1.318 0 2.48.44 3.294 1.287zm-6.524 9.036c-1.412 0-2.573-1.16-2.573-2.573 0-1.412 1.16-2.573 2.573-2.573 1.412 0 2.572 1.16 2.572 2.573 0 1.412-1.16 2.573-2.572 2.573zM12.078 4.8c-.502 0-.91-.407-.91-.91 0-.502.408-.91.91-.91.503 0 .91.408.91.91 0 .503-.407.91-.91.91zm2.165 6.463h.063V4.8c0-.91-.314-1.694-.847-2.26-.566-.564-1.35-.846-2.29-.846H4.705c-.94 0-1.757.282-2.29.816-.565.565-.847 1.35-.847 2.29v6.43c0 .974.28 1.758.846 2.323.564.533 1.35.816 2.29.816h6.4c.94 0 1.725-.284 2.29-.817.565-.533.847-1.318.847-2.29z"></path>

                    </svg>

                </a>

                <a class="if-SocialLink" target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/company/universal-soft" title="universal-soft Linkedin" data-track="social/linkedin"> 

                    <svg viewBox="0 0 16 16">

                        <path d="M0 1.908C0 2.47.18 2.935.54 3.3c.36.366.825.55 1.395.55h.04c.585 0 1.06-.184 1.425-.55.365-.365.547-.83.547-1.392 0-.546-.183-1-.55-1.363C3.028.182 2.554 0 1.973 0 1.404 0 .932.183.56.55.185.914 0 1.367 0 1.91v-.002zM.47 16h3.295V5H.47v11zM5.57 5h3.254l.12 1.622c.76-1.218 1.898-1.826 3.11-1.826 1.198 0 2.156.435 2.873 1.306.717.87 1.075 2.068 1.075 3.59V16h-3.292v-5.962c0-.798-.162-1.404-.486-1.82-.324-.414-.91-.622-1.455-.622-.54 0-.98.186-1.318.558-.34.37-.503.884-.508 1.54V16H5.648V7.605L5.568 5z"></path>

                    </svg>

                </a>

                <a class="if-SocialLink" target="_blank" rel="noopener noreferrer" href="https://issuu.com/universalsoft/docs/revista" title="universal-soft on Issuu" data-track="social/issuu">

                    <svg viewBox="0 0 16 16">

                        <path fill-rule="evenodd" d="M7.98 10.025c-1.118-.01-2.016-.926-2.005-2.044.01-1.118.925-2.016 2.044-2.005 1.117.01 2.016.926 2.005 2.044-.01 1.118-.926 2.016-2.044 2.005zM8 0c4.418 0 8 3.582 8 8s-3.582 8-8 8-8-3.582-8-8V.665C0 .3.3 0 .665 0H8zM2.504 7.89c-.03 3.066 2.434 5.577 5.502 5.606 3.067.03 5.578-2.435 5.607-5.502.03-3.067-2.435-5.577-5.502-5.606-3.067-.03-5.577 2.434-5.606 5.502zM8.038 4c2.21.02 3.983 1.83 3.962 4.038-.02 2.21-1.83 3.983-4.038 3.962C5.752 11.98 3.98 10.17 4 7.962 4.02 5.752 5.83 3.98 8.038 4z"></path>

                    </svg>

                </a>

            </div>

            <p class="footer-p">© Universal soft. Todos los derechos reservados 2018 - 2020.

                <a href="#">Empleos | </a>

                <a href="#">Términos y condiciones | </a>

                <a href="#">Política de privacidad</a>

            </p>

            

        </div>

    </section>



    <a class="scroll-top" href="#scrolltop"><i class="fas fa-chevron-up"></i></a>



 <!-- loader -->

 <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>



    <script src="js/jquery.min.js"></script>

    <script src="js/jquery-migrate-3.0.1.min.js"></script>

    <script src="js/popper.min.js"></script>

    <script src="js/bootstrap.min.js"></script>

    <script src="js/jquery.easing.1.3.js"></script>

    <script src="js/jquery.waypoints.min.js"></script>

    <script src="js/jquery.stellar.min.js"></script>

    <script src="js/jquery.animateNumber.min.js"></script>

    <script src="js/bootstrap-datepicker.js"></script>

    <script src="js/jquery.timepicker.min.js"></script>

    <script src="js/owl.carousel.min.js"></script>

    <script src="js/jquery.magnific-popup.min.js"></script>

    <script src="js/scrollax.min.js"></script>

    <script src="js/mystyles.js"></script>

    <script src="js/main.js"></script> 

    <script src="js/moment.js"></script>

  </body>

</html>