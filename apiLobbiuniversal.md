UNIVERSAL SOFT CASINO - API IMPLEMENTATION v1.41_ENG
(Updated: Oct, 30/2021)

DESCRIPTION

Universal Soft Casino Games by HTML iFrame.

INSERTION URL FOR CASINO iFRAME
https://www.api-usoft.com/casino/index.php

PROGRAMMING SEQUENCE
⦁	The user starts session on your Web.
⦁	If user is not registered in UNIVERSAL SOFT, you must send user data via YSetUser EndPoint.
⦁	Start sesion in CASINO with YStartSession EndPoint.
⦁	YStartSession sends you the User's Session Token as response.
⦁	In your Web open a HTML iFrame with the insertion URL, sending your assigned Auth Token and Session Token via GET method. Ex:

https://www.api-usoft.com/casino/index.php
?a=00112233ABC&t=abcde12345

	Where:
⦁	"00112233ABC" is your assigned Auth Token.
⦁	"abcde12345" is the Session Token from YStartSession.

AUTHORIZATION
Must send the EndPoints with the following headers:

Content-Type: application/json
auth: [Assigned Authorization Token to customer.]


UNIVERSAL SOFT ENDPOINTS

Users
YSetUser – Create user.

YChangeUser – Change  user data.

YUserType – Show available user's types.


Casino
YStartSession – Start  user session.


General purpose
YCountry – Get country geo data according to IP or ISO 3166-1 country code.

YCurrency – Get currency data according to ISO 4217 code.


CUSTOMERS ENDPOINTS
ReadBalance – Obtain Client's users balance.

BetInsert – User's bet notification.
 
ResultBet – Notification of result from the user's play.


APPENDIX

I - API-USOFT Error table.

II – Standard Error table.


YSetUser
Create user

URL: https://www.api-usoft.com/UniversalAPI

POST JSON
 {
  "actionAPI" : "YSetUser",
  "type"      : "WEB",
  "login"     : "MyLogin",
  "pass"      : "MyPass123",
  "names"     : "USER NAME",
  "surnames"  : "USER SURNAME",
  "sex"       : "M",
  "bdate"     : "20000101",
  "doctype"   : "DNI",
  "docnum"    : "123456789",
  "address"   : "SAN JUAN DE LURIGANCHO",
  "zip"       : "15001",
  "city"      : "LIMA",
  "country"   : "PE",
  "phone"     : "+51 999 999 999",
  "email"     : "email@domain.com",
  "web"       : "http://www.miweb.com",
  "image"     : "",
  "currency"  : "USD",
  "balance"   : 0.00
 }

⦁	REQUEST -
Key	Type	Size	Description
type	STR	5	"ABC" User is "ABC" type.
(User types from YUserType.)
login	STR	100	User Login. (Must be unique.)
pass	STR	100	User Password.
names	STR	100	User name (or enterprise name).
surnames	STR	100	User surname.
sex	STR	1	Sex. (M-ale / F-emale / N-o sex)
N for enterprise, institutions, etc.
bdate	STR	8	User birth date. (YYYYMMDD).
doctype	STR	5	ID Type doc. (Do not repeat the doctype-docnum combination.)
(See III - DocType Table.)
docnum	STR	20	ID document number. (Do not repeat the doctype-docnum combination.)
address	STR	200	Postal address.
zip	STR	20	Postal code. ZIP.
city	STR	100	User's city of residence.
country	STR	2	Country ISO 3166-1 code.
phone	STR	100	Phone numbers.
email	STR	100	User email. (Must be unique.)
web	STR	100	User or enterprise Web page.
image	STR	0 / 99999	Base64 image file.
(In development stage.)
currency	STR	0 / 3	Currency ISO 4217 code.
(Find allowed currency on YCurrency EndPoint.)
⦁	This value is only valid when "type" is "TER" (Terminal) o "WEB" (Web User). This value is ignored for other "type".
balance	DEC	10,2	User balance.
⦁	Only valid when "type" value is "TER" o "WEB". Must be zero (0) or greater.
⦁	This key will be ignored if "type" is different from "TER" or "WEB".

⦁	RESPONSE –
{
Key	Tipo	Tamaño	Description
endpoint	STR	100	Requested EndPoint: YSetUser
status	INT	1	Response status:
0: Invalid. / 1: Valid.
errcod	STR	7	Error code.
(See: API-USOFT Error Table)
errmsg	STR	50	Error description.
uParent	STR	13	Parent (customer) Serial (ID).
uSerial	STR	13	Assigned User Serial (ID).
uDateReg	STR	8	Registration Date:YYYYMMDD.
uTimeReg	STR	8	Registration Time: HH:MM:SS.
uActive	INT	1	User status:
0: Inactive. / 1: Active.
}

YChangeUser
Change user data

URL: https://www.api-usoft.com/UniversalAPI

POST JSON
 {
  "actionAPI" : "YChangeUser",
  "serial"    : "1234567890001",
  "address"   : "SANTIAGO DE SURCO",
  "zip"       : "15001",
  "city"      : "LIMA",
  "phone"     : "+51 987 654 321",
  "web"       : "http://www.miweb.com",
  "image"     : "",
  "active"    : 0	1
 }

⦁	REQUEST –
Key	Type	Size	Description
serial	STR	13	Key MANDATORY.
User serial to modify.
address	STR	200	Key OPTIONAL.
Postal address.
zip	STR	20	Key OPTIONAL.
Postal code / ZIP.
city	STR	100	Key OPTIONAL.
User origin city.
phone	STR	100	Key OPTIONAL.
Phone numbers.
web	STR	100	Key OPTIONAL.
User Web page.
image	STR	0 / 99999	Key OPTIONAL.
Image on Base64 format.
(In development stage.)
active	INT	1	Key MANDATORY.
User status:
0: Inactive. 1: Active.

⦁	In case of sending an OPTIONAL Key, the data will be MANDATORY according to the previous specification.


⦁	RESPONSE –
{
Key	Type	Size	Description
endpoint	STR	100	EndPoint requested: YChangeUser
status	INT	1	Response status:
0: Error. 1: No Error.
errcod	STR	7	Error code.
(See: API-USOFT Error Table)
errmsg	STR	50	Error description.
uAddress	STR	200	Postal address.
uZip	STR	20	Postal code / ZIP.
uCity	STR	100	User's city of origin.
uPhone	STR	100	Phone numbers.
uWeb	STR	100	Web page.
uImage	STR	0 / 99999	Image URL.
(In development stage.)
uActive	INT	1	User status:
0: Inactive. / 1: Active.
}




















YUserType
Display available user types

URL: https://www.api-usoft.com/UniversalAPI

POST JSON
 {
  "actionAPI" : "YUserType",
  "utype"     : ""
 }
⦁	REQUEST -
Key	Type	Size	Description
utype	STR	5	User type according to available types. ("" For all available types)

⦁	RESPONSE –
{
Key	Type	Size	Description
endpoint	STR	100	Requested EndPoint: YUserType
status	INT	1	Status: 0: Error. 1: No Error.
errcod	STR	7	Error code. 
(See: API-USOFT Error Table)
errmsg	STR	50	Error description.
total	INT	7	Total records.
utype			
   [99999
    {
utCode	STR	5	User type code.
utName	STR	50	User type name.
utClass	STR	1	User class:
P-Personal (Gambler).
I-Impersonal (Premises, Brand)
utValue	INT	0 / 9999	Value of each user compared to other types of users.
utIcon	STR	20	Image Icon in
http://fontawesome.com/icons/???
utSessTemp	INT	1	Types of sessions of this user:
0: Permanent / 1: Temporary
utSessMin	INT	7	Minutes allowed when sessions are temporary. Zero (0) for Permanent.
utActive	INT	1	User status: 
0: Inactive. / 1: Active.
    }
   ]
}

YStartSession
Start users session

URL: https://www.api-usoft.com/UniversalAPI

POST JSON
 {
  "actionAPI" : "YStartSession",
  "userial"   : "1234567890001",
  "ubalance"  : 5000.00
 }

⦁	REQUEST -
Key	Type	Size	Description
userial	STR	13	Registered user serial.
It will be validated that it belongs to each client.
ubalance	DEC	10,2	Updated user balance.


⦁	RESPUESTA -
{
Key	Type	Size	Description
endpoint	STR	100	Requested EndPoint: YStartSession
status	INT	1	Status: 0: Error. 1: No Error.
errcod	STR	7	Error code. 
(See: API-USOFT Error Table)
errmsg	STR	50	Error description.
utoken	STR	128	Session ID User Token.
}














YCountry
Display country geo data according to IP or ISO 3166-1 country code.

URL: https://www.api-usoft.com/UniversalAPI

POST JSON
 {
  "actionAPI" : "YCountry",
  "ip"        : "999.999.999.999",
  "country"   : "VE"
 }

⦁	REQUEST -
Key	Type	Size	Description
ip	STR	7 a 15	Country IP address.
"": All countries.
(Excluding with "country"). Priority.
country	STR	2	Two bytes ISO 3166-1 country code.
"": All countries.
(Excluding with "ip").
Secondary.

⦁	RESPONSE –
{
Key	Type	Size	Description
endpoint	STR	100	Requested EndPoint: YCountry
status	INT	1	Status: 0: Error. 1: No Error.
errcod	STR	7	Error code. 
(See: API-USOFT Error Table)
errmsg	STR	50	Error description.
urlbase	STR	50	Full base URL of flags images.
Include final slash "/".
total	INT	1	Total record (countries).
country			
   [99999
    {
cISO	STR	2	Two bytes ISO 3166-1 country code.
"": Not found / Not exist.

cNameLOW	STR	100	Lowercase country name (latin characters).
"": Not found / Not exists.
cNameUPP	STR	100	Uppercase country name (latin characters).
"": Not found / Not exist.
cLangCOD	STR	5	Language ISO 639 code.
"": Not found / Not exist.
cLangNAM	STR	100	Language name (Latin).
"": Not found / Not exist.
cCurrCOD	STR	3	Currency ISO 4217 code.
"": Not found / Not exist.
cCurrSYM	STR	5	Currency symbol (Windows-1252).
"": Not found / Not exist.
cCurrNAM	STR	100	Currency name (Latin).
"": Not found / Not exist.
cPhonCOD	STR	5	Phone country code (without "+").
"": Not found / Not exist.
cFlagIMG	STR	11	Country flag image file name. 
(128 px wide / 85 px high / 24 bits colour / jpg format).
    }
   ]
}


















YCurrency
Returns list of currencies
according to ISO 4217 code

URL: https://www.api-usoft.com/UniversalAPI

POST JSON
{
 "actionAPI" : "YCurrency",
 "serial"    : "1234567890001",
 "currency"  : "PEN",
 "setted"    : 0 / 1 / 2
}

⦁	REQUEST -
Key	Type	Size	Description
serial	STR	0 a 13	Customer / User Serial ID.
- "": All currencies.
- "ABC": Currency for "ABC" Serial. - Priority over "currency".
currency	STR	0 / 3	Currency ISO 4217 code.
- Secondary under "serial".
setted	INT	1	Request currencies that are established as valid for providers.
0: Request non setted.
1: Request setted.
2: Request all currencies.

⦁	RESPUESTA -
 {
Key	Type	Size	Explicación
endpoint	STR	100	Requested EndPoint: YCurrency
status	INT	1	Status: 0: Error. 1: No Error.
errcod	STR	7	Error code. 
(See: API-USOFT Error Table)
errmsg	STR	100	Error description.
total	INT	3	Total record (currencies).
currency			

   [99999
    {
curISO	STR	3	Currency ISO 4217 code.
curSYM	STR	1 a 3	Currency symbol.
"---": Can not be represented in Windows-1252 character code.
curNAM	STR	100	Currency name (Latin).
curSET	INT	1	Indicates if this currency is set as valid for providers.
1: Currency setted.
0: Currency not setted.
    }
   ]
  }



























ReadBalance
Get user balance

URL: https://[Customer_CallBack_URL]/ReadBalance

POST JSON
 {
  "sessionid" : (STR) (128)
 }

⦁	REQUEST -
Key	Type	Size	Description
sessionid	STR	128	User session Token.


⦁	EXPECTED RESPONSE -
{
Key	Type	Size	Description
status	INT	1	Status: 0: Error. 1: No Error.
balance	DEC	10,2	Updated user balance.
code	STR	20	Standard error code. 
(See: Standard Error Table)
message	STR	50	Custom error message.
}






BetInsert
User's bet notification.


URL: https://[Customer_CallBack_URL]/BetInsert

POST JSON
 {
  "trxid"     : "Y_BET_005",
  "movement"  : "BET",
  "amount"    : -1,
  "sessionid" : "abcedefghijklmnopqrestuvwxyz1234567890",
  "gameid"    : "bjmb",
  "game_name" : "American Blackjack",
  "custom"    : "",
  "usertoken" : "abcedefghijklmnopqrestuvwxyz"
 }

⦁	REQUEST -
Key	Type	Size	Description
trxid	STR	100	Transaction ID
movement	STR	10	"BET"
(For BetInsert is ALWAYS "BET")
amount	DEC	12,2	Bet amount.
Always a negative value.
sessionid	STR	128	Game ID session.
gameid	STR	100	Game ID code.
game_name	STR	100	Name of game.
custom	STR	100	Optional field. (Commonly "")
usertoken	STR	128	User session Token.

⦁	EXPECTED RESPONSE -
{
Key	Type	Size	Description
status	INT	1	Status: 0: Error. 1: No Error.
balance	DEC	10,2	Updated user balance.
code	STR	20	Standard error code. 
(See: Standard Error Table)
message	STR	50	Custom error message.
}

ResultBet
Notification of result
from the user's play.

URL: https://[Customer_CallBack_URL]/ResultBet

POST JSON
 {
  "trxid"        : "Y_BET_005",
  "movement"     : "REFUND" / "WIN"
  "amount"       : 1,
  "sessionid"    : "abcedefghijklmnopqrestuvwxyz1234567890",
  "gameid"       : "bjmb",
  "game_name"    : "American Blackjack",
  "custom"       : "",
  "referenceBet" : "Y_BET_004",
  "usertoken"    : "yz1234567890"
 }

⦁	REQUEST -
Key	Type	Size	Description
trxid	STR	100	Transaction's ID.
movement	STR	10	"REFUND"
If referenceBet = BET: Add amount to user balance.
If referenceBet = WIN: Subtract amount from user balance.
"WIN": Add amount to user balance.
amount	DEC	12,2	Amount of movement. (Absolute).
sessionid	STR	128	Game SessionID.
gameid	STR	100	Game code.
game_name	STR	100	Game name.
custom	STR	100	Optional field.
referenceBet	STR	100	Reference transaction ID.
usertoken	STR	128	User session Token.

⦁	EXPECTED RESPONSE -
{
Key	Type	Size	Description
status	INT	1	Status: 0: Error. 1: No Error.
balance	DEC	10,2	Updated user balance.
code	STR	20	Standard error code. See Error Table.
message	STR	50	Custom error message.
}

I - API-USOFT Error Table

//--- General.
OK-00  : Ok.
OK-01  : No data was found according to received parameters.
ERR-00 : Undetermined error.

//--- Access.
ACC-01 : EndPoint not found. Review documentation.
ACC-02 : Method not accepted.
ACC-03 : No JSON format received.
ACC-04 : Incorrect access IP.
ACC-05 : Invalid IP. No IPv4.

//--- Requests.
REQ-01 : No Keys received.
REQ-02 : Keys absent. Review structure.
REQ-03 : Wrong Keys. Review structure.
REQ-04 : Too many Keys.
REQ-05 : Wrong Keys values.

//--- Customer.
CLI-01 : Customer serial not found.
CLI-02 : Access denied for customer.
CLI-03 : Unregistered IP for customer.
CLI-04 : Development ended. You can not modify Access IP.

//--- User.
USER-01 : User serial not found.
USER-02 : User exists.
USER-03 : Insuficcient balance.
USER-04 : Undetermined error or inactive user.

//--- Bets.
BET-01 : User Token not found.
BET-02 : BetReference not found.
BET-03 : TRXID not found.
BET-04 : Duplicated TRXID.
BET-05 : Incorrect bet amount.





II – Standard Erro Table


INSUFFICIENT_FUNDS
Bet is greater than user's balance.


TRX_DUPLICATED
A previous transaction has been registered with same ID.


USER_NOT_FOUND
There is no record for this user on data base.


BETREFERENCE_NOT_FOUND
Bet identifier is not registered.


SESSION_NOT_FOUND
Requested Session ID not found.


INTERNAL_ERROR
Internal error due unknown or undetermined causes.





















III – DocType Table


Code		Description
DNI		Documento Nacional de Identidad
(Identity National Document)
PAS		Pasaporte
(Passport)
PTP		Permiso Temporal de Permanencia
(Permanence Temporal Permission)
CPP		Carnet de Permanencia en Perú
(Peruvian Permanence Carnet)
CRE		Carnet de Refugio
(Refuge Carnet)
CEX		Carnet de Extranjería
(Foreigner Carnet)
RUC		Registro Unico de Contribuyentes
(Unique Registry for Taxes)
CED		Cédula de Identidad
(Identity Paper)
RIF		Registro de Identificación Fiscal
(IRS ID Registry)
NIT		Número de Identificación Tributaria
(Tributary ID Number)
NSS		Número de Seguridad Social
(Social Security Number)
LIC		Licencia de Conducir
(Drivers License)

