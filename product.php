<!DOCTYPE html>

<html lang="en">

  <head>

    <title>Universal Soft</title>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="icon" type="image/x-icon" href="images/favicon.png">



    <link rel="stylesheet" href="css/animate.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/owl.carousel.min.css">

    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <link rel="stylesheet" href="css/magnific-popup.css">



    <link rel="stylesheet" href="css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="css/jquery.timepicker.css">

    <link rel="stylesheet" href="fonts/fontawesome/css/all.css">

    <link rel="stylesheet" href="css/style.css">

  </head>

  <body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark no-desktop">
        <a class="navbar-brand" href="index.php"><img class="d-inline-block align-top img-fluid" src="images/logo.png" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menucontrol" aria-controls="menucontrol" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="menucontrol">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <div class="header-line">
                    <a class="header-span" href="about.php">
                        <span>ACERCA</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <div class="header-line">
                    <a class="header-span" href="services.php">
                        <span>SERVICIOS</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <div class="header-line">
                    <a class="header-span" href="product.php">
                        <span>PRODUCTOS</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <div class="header-line">
                    <a class="header-span" href="systems.php">
                        <span>SISTEMAS</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <div class="header-line">
                    <a class="header-span" href="news.php">
                        <span>NOTICIAS</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <div class="header-line">
                    <a id="#" class="header-span" href="#contact">
                        <span>CONTACTO</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <div class="header-line">
                    <select class="header-select" name="" id="">
                        <option value="es">ES</option>
                        <option value="eu">EN</option>
                    </select>
                </div>
            </li>
          </ul>
        </div>
    </nav>
    <nav class="header no-movil">
        <div class="header-div">
            <div class="header-w28">
                <a href="index.php"><img class="img-fluid" src="images/logo.png" alt=""></a>
            </div>
            <div class="header-line">
                <a class="header-span" href="about.php">
                    <span>ACERCA</span>
                </a>
            </div>
            <div class="header-line">
                <a class="header-span" href="product.php">
                    <span>PRODUCTOS</span>
                </a>
            </div>
            <div class="header-line">
                <a class="header-span" href="services.php">
                    <span>SERVICIOS</span>
                </a>
            </div>
            <div class="header-line">
                <a class="header-span" href="systems.php">
                    <span>SISTEMAS</span>
                </a>
            </div>
            <div class="header-line">
                <a class="header-span" href="news.php">
                    <span>NOTICIAS</span>
                </a>
            </div>
            <div class="header-line">
                <!-- <a id="#" class="header-span" href="#" data-toggle="modal" data-target="#modalNoticia"> -->
                <a id="#" class="header-span" href="#contact"> <span>CONTACTO</span>
                </a>
            </div>
            <div class="header-line">
                <select class="header-select" name="" id="">
                    <option value="es">ES</option>
                    <option value="eu">EN</option>
                </select>
            </div>
        </div>
    </nav>

    

    <section class="body-bg">

        <section class="container-product"> 

            <div class="slider-bg product-us">

                <div class="animation-wrapper">

                    <div class="particle particle-1"></div>

                    <div class="particle particle-2"></div>

                    <div class="particle particle-3"></div>

                    <div class="particle particle-4"></div>

                </div>

                <video id="video" class="slider-video" width="100%" preload="auto" loop autoplay muted poster="images/slider2a.png">

                    <source id="mp4" src="images/video-1.mp4" type="video/mp4">

                    <source id="webm" src="images/hipica.webm" type="video/webm">

                </video>

            </div>

            <button id="audio-mute" class="audio" onclick="getVideo().muted=true" style="display: none;"><i class="fas fa-volume-up"></i></button>

            <button id="audio-vol"  class="audio" onclick="getVideo().muted=false" style="display: block;"><i class="fas fa-volume-mute"></i></button>

            <div class="tabs-productos">

                <ul class="nav nav-pills w-100" id="pills-tab" role="tablist">

                    <li class="nav-item w-20" role="presentation" onclick="switchVideo(0);">

                        <a class="nav-link nav-btn-pro waves-effect waves-button active" id="pills-hipica-tab" data-toggle="pill" href="#pills-hipica" role="tab" aria-controls="pills-hipica" aria-selected="true">HÍPICA EN VIVO</a>

                    </li>

                    <li class="nav-item w-20" role="presentation" onclick="switchVideo(1);">

                        <a class="nav-link nav-btn-pro waves-effect waves-button" style="background: rgba(0, 0, 0, 0.4)!important;" id="pills-juegos-tab" data-toggle="pill" href="#pills-juegos" role="tab" aria-controls="pills-juegos" aria-selected="false">JUEGOS VIRTUALES</a>

                    </li>

                    <li class="nav-item w-20" role="presentation" onclick="switchVideo(2);">

                        <a class="nav-link nav-btn-pro waves-effect waves-button"  id="pills-casino-tab" data-toggle="pill" href="#pills-casino" role="tab" aria-controls="pills-casino" aria-selected="false">CASINO</a>

                    </li>

                    <li class="nav-item w-20" role="presentation" onclick="switchVideo(3);">

                        <a class="nav-link nav-btn-pro waves-effect waves-button" style="background: rgba(0, 0, 0, 0.4)!important;" id="pills-casinovivo-tab" data-toggle="pill" href="#pills-casinovivo" role="tab" aria-controls="pills-casinovivo" aria-selected="false">CASINO EN VIVO</a>

                    </li>

                    <li class="nav-item w-20" role="presentation" onclick="switchVideo(4);">

                        <a class="nav-link nav-btn-pro waves-effect waves-button" id="pills-poker-tab" data-toggle="pill" href="#pills-poker" role="tab" aria-controls="pills-poker" aria-selected="false">POKER</a>

                    </li>

                </ul>

            </div>

        </section>

        <section class="product-content">

            <div class="tab-content" id="pills-tabContent">

                <div class="tab-pane fade show active" id="pills-hipica" role="tabpanel" aria-labelledby="pills-hipica-tab">

                    <div class="container">

                        <div class="row">

                            <div class="col-12 col-md-6 order-2 order-lg-1">

                                <div class="about-section wow animated fadeInLeft">

                                    <h2>Hípica Online</h2>

                                    <p>

                                        Hoy en dia gracias al desarrollo de universal soft  jamas fue tan fácil apostar  en 

                                        las distintas plataformas sin limites  limites. Ofrecemos una experiencia de 

                                        usuarios en cualquier parte del mundo ya que nuestro software proporciona 

                                        exclusividad para cada mercado especifico

                                    </p>

                                </div>

                            </div>

                            <div class="col-12 col-md-6 order-1 order-lg-2">

                                <img class="img-fluid wow animated fadeInRight" src="images/onlines.png" alt="">

                            </div>

                            <div class="col-12 col-md-6 order-3 order-lg-3">

                                <img class="img-fluid wow animated fadeInLeft" src="images/retails.png" alt="">

                            </div>

                            <div class="col-12 col-md-6 order-4 order-lg-4">

                                <div class="about-section wow animated fadeInRight">

                                    <h2>Hípica Retail</h2>

                                    <p>

                                        Nuestros canales 24/7 en vivo disponibles de todos los mercado emitiendo las carreras mas 

                                        exclusivas en todos los idiomas.<br><br>

                                        Ahora usted puede optimizar y fidelizar a sus clientes con nuestra plataforma sincronizada

                                        con todos los canales de transmisión de alta calidad mejorando la experiencia de sus 

                                        clientes  en bares ,locales de apuestas , tiendas , tragamonedas.<br><br>

                                        Proporcionamos las mejores herramientas y ofertas mejorando su producción incluyendo apuestas

                                        exclusivas q solo Universal soft creo para una producción agresiva y perfecta.<br><br>

                                    </p>

                                </div>

                            </div>

                            <div class="col-12 col-md-4 mt-5 pt-3 tab-vertical order-5 order-lg-5">

                                <div class="nav flex-column nav-pills wow animated fadeInLeft" id="v-pills-tab" role="tablist" aria-orientation="vertical">

                                    <a class="nav-link tab-vertical-nav active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Carrera de Caballos</a>

                                    <a class="nav-link tab-vertical-nav" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Carrera de Galgos</a>

                                    <a class="nav-link tab-vertical-nav" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Carrera de Harness</a>

                                    

                                </div>

                            </div>

                            <div class="col-12 col-md-8 mt-5 pt-3 order-6 order-lg-6">

                                <div class="tab-content wow animated fadeInRight" id="v-pills-tabContent">

                                  <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">

                                    <div class="wow animated fadeIn">

                                        <p class="tab-vertical-p">

                                            Uno de los deportes mas antiguos ahora  en nuestra plataforma , sencilla ,completa y robusta 

                                            cubriendo todos los países del mundo con mas de 1000 eventos diarios y mas de 30 paises 

                                            con transmisiones full HD.

                                        </p>

                                        <img class="img-fluid" src="images/caballos.png" alt="">

                                    </div>

                                  </div>

                                  <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">

                                    <div class="wow animated fadeIn">

                                        <p class="tab-vertical-p">

                                            Debido a que las carrera de galgos son muy populares en varios  países, tuvo como inicio 

                                            EE.UU pero hoy en día incluyendo Reino unido  generando popularidad  ya que los eventos se 

                                            muestran cada 7 minutos (24/7) manteniendo a los apostadores activos , generando y 

                                            optimizando mucho más en tu negocio.

                                        </p>

                                        <img class="img-fluid" src="images/perros.png" alt="">

                                    </div>

                                  </div>

                                  <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">

                                    <div class="wow animated fadeIn">

                                        <p class="tab-vertical-p">



                                            Las naciones líderes en carreras de caballos como Europa, Francia, Italia y Suecia

                                            es bastante popular las carrera de harness en la mayoría de los países del norte de Europa. 

                                            ofrecemos este tipo de evento en la plataforma con transmisiones full HD. 

                                        </p>

                                        <img class="img-fluid" src="images/harness.png" alt="">

                                    </div>

                                  </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="tab-pane fade" id="pills-juegos" role="tabpanel" aria-labelledby="pills-juegos-tab">

                    <div class="container">

                        <div class="row">

                            <div class="col-12 col-md-6">

                                <div class="about-section wow animated fadeInLeft">

                                    <h2>Deportes Virtuales y Más</h2>

                                    <p>

                                        Manejamos un paquete muy exclusivo y variado de juegos reales pregrabados y 3D.<br><br>

                                        Universal soft cuenta co las mas alta experiencia con proveedres que destacan en 

                                        todos los mercados formando alianzas para proveer a nuestros clientes el complemento

                                        perfecto para casas  de apuestas e integraciones de los mejores juegos a nivel mundial.

                                    </p>

                                </div>

                            </div>

                            <div class="col-12 col-md-6">

                                <img class="img-fluid wow animated fadeInRight" src="images/virtuales.png" alt="">

                            </div>

                        </div>

                    </div>

                    <section class="casino-slot-bg">

                        <div class="container">

                            <div class="row">

                                <div class="col-12">

                                    <h2 class="section-title full animated wow fadeInDown">Nuestro Proveedores</h2>

                                    <div class="row">

                                        <div class="col-12 col-md-6 col-lg-4">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-virtual-3"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-3"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-3"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-3"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-3"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-virtual-7"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-7"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-7"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-7"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-7"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-virtual-1"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-1"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-1"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-1"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-1"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-virtual-2"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-2"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-2"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-2"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-2"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-virtual-5"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-5"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-5"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-5"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-5"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-virtual-6"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-6"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-6"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-6"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-6"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-virtual-4"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-4"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-4"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-4"></span>

                                                <span class="logo-slot logo-carousel-slot logo-virtual-4"></span>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </section>

                </div>

                <div class="tab-pane fade" id="pills-casino" role="tabpanel" aria-labelledby="pills-casino-tab">

                    <div class="container">

                        <div class="row">

                            <div class="col-12 col-md-6">

                                <div class="about-section wow animated fadeInLeft">

                                    <h2>Slot Casino</h2>

                                    <p>

                                        Lo mejor y mas variado de los mejores proveedores del mundo.<br><br>

                                        En estos últimos años se desarrollo una atraciion y fascinación con 

                                        los juegos de casino en línea .Los operadorespueden invertir en sus 

                                        negocios de casino en línea obteniendo grandes beneficios con nuestra

                                        solución todo en uno con lo mejores juegos del mundo totalmente personalizado.<br><br>



                                        Brindamos exitosas asociaciones de Universal Soft con proveedores de 

                                        renombre para agregar un valor muy importante en tu negocio.<br><br>



                                        Tenemos : SLOTS,  PREMIUM SLOTS , VIDEO BINGO, RASPA , FISHING , ARCADE.



                                    </p>

                                </div>

                            </div>

                            <div class="col-12 col-md-6">

                                <img class="img-fluid wow animated fadeInRight" src="images/casinos.png" alt="">

                            </div>

                        </div>

                    </div>

                    <section class="casino-slot-bg">

                        <div class="container">

                            <div class="row">

                                <div class="col-12">

                                    <h2 class="section-title full animated wow fadeInDown">Nuestro Proveedores</h2>

                                    <ul class="filters-provee filters-1">

                                        <li class="filters-li" data-casino="casino1"><span>Casino</span></li>

                                        <li class="filters-li" data-casino="casino2"><span>Casino Premium</span></li>

                                        <li class="filters-li" data-casino="casino3"><span>Pesca</span></li>

                                        <li class="filters-li" data-casino="casino4"><span>Video Bingo</span></li>

                                        <li class="filters-li" data-casino="casino5"><span>Raspa y gana</span></li>

                                    </ul>

                                    <div class="row filter">

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-46"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-46"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-46"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-46"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-46"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-47"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-47"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-47"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-47"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-47"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino5">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-48"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-48"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-48"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-48"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-48"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-49"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-49"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-49"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-49"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-49"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-50"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-50"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-50"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-50"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-50"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-51"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-51"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-51"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-51"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-51"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-52"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-52"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-52"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-52"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-52"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-53"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-53"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-53"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-53"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-53"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-54"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-54"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-54"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-54"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-54"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-39"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-39"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-39"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-39"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-39"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-40"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-40"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-40"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-40"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-40"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-41"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-41"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-41"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-41"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-41"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-42"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-42"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-42"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-42"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-42"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-43"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-43"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-43"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-43"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-43"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-44"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-44"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-44"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-44"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-44"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-45"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-45"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-45"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-45"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-45"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-37"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-37"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-37"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-37"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-37"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-1"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-1"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-1"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-1"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-1"></span>

                                            </div>

                                            

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-9"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-9"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-9"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-9"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-9"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-10"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-10"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-10"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-10"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-10"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-11"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-11"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-11"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-11"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-11"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-12"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-12"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-12"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-12"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-12"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-13"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-13"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-13"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-13"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-13"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-14"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-14"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-14"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-14"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-14"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-15"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-15"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-15"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-15"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-15"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-16"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-16"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-16"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-16"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-16"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-17"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-17"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-17"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-17"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-17"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-18"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-18"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-18"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-18"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-18"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-19"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-19"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-19"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-19"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-19"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-20"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-20"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-20"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-20"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-20"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-21"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-21"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-21"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-21"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-21"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-22"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-22"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-22"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-22"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-22"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-23"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-23"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-23"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-23"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-23"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-24"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-24"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-24"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-24"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-24"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-25"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-25"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-25"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-25"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-25"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-26"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-26"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-26"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-26"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-26"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-27"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-27"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-27"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-27"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-27"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-28"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-28"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-28"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-28"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-28"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-29"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-29"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-29"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-29"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-29"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-30"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-30"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-30"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-30"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-30"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-31"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-31"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-31"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-31"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-31"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-32"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-32"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-32"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-32"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-32"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-33"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-33"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-33"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-33"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-33"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-34"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-34"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-34"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-34"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-34"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-35"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-35"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-35"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-35"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-35"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-36"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-36"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-36"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-36"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-36"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-38"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-38"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-38"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-38"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-38"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino2">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-3"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-3"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-3"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-3"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-3"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino2">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-4"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-4"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-4"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-4"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-4"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino3">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-5"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-5"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-5"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-5"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-5"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino3">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-2"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-2"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-2"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-2"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-2"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-6"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-6"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-6"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-6"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-6"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino3">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-7"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-7"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-7"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-7"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-7"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino3">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casino-8"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-8"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-8"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-8"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casino-8"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino4">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-11"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-11"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-11"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-11"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-11"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino4">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-12"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-12"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-12"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-12"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-12"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino4">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-13"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-13"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-13"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-13"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-13"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino4">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-14"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-14"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-14"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-14"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-14"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino4">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-15"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-15"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-15"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-15"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-15"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casino4">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-16"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-16"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-16"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-16"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-16"></span>

                                            </div>

                                        </div>  

                                    </div>

                                </div>

                            </div>

                        </div>

                    </section>

                </div>

                <div class="tab-pane fade" id="pills-casinovivo" role="tabpanel" aria-labelledby="pills-casinovivo-tab">

                    <div class="container">

                        <div class="row">

                            <div class="col-12 col-md-6">

                                <div class="about-section wow animated fadeInLeft">

                                    <h2>Casino en vivo</h2>

                                    <p>

                                        Los juegos de casino en vivo en la actualidad causan furor insaciable e interminable.<br><br>

                                        Universal soft sigue formando alianzas exitosas para brindar la mas variada participación

                                        y colaboración con proveedores de las principales compañías de casino en vivo de alta

                                        gama para satifacer los distintos mercados del mundo.



                                    </p>

                                </div>

                            </div>

                            <div class="col-12 col-md-6">

                                <img class="img-fluid wow animated fadeInRight" src="images/casino-live.png" alt="">

                            </div>

                        </div>

                    </div>

                    <section class="casino-slot-bg">

                        <div class="container">

                            <div class="row">

                                <div class="col-12">

                                    <h2 class="section-title full animated wow fadeInDown">Nuestro Proveedores</h2>

                                    <ul class="filters-provee filters-2">

                                        <li class="filters-li" data-casinolive="casinolive1"><span>Casino en vivo</span> </li>

                                        <li class="filters-li" data-casinolive="casinolive2"><span>juegos Backgammon</span></li>

                                        <li class="filters-li" data-casinolive="casinolive3"><span>Juegos TV</span></li>

                                    </ul>

                                    <div class="row filter-2">

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casinolive1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-3"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-3"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-3"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-3"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-3"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casinolive1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-4"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-4"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-4"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-4"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-4"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casinolive1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-9"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-9"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-9"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-9"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-9"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casinolive1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-1"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-1"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-1"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-1"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-1"></span>

                                            </div>

                                            

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casinolive1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-2"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-2"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-2"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-2"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-2"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casinolive3">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-5"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-5"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-5"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-5"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-5"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casinolive1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-6"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-6"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-6"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-6"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-6"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casinolive1">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-7"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-7"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-7"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-7"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-7"></span>

                                            </div>

                                            

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casinolive3">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-8"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-8"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-8"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-8"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-8"></span>

                                            </div>

                                        </div>

                                        <div class="col-12 col-md-6 col-lg-4" data-filter="casinolive2">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-10"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-10"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-10"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-10"></span>

                                                <span class="logo-slot logo-carousel-slot logo-casinolive-10"></span>

                                            </div>

                                        </div>

                                        

                                    </div>

                                </div>

                            </div>

                        </div>

                    </section>

                </div>

                <div class="tab-pane fade" id="pills-poker" role="tabpanel" aria-labelledby="pills-poker-tab">

                    <div class="container">

                        <div class="row">

                            <div class="col-12 col-md-6">

                                <div class="about-section wow animated fadeInLeft">

                                    <h2>Juego De Poker En Vivo</h2>

                                    <p>

                                        En estos últimos años la capacidad de la persona evoluciono para demostrar

                                        quién es el más hábil y astuto para participar en los eventos en red de poker

                                        con todas las bondades que te brinda nuestra plataforma en tiempo real.

                                        los apostadores ahora entrenan como atletas sus mentes y sus capacidades

                                        para burlar a sus oponentes, hacer saber quién es el más agresivo en cada partida.<br><br>



                                        - 24/7 de eventos y apuestas.<br>

                                        - Salas disponibles las 24 horas.<br>

                                        - Usuarios de apuesta.<br>

                                        - APP.<br>

                                        - API.<br>

                                    </p>

                                </div>

                            </div>

                            <div class="col-12 col-md-6">

                                <img class="img-fluid wow animated fadeInRight" src="images/pokers.png" alt="">

                            </div>

                        </div>

                    </div>

                    <section class="casino-slot-bg">

                        <div class="container">

                            <div class="row">

                                <div class="col-12">

                                    <h2 class="section-title full animated wow fadeInDown">Nuestro Proveedores</h2>

                                    <div class="row justify-content-center">

                                        <div class="col-12 col-md-6 col-lg-4">

                                            <div class="logo-evento">

                                                <span class="logo-slot logo-carousel-slot logo-poker-2"></span>

                                                <span class="logo-slot logo-carousel-slot logo-poker-2"></span>

                                                <span class="logo-slot logo-carousel-slot logo-poker-2"></span>

                                                <span class="logo-slot logo-carousel-slot logo-poker-2"></span>

                                                <span class="logo-slot logo-carousel-slot logo-poker-2"></span>

                                            </div>

                                        </div>

                                      

                                    </div>

                                </div>

                            </div>

                        </div>

                    </section>

                </div>

              </div>

        </section>

    </section>

    <section class="contact-bg">              

        <div class="container">

            <h3 class="pb-5 contact-title">CONTÁCTANOS</h3>

            <form method="post" action="enviar.php">

                <div class="form-row">

                    <div class="col-12 col-md input-effect ml-0">

                        <input name="nombre" class="effect-16" type="text" required>

                        <label>NOMBRE Y APELLIDO</label>

                        <span class="focus-border"></span>

                    </div>

                    <div class="col-12 col-md input-effect ml-0 mr-0">

                        <input name="correo" class="effect-16" type="text" placeholder="" required>

                        <label>CORREO</label>

                        <span class="focus-border"></span>

                    </div>

                    <div class="col-12 col-md input-effect mr-0">

                        <input name="telefono" class="effect-16" type="text" placeholder="" required>

                        <label>TELÉFONO</label>

                        <span class="focus-border"></span>

                    </div>

                    <div class="col-12 input-effect ml-0 mr-0 mt-5">

                        <input name="mensaje" class="effect-16" type="text" placeholder="" required>

                        <label>MENSAJE</label>

                        <span class="focus-border"></span>

                        <input type="submit" class="btn-contact" name="enviar" value="Enviar">

                    </div>

                </div>

            </form>

        </div>

        

        <div class="footer-bottom">

            <div class="if-SocialLinks">

                <a class="if-SocialLink" target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/UniversalSoftOficial/" title="UniversalSoft Facebook" data-track="social/facebook">

                    <svg viewBox="0 0 16 16">

                        <path d="M14.27.006H.835C.374.006 0 .38 0 .84v13.437c0 .46.373.834.834.834h7.234V9.263H6.1v-2.28h1.968v-1.68C8.068 3.35 9.26 2.286 11 2.286c.834 0 1.55.062 1.76.09v2.04H11.55c-.946 0-1.13.45-1.13 1.11V6.98h2.258l-.294 2.28h-1.964v5.85h3.85c.46 0 .833-.374.833-.835V.84c0-.46-.373-.834-.834-.834"></path>

                    </svg>

                </a>

                <a class="if-SocialLink" target="_blank" rel="noopener noreferrer" href="https://www.instagram.com/universal.race/" title="@universal.race Instagram" data-track="social/instagram">    

                    <svg viewBox="0 0 16 16">

                        <path d="M7.906 3.984C5.71 3.984 3.89 5.774 3.89 8c0 2.227 1.788 4.016 4.016 4.016 2.227 0 4.016-1.82 4.016-4.016 0-2.196-1.82-4.016-4.016-4.016zm6.525-2.447c.817.816 1.256 1.945 1.256 3.263v6.463c0 1.35-.44 2.51-1.286 3.325-.816.785-1.945 1.224-3.294 1.224h-6.4c-1.286 0-2.416-.408-3.263-1.224C.565 13.74.125 12.58.125 11.23V4.8c0-2.73 1.82-4.55 4.55-4.55h6.462c1.318 0 2.48.44 3.294 1.287zm-6.524 9.036c-1.412 0-2.573-1.16-2.573-2.573 0-1.412 1.16-2.573 2.573-2.573 1.412 0 2.572 1.16 2.572 2.573 0 1.412-1.16 2.573-2.572 2.573zM12.078 4.8c-.502 0-.91-.407-.91-.91 0-.502.408-.91.91-.91.503 0 .91.408.91.91 0 .503-.407.91-.91.91zm2.165 6.463h.063V4.8c0-.91-.314-1.694-.847-2.26-.566-.564-1.35-.846-2.29-.846H4.705c-.94 0-1.757.282-2.29.816-.565.565-.847 1.35-.847 2.29v6.43c0 .974.28 1.758.846 2.323.564.533 1.35.816 2.29.816h6.4c.94 0 1.725-.284 2.29-.817.565-.533.847-1.318.847-2.29z"></path>

                    </svg>

                </a>

                <a class="if-SocialLink" target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/company/universal-soft" title="universal-soft Linkedin" data-track="social/linkedin"> 

                    <svg viewBox="0 0 16 16">

                        <path d="M0 1.908C0 2.47.18 2.935.54 3.3c.36.366.825.55 1.395.55h.04c.585 0 1.06-.184 1.425-.55.365-.365.547-.83.547-1.392 0-.546-.183-1-.55-1.363C3.028.182 2.554 0 1.973 0 1.404 0 .932.183.56.55.185.914 0 1.367 0 1.91v-.002zM.47 16h3.295V5H.47v11zM5.57 5h3.254l.12 1.622c.76-1.218 1.898-1.826 3.11-1.826 1.198 0 2.156.435 2.873 1.306.717.87 1.075 2.068 1.075 3.59V16h-3.292v-5.962c0-.798-.162-1.404-.486-1.82-.324-.414-.91-.622-1.455-.622-.54 0-.98.186-1.318.558-.34.37-.503.884-.508 1.54V16H5.648V7.605L5.568 5z"></path>

                    </svg>

                </a>

                <a class="if-SocialLink" target="_blank" rel="noopener noreferrer" href="https://issuu.com/universalsoft/docs/revista" title="universal-soft on Issuu" data-track="social/issuu">

                    <svg viewBox="0 0 16 16">

                        <path fill-rule="evenodd" d="M7.98 10.025c-1.118-.01-2.016-.926-2.005-2.044.01-1.118.925-2.016 2.044-2.005 1.117.01 2.016.926 2.005 2.044-.01 1.118-.926 2.016-2.044 2.005zM8 0c4.418 0 8 3.582 8 8s-3.582 8-8 8-8-3.582-8-8V.665C0 .3.3 0 .665 0H8zM2.504 7.89c-.03 3.066 2.434 5.577 5.502 5.606 3.067.03 5.578-2.435 5.607-5.502.03-3.067-2.435-5.577-5.502-5.606-3.067-.03-5.577 2.434-5.606 5.502zM8.038 4c2.21.02 3.983 1.83 3.962 4.038-.02 2.21-1.83 3.983-4.038 3.962C5.752 11.98 3.98 10.17 4 7.962 4.02 5.752 5.83 3.98 8.038 4z"></path>

                    </svg>

                </a>

            </div>

            <p class="footer-p">© Universal soft. Todos los derechos reservados 2018 - 2020.

                <a href="#">Empleos | </a>

                <a href="#">Términos y condiciones | </a>

                <a href="#">Política de privacidad</a>

            </p>

            

        </div>

    </section>



    <a class="scroll-top" href="#scrolltop"><i class="fas fa-chevron-up"></i></a>



 <!-- loader -->

 <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>



    <script src="js/jquery.min.js"></script>

    <script src="js/jquery-migrate-3.0.1.min.js"></script>

    <script src="js/popper.min.js"></script>

    <script src="js/bootstrap.min.js"></script>

    <script src="js/jquery.easing.1.3.js"></script>

    <script src="js/jquery.waypoints.min.js"></script>

    <script src="js/jquery.stellar.min.js"></script>

    <script src="js/jquery.animateNumber.min.js"></script>

    <script src="js/bootstrap-datepicker.js"></script>

    <script src="js/jquery.timepicker.min.js"></script>

    <script src="js/owl.carousel.min.js"></script>

    <script src="js/jquery.magnific-popup.min.js"></script>

    <script src="js/scrollax.min.js"></script>

    <script src="js/mystyles.js"></script>

    <script src="js/main.js"></script> 

  </body>

</html>