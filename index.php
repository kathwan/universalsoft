<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Universal Soft</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/x-icon" href="images/favicon.png">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">
    <link rel="stylesheet" href="fonts/fontawesome/css/all.css">
    <link rel="stylesheet" href="css/style.css?=v2">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-180697465-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-180697465-1');
    </script>

  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark no-desktop">
        <a class="navbar-brand" href="index.php"><img class="d-inline-block align-top img-fluid" src="images/logo.png" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menucontrol" aria-controls="menucontrol" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="menucontrol">
          <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <div class="header-line">
                    <a class="header-span" href="about.php">
                        <span>ACERCA</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <div class="header-line">
                    <a class="header-span" href="services.php">
                        <span>SERVICIOS</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <div class="header-line">
                    <a class="header-span" href="product.php">
                        <span>PRODUCTOS</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <div class="header-line">
                    <a class="header-span" href="systems.php">
                        <span>SISTEMAS</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <div class="header-line">
                    <a class="header-span" href="news.php">
                        <span>NOTICIAS</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <div class="header-line">
                    <a id="#" class="header-span" href="#contact">
                        <span>CONTACTO</span>
                    </a>
                </div>
            </li>
            <li class="nav-item">
                <div class="header-line">
                    <select class="header-select" name="" id="">
                        <option value="es">ES</option>
                        <option value="eu">EN</option>
                    </select>
                </div>
            </li>
          </ul>
        </div>
    </nav>
    <nav class="header no-movil">
        <div class="header-div">
            <div class="header-w28">
                <a href="index.php"><img class="img-fluid" src="images/logo.png" alt=""></a>
            </div>
            <div class="header-line">
                <a class="header-span" href="about.php">
                    <span>ACERCA</span>
                </a>
            </div>
            <div class="header-line">
                <a class="header-span" href="product.php">
                    <span>PRODUCTOS</span>
                </a>
            </div>
            <div class="header-line">
                <a class="header-span" href="services.php">
                    <span>SERVICIOS</span>
                </a>
            </div>
            <div class="header-line">
                <a class="header-span" href="systems.php">
                    <span>SISTEMAS</span>
                </a>
            </div>
            <div class="header-line">
                <a class="header-span" href="news.php">
                    <span>NOTICIAS</span>
                </a>
            </div>
            <div class="header-line">
                <!-- <a id="#" class="header-span" href="#" data-toggle="modal" data-target="#modalNoticia"> -->
                <a id="#" class="header-span" href="#contact"> <span>CONTACTO</span>
                </a>
            </div>
            <div class="header-line">
                <select class="header-select" name="" id="">
                    <option value="es">ES</option>
                    <option value="eu">EN</option>
                </select>
            </div>
        </div>
    </nav>
    <section class="body-bg">
        <div class="position-relative">
            <div class="owl-carousel owl-theme carousel2">
                <div class="item">
                    <div class="large-header">
                        <canvas class="demo-canvas" width="1280" height="840"></canvas>
                    </div>
                    <!-- <div class="text-slider">
                        <h3 class="text-slider-p">Emociones </h3>
                        <span class="text-slider-span">DIARIAS</span>
                        <div class="btn-div-slider">
                            <button id="products" class="btn btn-text">Click aca para ver mas</button>
                        </div>
                    </div> -->
                    <!-- <div class="text-slider no-desktop">
                        <h3 class="text-slider-p">EMOCIONES</h3>
                        <span class="text-slider-span">DIARIAS</span>
                        <p class="text-slider-video">EN VIVO</p>
                    </div> -->
                    <img class="img-fluid h-100 no-movil" src="images/navidad-bg.png" alt="">
                    <img class="img-fluid h-100 no-desktop" src="images/mobile-bg.png" alt="">
                    <!-- <video class="slider-video no-movil" width="100%" preload="auto" loop autoplay muted style="visibility: visible; width: 100%;" poster="images/navidad-bg.png">
                        <source src="" type="video/mp4">
                    </video> -->
                </div>
            </div>
            <a href="#" class="btn btn-success btn-circle btn-position-ab btn-lg" data-toggle="modal" data-target="#modalVideo">
                <i class="fas fa-pen"></i>
            </a>
        </div>    
        <section class="about-bg">
            <div class="container">
                <div class="row no-gutters">
                    <div class="col-12 col-md-4">
                        <img class="img-fluid" src="images/esfera.png" alt="">
                    </div>
                    <div class="col-12 col-md-8">
                        <div class="about-div">
                            <span class="about-p">Sobre nosotros</span>
                            <span class="focus-border"></span>
                        </div>
                        <div>
                            <p>
                               Somos una empresa joven dedicada a la operación y desarrollo 
                               de contenido de juegos de azar. Iniciando operaciones en Perú desde el año 2017 y 
                               nos hemos expandido rápidamente en todo el mercado latinoamericano, 
                               brindando soluciones efectivas a todos nuestros clientes.<br><br>

                               Nuestro permanente compromiso y dedicación, nos permite brindar productos
                               de alta calidad posicionándonos como la empresa de mayor proyección y crecimiento en 
                               la región, listos para ingresar a los mercados europeos y de África 
                               consolidando nuestra expansión.<br><br>

                               Acompañamos y asesoramos a todos nuestros clientes, en todos los niveles de desarrollo
                               desde la planificación hasta la implementación de nuestras plataformas en sus locales y/o 
                               páginas de apuestas propiciando el crecimiento e impulso de sus negocios.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" class="btn btn-success btn-circle btn-position-ab btn-lg" data-toggle="modal" data-target="#modalAbout">
                <i class="fas fa-pen"></i>
            </a>
        </section>
        <!-- <section class="services-bg">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="text-center position-relative">
                            <div class="services-img">
                                <img class="img-fluid" src="images/tablet.png" alt="">
                            </div>
                            <div id="servimg" class="services-mirror" style="background-image: url('images/tablet.png'); opacity: 0; transition: all 0.4s ease 0s;">
                                <img class="img-fluid" src="images/servi1.png" alt="">
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="about-div services-div">
                            <span class="about-p text-center">Servicios</span>
                            <span class="focus-border"></span>
                        </div>
                        <div class="services-flex">
                            <div id="serv1" class="services-w">
                                <div class="services-btn">Contrato de asociación en participación</div>
                            </div>
                            <div id="serv2" class="services-w">
                                <div class="services-btn">Asesoramiento legal para pagos de impuestos</div>
                            </div>
                            <div id="serv3" class="services-w">
                                <div class="services-btn">Plataformas de apuestas deportivas & <br> apuestas virtuales en general</div>
                            </div>
                            <div id="serv4" class="services-w">
                                <div class="services-btn">Capacitación física y online para <br> administración para cajeros</div>
                            </div>
                            <div id="serv5" class="services-w">
                                <div class="services-btn">Soporte técnico online 24/7</div>
                            </div>
                            <div id="serv6" class="services-w">
                                <div class="services-btn">Soporte físico para instalación</div>
                            </div>
                            <div id="serv7" class="services-w">
                                <div class="services-btn">Diseño y marketing para su negocio</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="new-btn-link">
                            <a role="button" href="" class="btn services-btn-link2">Ver Servicios</a>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
        <section class="servi-bg">
            <div class="clientes-title about-div ">
                <p>Nuestros Servicios</p>
            </div>
            <div class="container-fluid" style="width: 90%;">
                <div class="row">
                    <div class="col-12 col-sm-6 col-lg-4">
                        <div class="item new-w">
                            <div class="new-noticia text-center">
                                <img class="img-fluid" style="width: 30%;" src="images/api-index.png" alt="noticias_universal">
                            </div>
                            <p class="new-title" style="font-size:1.4rem">API</p>
                            <p class="new-content ">
                                Contamos con una integración compatible y elegible, permitiendo usar todos los
                                navegadores de cualquier plataforma, También es adecuado para Windows, Mac OS,
                                Linux, Android, Windows Phone e iOS.
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-4">
                        <div class="item new-w">
                            <div class="new-noticia text-center">
                                <img class="img-fluid" style="width: 30%;" src="images/white-label-index.png" alt="noticias_universal">
                            </div>
                            <p class="new-title" style="font-size:1.4rem">White Label</p>
                            <p class="new-content ">
                                Nuestra poderosa y amplia solución de marca blanca brinda la oportunidad de éxito
                                en cada mercado exclusivo bajo su propia marca, adaptada a las necesidades de 
                                cada cliente.
                
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-4">
                        <div class="item new-w">
                            <div class="new-noticia text-center">
                                <img class="img-fluid" style="width: 30%;" src="images/gestion-certificados-index.png" alt="noticias_universal">
                            </div>
                            <p class="new-title" style="font-size:1.4rem">Gestión de Certificados</p>
                            <p class="new-content ">
                                Todas nuestras integraciones cuentan
                                con los respectivos protocolos de seguridad y cifrado complejo (SSL y TSL).
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-4">
                        <div class="item new-w">
                            <div class="new-noticia text-center">
                                <img class="img-fluid" style="width: 30%;" src="images/novedades-index.png" alt="noticias_universal">
                            </div>
                            <p class="new-title" style="font-size:1.4rem">Novedades Exclusivas</p>
                            <p class="new-content ">
                                Todas nuestras soluciones integradas, bajo la plataforma cuentan con 
                                características únicas a todo nivel convirtiéndola en una opción
                                altamente viable que ofrecer nuestros servicios al rededor del mundo.
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-4">
                        <div class="item new-w">
                            <div class="new-noticia text-center">
                                <img class="img-fluid" style="width: 30%;" src="images/b2b.png" alt="noticias_universal">
                            </div>
                            <p class="new-title" style="font-size:1.4rem">B2B</p>
                            <p class="new-content ">
                                Nuestra amplia plataforma, nos permite brindar soluciones integradas 
                                a cualquier empresa al rededor del mundo (all in one solution), 
                                en todo momento y con una sostenibilidad compartida, la estabilidad 
                                de nuestros sitemas, otorgan a empresas de varios continentes la 
                                rentabilidad necesaria para generar ganancias y satisfacer las necesidades 
                                de sus clientes finales.
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-4">
                        <div class="item new-w">
                            <div class="new-noticia text-center">
                                <img class="img-fluid" style="width: 30%;" src="images/b2c.png" alt="noticias_universal">
                            </div>
                            <p class="new-title" style="font-size:1.4rem">B2C</p>
                            <p class="new-content ">
                                Nuestra variedad de productos en todos los rubros de las apuestas
                                deportivas, nos convierten en un objetivo ideal para el cliente final,
                                ya que ofrecemos las mas amplia y diversa carta de juegos que van desde
                                las apuestas de carreras hipicas hasta casino en vivo, y tragamonedas.
                            </p>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="new-btn-link">
                            <a role="button" href="services.php" class="btn new-btn-link2">Ver Servicios</a>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" class="btn btn-success btn-circle btn-position-ab btn-lg" data-toggle="modal" data-target="#modalService">
                <i class="fas fa-pen"></i>
            </a>
        </section>
        <section class="events-bg">
            <div class="text-center">
                <div class="logo-events">
                    <img class="img-fluid" src="images/universal.png" alt="">
                </div>
                <div class="timeline-Header">
                    <p>NUESTROS PRODUCTOS</p>
                </div>
            </div>
            <div class="section-product">
                <div class="container pt-5">
                    <div class="row no-gutters justify-content-center align-items-center pb-5">
                        <div class="col-12 col-md-6">
                            <div class="item-image" >
                                <img class="img-fluid ml-left" src="images/product1.png" alt="">
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="item-text">
                                <div class="text-content">
                                    <h3>APUESTAS HÍPICAS</h3>
                                    <strong>Variedad de Apuestas</strong>
                                    <p>
                                        Software integral multifuncional de apuestas hípicas para su propio negocio.<br><br>

                                        La gran ventaja que ofrece nuestro servicio son los miles de eventos de todos los
                                        deportes en vivo para apostar obteniendo grandes ganancias respaldado con un
                                        sistema de riesgo muy agresivo.<br><br>
                                        No solo es personalizada de acuerdo a las necesidades del cliente si no también
                                        tiene una arquitectura altamente escalable y muy sencilla.<br>
                                        El software de apuestas deportivas de universal soft es una platforma eficiente y
                                        totalmente amigable con la administración que brinda las mejores probabilidades
                                        del mercado con un solo click.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row no-gutters justify-content-center align-items-center pb-5">
                        <div class="col-12 col-md-6 order-2 order-md-1">
                            <div class="item-text-left">
                                <div class="text-content">
                                    <h3>APUESTAS VIRTUALES</h3>
                                    <strong>Plataforma mas  Variada</strong>
                                    <p>
                                        Manejamos un exclusivo y gran paquete de los mas variados juegos reales y en 3d.
                                        Universal soft con la más alta experiencia siempre formando alianzas firmes para
                                        proveer a nuestros clientes el complemento perfecto para las tiendas e integracion
                                        de los juegos en línea a nivel mundial.<br><br>
                                        Tenemos:
                                    </p>
                                    <ul>
                                        <li>- Pelea de Gallos.</li>
                                        <li>- Carrera de Perros HD.</li>
                                        <li>- Carreraa de Caballos HD.</li>
                                        <li>- Penalty HD.</li>
                                        <li>- Keno</li>
                                        <li>- Spin to Win.</li>
                                        <li>- Futbol Internacional</li>
                                        <li>- Pregrabado en HD.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 order-1 order-md-2">
                            <div class="item-image">
                                <img class="img-fluid ml-right" src="images/product2.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="row no-gutters justify-content-center align-items-center pb-5">
                        <div class="col-12 col-md-6">
                            <div class="item-image" >
                                <img class="img-fluid ml-left" src="images/product3.png" alt="">
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="item-text">
                                <div class="text-content">
                                    <h3>CASINO</h3>
                                    <strong>Jackpot Integrado</strong>
                                    <p>
                                        Nos da gusto, placer fabricar Software y proveer nuestro propio desarrollo totalmente
                                        automatizado y virtual.<br><br>
                                        Nuestros clientes podrán jugar en tiempo real y viviran una experiencia
                                        totalmente distinta ya que fue diseñado para todo tipo de jugadores.<br><br>
                                        Especificaciones:
                                    </p>
                                    <ul>
                                        <li>- Gabinete diseñado en metal y madera.</li>
                                        <li>- Sistema hibrido.</li>
                                        <li>- Monitor de la mesa principal 55”.</li>
                                        <li>- Joysticks para 6 jugadores (versión sencilla).</li>
                                        <li>- Monitores de 15.6 táctiles (versión full).</li>
                                        <li>- Aceptador de billetes jcm.</li>
                                        <li>- Impresora gen2.</li>
                                        <li>- Online.</li>
                                        <li>- Sistema SAS. <span class="btn-question question1" data-toggle="tooltip" data-placement="right" title="a"><i class="fa fa-question"></i></span></li>
                                        <li>- Sistema tito. <span class="btn-question question2" data-toggle="tooltip" data-placement="right" title="b"><i class="fa fa-question"></i></span></li>
                                        <li>- Certificación RNG. <span class="btn-question question3" data-toggle="tooltip" data-placement="right" title="c"><i class="fa fa-question"></i></span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" class="btn btn-success btn-circle btn-position-ab btn-lg" data-toggle="modal" data-target="#modalProduct">
                <i class="fas fa-pen"></i>
            </a>
        </section>
        <section class="clientes-bg">
            <div class="clientes-title">
                <p>Nuestros Operadores</p>
            </div>
            <div class="container pt-5 pb-5">
                <div class="row">
                    
                    <div class="col-6 col-md-3">
                        <ul ontouchstart>
                            <li>
                              <div class='link'>
                                <a></a>
                                <a></a>
                                <a></a>
                                <a></a>
                                <div class='cube cliente'>
                                  <div></div>
                                  <div></div>
                                  <div></div>
                                  <div></div>
                                  <div class="cliente-img1">
                                    <!-- <svg viewbox='0 0 85 85'>
                                      <path d='M82.5,29.1666667 L42.5,2.5 L2.5,29.1666667 L2.5,55.8922001 L42.5,82.5 L82.5,55.8922001 L82.5,29.1666667 Z M42.5,2.9120677 L42.5,29.1666667 L42.5,2.9120677 Z M42.5,55.8922001 L42.5,82.5 L42.5,55.8922001 Z M2.5,29.1666667 L2.5,55.8922001 L42.5,29.1666667 L82.5,55.8922001 L82.5,29.1666667 L42.5,55.8922001 L2.5,29.1666667 Z'></path>
                                    </svg> -->
                                  </div>
                                  <div><span class="cube-title">Universal Race</span><p class="cube-text">Sistema de apuestas Hípica universalsoft peru</p></div>
                                </div>
                              </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-6 col-md-3">
                        <ul ontouchstart>
                            <li>
                              <div class='link'>
                                <a></a>
                                <a></a>
                                <a></a>
                                <a></a>
                                <div class='cube cliente'>
                                  <div></div>
                                  <div></div>
                                  <div></div>
                                  <div></div>
                                  <div class="cliente-img2">
                                    <!-- <svg viewbox='0 0 85 85'>
                                      <path d='M82.5,29.1666667 L42.5,2.5 L2.5,29.1666667 L2.5,55.8922001 L42.5,82.5 L82.5,55.8922001 L82.5,29.1666667 Z M42.5,2.9120677 L42.5,29.1666667 L42.5,2.9120677 Z M42.5,55.8922001 L42.5,82.5 L42.5,55.8922001 Z M2.5,29.1666667 L2.5,55.8922001 L42.5,29.1666667 L82.5,55.8922001 L82.5,29.1666667 L42.5,55.8922001 L2.5,29.1666667 Z'></path>
                                    </svg> -->
                                  </div>
                                  <div><span class="cube-title">SportBet Peru</span><p class="cube-text">Empresa peruana proveedora de software iGaming</p></div>
                                </div>
                              </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-6 col-md-3">
                        <ul ontouchstart>
                            <li>
                              <div class='link'>
                                <a></a>
                                <a></a>
                                <a></a>
                                <a></a>
                                <div class='cube cliente'>
                                  <div></div>
                                  <div></div>
                                  <div></div>
                                  <div></div>
                                  <div class="cliente-img3">
                                    <!-- <svg viewbox='0 0 85 85'>
                                      <path d='M82.5,29.1666667 L42.5,2.5 L2.5,29.1666667 L2.5,55.8922001 L42.5,82.5 L82.5,55.8922001 L82.5,29.1666667 Z M42.5,2.9120677 L42.5,29.1666667 L42.5,2.9120677 Z M42.5,55.8922001 L42.5,82.5 L42.5,55.8922001 Z M2.5,29.1666667 L2.5,55.8922001 L42.5,29.1666667 L82.5,55.8922001 L82.5,29.1666667 L42.5,55.8922001 L2.5,29.1666667 Z'></path>
                                    </svg> -->
                                  </div>
                                  <div><span class="cube-title">Bets America</span><p class="cube-text">Plataforma online de apuestas deportivas </p></div>
                                </div>
                              </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-6 col-md-3">
                        <ul ontouchstart>
                            <li>
                              <div class='link'>
                                <a></a>
                                <a></a>
                                <a></a>
                                <a></a>
                                <div class='cube cliente'>
                                  <div></div>
                                  <div></div>
                                  <div></div>
                                  <div></div>
                                  <div class="cliente-img4">
                                    <!-- <svg viewbox='0 0 85 85'>
                                      <path d='M82.5,29.1666667 L42.5,2.5 L2.5,29.1666667 L2.5,55.8922001 L42.5,82.5 L82.5,55.8922001 L82.5,29.1666667 Z M42.5,2.9120677 L42.5,29.1666667 L42.5,2.9120677 Z M42.5,55.8922001 L42.5,82.5 L42.5,55.8922001 Z M2.5,29.1666667 L2.5,55.8922001 L42.5,29.1666667 L82.5,55.8922001 L82.5,29.1666667 L42.5,55.8922001 L2.5,29.1666667 Z'></path>
                                    </svg> -->
                                  </div>
                                  <div><span class="cube-title">Novobets</span><p class="cube-text">Plataforma online de apuestas deportivas peruana</p></div>
                                </div>
                              </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-6 col-md-3">
                        <ul ontouchstart>
                            <li>
                              <div class='link'>
                                <a></a>
                                <a></a>
                                <a></a>
                                <a></a>
                                <div class='cube cliente'>
                                  <div></div>
                                  <div></div>
                                  <div></div>
                                  <div></div>
                                  <div class="cliente-img5">
                                    <!-- <svg viewbox='0 0 85 85'>
                                      <path d='M82.5,29.1666667 L42.5,2.5 L2.5,29.1666667 L2.5,55.8922001 L42.5,82.5 L82.5,55.8922001 L82.5,29.1666667 Z M42.5,2.9120677 L42.5,29.1666667 L42.5,2.9120677 Z M42.5,55.8922001 L42.5,82.5 L42.5,55.8922001 Z M2.5,29.1666667 L2.5,55.8922001 L42.5,29.1666667 L82.5,55.8922001 L82.5,29.1666667 L42.5,55.8922001 L2.5,29.1666667 Z'></path>
                                    </svg> -->
                                  </div>
                                  <div><span class="cube-title">DoradoBet</span><p class="cube-text">Plataforma online de apuestas deportivas y casino online.</p></div>
                                </div>
                              </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-6 col-md-3">
                        <ul ontouchstart>
                            <li>
                              <div class='link'>
                                <a></a>
                                <a></a>
                                <a></a>
                                <a></a>
                                <div class='cube cliente'>
                                  <div></div>
                                  <div></div>
                                  <div></div>
                                  <div></div>
                                  <div class="cliente-img6">
                                    <!-- <svg viewbox='0 0 85 85'>
                                      <path d='M82.5,29.1666667 L42.5,2.5 L2.5,29.1666667 L2.5,55.8922001 L42.5,82.5 L82.5,55.8922001 L82.5,29.1666667 Z M42.5,2.9120677 L42.5,29.1666667 L42.5,2.9120677 Z M42.5,55.8922001 L42.5,82.5 L42.5,55.8922001 Z M2.5,29.1666667 L2.5,55.8922001 L42.5,29.1666667 L82.5,55.8922001 L82.5,29.1666667 L42.5,55.8922001 L2.5,29.1666667 Z'></path>
                                    </svg> -->
                                  </div>
                                  <div><span class="cube-title">TimerBet</span><p class="cube-text">Plataforma online de apuestas deportivas y casino online.</p></div>
                                </div>
                              </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-6 col-md-3">
                        <ul ontouchstart>
                            <li>
                              <div class='link'>
                                <a></a>
                                <a></a>
                                <a></a>
                                <a></a>
                                <div class='cube cliente'>
                                  <div></div>
                                  <div></div>
                                  <div></div>
                                  <div></div>
                                  <div class="cliente-img7">
                                    <!-- <svg viewbox='0 0 85 85'>
                                      <path d='M82.5,29.1666667 L42.5,2.5 L2.5,29.1666667 L2.5,55.8922001 L42.5,82.5 L82.5,55.8922001 L82.5,29.1666667 Z M42.5,2.9120677 L42.5,29.1666667 L42.5,2.9120677 Z M42.5,55.8922001 L42.5,82.5 L42.5,55.8922001 Z M2.5,29.1666667 L2.5,55.8922001 L42.5,29.1666667 L82.5,55.8922001 L82.5,29.1666667 L42.5,55.8922001 L2.5,29.1666667 Z'></path>
                                    </svg> -->
                                  </div>
                                  <div><span class="cube-title">LatinosBet</span><p class="cube-text">Plataforma online de apuestas deportivas y casino online.</p></div>
                                </div>
                              </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-6 col-md-3">
                        <ul ontouchstart>
                            <li>
                              <div class='link'>
                                <a></a>
                                <a></a>
                                <a></a>
                                <a></a>
                                <div class='cube cliente'>
                                  <div></div>
                                  <div></div>
                                  <div></div>
                                  <div></div>
                                  <div class="cliente-img8">
                                    <!-- <svg viewbox='0 0 85 85'>
                                      <path d='M82.5,29.1666667 L42.5,2.5 L2.5,29.1666667 L2.5,55.8922001 L42.5,82.5 L82.5,55.8922001 L82.5,29.1666667 Z M42.5,2.9120677 L42.5,29.1666667 L42.5,2.9120677 Z M42.5,55.8922001 L42.5,82.5 L42.5,55.8922001 Z M2.5,29.1666667 L2.5,55.8922001 L42.5,29.1666667 L82.5,55.8922001 L82.5,29.1666667 L42.5,55.8922001 L2.5,29.1666667 Z'></path>
                                    </svg> -->
                                  </div>
                                  <div><span class="cube-title">Wings</span><p class="cube-text">Plataforma online de apuestas deportivas y casino online.</p></div>
                                </div>
                              </div>
                            </li>
                        </ul>
                    </div>
                    <div class="col-6 col-md-3">
                        <ul ontouchstart>
                            <li>
                              <div class='link'>
                                <a></a>
                                <a></a>
                                <a></a>
                                <a></a>
                                <div class='cube cliente'>
                                  <div></div>
                                  <div></div>
                                  <div></div>
                                  <div></div>
                                  <div class="cliente-img9">
                                    <!-- <svg viewbox='0 0 85 85'>
                                      <path d='M82.5,29.1666667 L42.5,2.5 L2.5,29.1666667 L2.5,55.8922001 L42.5,82.5 L82.5,55.8922001 L82.5,29.1666667 Z M42.5,2.9120677 L42.5,29.1666667 L42.5,2.9120677 Z M42.5,55.8922001 L42.5,82.5 L42.5,55.8922001 Z M2.5,29.1666667 L2.5,55.8922001 L42.5,29.1666667 L82.5,55.8922001 L82.5,29.1666667 L42.5,55.8922001 L2.5,29.1666667 Z'></path>
                                    </svg> -->
                                  </div>
                                  <div><span class="cube-title">DrBet</span><p class="cube-text">Plataforma online de apuestas deportivas y casino online.</p></div>
                                </div>
                              </div>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
            <a href="#" class="btn btn-success btn-circle btn-position-ab btn-lg" data-toggle="modal" data-target="#modalOperador">
                <i class="fas fa-pen"></i>
            </a>
        </section>
        <section class="num-clientes-bg">
            <div class="num-clientes-div">
                <div class="container">
                    <div class="row">
                        <div class="col-6 col-md-3 text-center">
                            <img class="img-fluid" src="images/icons/1.png" alt="">
                            <p class="number" data-number="1597023">1597023</p>
                            <span class="text-number">TICKET POR MES</span>
                        </div>
                        <div class="col-6 col-md-3 text-center">
                            <img class="img-fluid" src="images/icons/2.png" alt="">
                            <p class="number" data-number="576">576</p>
                            <span class="text-number">USUARIOS POR DIA</span>
                        </div>
                        <div class="col-6 col-md-3 text-center">
                            <img class="img-fluid" src="images/icons/3.png" alt="">
                            <p class="number" data-number="8">8</p>
                            <span class="text-number">PAÍSES</span>
                        </div>
                        <div class="col-6 col-md-3 text-center">
                            <img class="img-fluid" src="images/icons/4.png" alt="">
                            <p class="number" data-number="3">3</p>
                            <span class="text-number">AÑOS DE EXPERIENCIA</span>
                        </div>
                        <div class="col-12">
                            <div class="about-div services-div pt-5">
                                <span class="about-p text-center">Universal Soft</span>
                                <span class="focus-border"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clients-country">
                <div class="d-flex-center pb-3">
                    <div class="flags-clients">
                        <div class="flag peru wave"><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div></div>
                        <span class="separator-flag">perú</span>
                    </div>
                    <div class="flags-clients">
                        <div class="flag brazil wave"><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div></div>
                        <span class="separator-flag">brasil</span>
                    </div>
                    <div class="flags-clients">
                        <div class="flag chile wave"><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div></div>
                         <span class="separator-flag">chile</span>
                    </div>
                    <div class="flags-clients">
                        <div class="flag ecuador wave"><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div></div>
                        <span class="separator-flag br-0">ecuador</span>
                    </div>
                </div>
                <div class="d-flex-center">
                    <div class="flags-clients">
                        <div class="flag venezuela wave"><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div></div>
                        <span class="separator-flag">Venezuela</span>
                    </div>
                    <div class="flags-clients">
                        <div class="flag colombia wave"><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div></div>
                        <span class="separator-flag">Colombia</span>
                    </div>
                    <div class="flags-clients">
                        <div class="flag tunez wave"><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div><div class="strip"></div></div>
                        <span class="separator-flag br-0">North Africa</span>
                    </div>
                </div>
            </div>
            <a href="#" class="btn btn-success btn-circle btn-position-ab btn-lg" data-toggle="modal" data-target="#modalClientes">
                <i class="fas fa-pen"></i>
            </a>
        </section>
        <section class="new-bg">
            <div class="about-div services-div pt-5">
                <span class="about-p text-center">Noticias importantes</span>
                <span class="focus-border"></span>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="owl-carousel owl-theme carousel3">
                            <div class="item new-w">
                                <div class="new-noticia">
                                    <img class="img-fluid" src="images/noticia1.png" alt="noticias_universal">
                                    <div class="noticia-link">
                                        <a role="button" class="btn btn-new" href="news.php">Ver noticias completa</a>
                                        <span class="btn-new-span">haz click</span>
                                    </div>
                                </div>
                                <p class="new-title">SAGSE TALKS MÉXICO</p>
                                <span class="new-content">Universal Race se alista para dar sus primeros pasos en el mercado de Centro America,
                                    además hace presencia como patrocinador en SAGSE Talks México</span>
                            </div>
                            <div class="item new-w">
                                <div class="new-noticia">
                                    <img class="img-fluid" src="images/noticia2.png" alt="noticias_universal">
                                    <div class="noticia-link">
                                        <a role="button" class="btn btn-new" href="news.php">Ver noticias completa</a>
                                        <span class="btn-new-span">haz click</span>
                                    </div>
                                </div>
                                <p class="new-title">GAT EXPO GAMING & TECHNOLOGY</p>
                                <span class="new-content">En un reciente comunicado, los responsables del evento anunciaron que posponen su
                                    edición 2020 de forma presencial.</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="new-btn-link">
                            <a role="button" href="news.php" class="btn new-btn-link2">ver mas noticias</a>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" class="btn btn-success btn-circle btn-position-ab btn-lg" data-toggle="modal" data-target="#modalNoticia">
                <i class="fas fa-pen"></i>
            </a>
        </section>
    </section>
    <section class="contact-bg">              
        <div class="container">
            <h3 class="pb-5 contact-title">CONTÁCTANOS</h3>
            <form method="post" action="enviar.php">
                <!-- <input type="hidden" name="origen" value="index.php"> -->
                <div class="form-row">
                    <div class="col-12 col-md input-effect ml-0">
                        <input name="nombre" class="effect-16" type="text" required>
                        <label>NOMBRE Y APELLIDO</label>
                        <span class="focus-border"></span>
                    </div>
                    <div class="col-12 col-md input-effect ml-0 mr-0">
                        <input name="correo" class="effect-16" type="text" placeholder="" required>
                        <label>CORREO</label>
                        <span class="focus-border"></span>
                    </div>
                    <div class="col-12 col-md input-effect mr-0">
                        <input name="telefono" class="effect-16" type="text" placeholder="" required>
                        <label>TELÉFONO</label>
                        <span class="focus-border"></span>
                    </div>
                    <div class="col-12 input-effect ml-0 mr-0 mt-5">
                        <input name="mensaje" class="effect-16" type="text" placeholder="" required>
                        <label>MENSAJE</label>
                        <span class="focus-border"></span>
                        <input type="submit" class="btn-contact" name="enviar" value="Enviar">
                    </div>
                </div>
            </form>
            
        </div>
        
        <div class="footer-bottom">
            <div class="if-SocialLinks">
                <a class="if-SocialLink" target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/UniversalSoftOficial/" title="UniversalSoft Facebook" data-track="social/facebook">
                    <svg viewBox="0 0 16 16">
                        <path d="M14.27.006H.835C.374.006 0 .38 0 .84v13.437c0 .46.373.834.834.834h7.234V9.263H6.1v-2.28h1.968v-1.68C8.068 3.35 9.26 2.286 11 2.286c.834 0 1.55.062 1.76.09v2.04H11.55c-.946 0-1.13.45-1.13 1.11V6.98h2.258l-.294 2.28h-1.964v5.85h3.85c.46 0 .833-.374.833-.835V.84c0-.46-.373-.834-.834-.834"></path>
                    </svg>
                </a>
                <a class="if-SocialLink" target="_blank" rel="noopener noreferrer" href="https://www.instagram.com/universal.race/" title="@universal.race Instagram" data-track="social/instagram">    
                    <svg viewBox="0 0 16 16">
                        <path d="M7.906 3.984C5.71 3.984 3.89 5.774 3.89 8c0 2.227 1.788 4.016 4.016 4.016 2.227 0 4.016-1.82 4.016-4.016 0-2.196-1.82-4.016-4.016-4.016zm6.525-2.447c.817.816 1.256 1.945 1.256 3.263v6.463c0 1.35-.44 2.51-1.286 3.325-.816.785-1.945 1.224-3.294 1.224h-6.4c-1.286 0-2.416-.408-3.263-1.224C.565 13.74.125 12.58.125 11.23V4.8c0-2.73 1.82-4.55 4.55-4.55h6.462c1.318 0 2.48.44 3.294 1.287zm-6.524 9.036c-1.412 0-2.573-1.16-2.573-2.573 0-1.412 1.16-2.573 2.573-2.573 1.412 0 2.572 1.16 2.572 2.573 0 1.412-1.16 2.573-2.572 2.573zM12.078 4.8c-.502 0-.91-.407-.91-.91 0-.502.408-.91.91-.91.503 0 .91.408.91.91 0 .503-.407.91-.91.91zm2.165 6.463h.063V4.8c0-.91-.314-1.694-.847-2.26-.566-.564-1.35-.846-2.29-.846H4.705c-.94 0-1.757.282-2.29.816-.565.565-.847 1.35-.847 2.29v6.43c0 .974.28 1.758.846 2.323.564.533 1.35.816 2.29.816h6.4c.94 0 1.725-.284 2.29-.817.565-.533.847-1.318.847-2.29z"></path>
                    </svg>
                </a>
                <a class="if-SocialLink" target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/company/universal-soft" title="universal-soft Linkedin" data-track="social/linkedin"> 
                    <svg viewBox="0 0 16 16">
                        <path d="M0 1.908C0 2.47.18 2.935.54 3.3c.36.366.825.55 1.395.55h.04c.585 0 1.06-.184 1.425-.55.365-.365.547-.83.547-1.392 0-.546-.183-1-.55-1.363C3.028.182 2.554 0 1.973 0 1.404 0 .932.183.56.55.185.914 0 1.367 0 1.91v-.002zM.47 16h3.295V5H.47v11zM5.57 5h3.254l.12 1.622c.76-1.218 1.898-1.826 3.11-1.826 1.198 0 2.156.435 2.873 1.306.717.87 1.075 2.068 1.075 3.59V16h-3.292v-5.962c0-.798-.162-1.404-.486-1.82-.324-.414-.91-.622-1.455-.622-.54 0-.98.186-1.318.558-.34.37-.503.884-.508 1.54V16H5.648V7.605L5.568 5z"></path>
                    </svg>
                </a>
                <a class="if-SocialLink" target="_blank" rel="noopener noreferrer" href="https://issuu.com/universalsoft/docs/revista" title="universal-soft on Issuu" data-track="social/issuu">
                    <svg viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M7.98 10.025c-1.118-.01-2.016-.926-2.005-2.044.01-1.118.925-2.016 2.044-2.005 1.117.01 2.016.926 2.005 2.044-.01 1.118-.926 2.016-2.044 2.005zM8 0c4.418 0 8 3.582 8 8s-3.582 8-8 8-8-3.582-8-8V.665C0 .3.3 0 .665 0H8zM2.504 7.89c-.03 3.066 2.434 5.577 5.502 5.606 3.067.03 5.578-2.435 5.607-5.502.03-3.067-2.435-5.577-5.502-5.606-3.067-.03-5.577 2.434-5.606 5.502zM8.038 4c2.21.02 3.983 1.83 3.962 4.038-.02 2.21-1.83 3.983-4.038 3.962C5.752 11.98 3.98 10.17 4 7.962 4.02 5.752 5.83 3.98 8.038 4z"></path>
                    </svg>
                </a>
            </div>
            <p class="footer-p">© Universal soft. Todos los derechos reservados 2018 - 2020.
                <a href="#">Empleos | </a>
                <a href="#">Términos y condiciones | </a>
                <a href="#">Política de privacidad</a>
            </p>
            
        </div>
    </section>
    <?php
        require 'modals.php';
    ?>

    <a class="scroll-top" href="#scrolltop"><i class="fas fa-chevron-up"></i></a>

 <!-- loader -->
 <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-migrate-3.0.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.stellar.min.js"></script>
    <script src="js/jquery.animateNumber.min.js"></script>
    <script src="js/bootstrap-datepicker.js"></script>
    <script src="js/jquery.timepicker.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/scrollax.min.js"></script>
    <script src="js/mystyles.js"></script>
    <script src="js/main.js"></script> 
    <script src="js/particle.js"></script> 
  </body>
</html>