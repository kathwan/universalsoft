<div class="modal fade" id="modalVideo" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Video Banner</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div class="text-xs mb-1">Coloca el nuevo video que vas a reemplazar</div>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="video1" aria-describedby="videoinput">
                        <label class="custom-file-label" for="video1"></label>
                    </div>
                </div>
                <div class="col-12 mt-3">
                    <video class="slider-video" width="100%" preload="auto" loop="false" autoplay="false" controls="" style="visibility: visible; width: 100%;" poster="img/400x400.png">
                    <source src="img/slider2.mp4" type="video/mp4">
                    </video>
                </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success">Guardar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalAbout" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Sobre nosotros</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <label for="nosotros1" class="text-xs mb-1">Cambiar imagen</label>
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="nosotros1" aria-describedby="inputfile" lang="es">
                <label id="label-nosotros" class="custom-file-label" for="nosotros1"></label>
            </div>
            <div class="form-group pt-3">
                <label for="formGroupExampleInput" class="text-xs mb-1">Coloque un titulo</label>
                <input type="text" class="form-control" id="nosotros-titulo" placeholder="Titulo" value="">
            </div>
            <div class="form-group">
                <label for="nosotros-textarea" class="text-xs mb-1">Coloque el contenido</label>
                <textarea class="form-control" id="nosotros-textarea" rows="3"></textarea>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success">Guardar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalService" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Nuestros servicios</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col form-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="inputGroupFile" aria-describedby="inputGroupFileAddon01">
                        <label class="custom-file-label" for="inputGroupFile">Coloca su imagen</label>
                    </div>
                    <label for="formGroupExampleInput">Titulo</label>
                    <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Titulo">
            
                    <label for="formGroupExampleInput">Contenido</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>

                </div>
                
            </div>
            <div class="row">
                <div class="col-12 form-group text-right">
                    <a href="#" id="btn-servicios" class="btn btn-success btn-circle">
                        Crear
                    </a>
                    <a href="#" id="btn-servicios-delete" class="btn btn-danger btn-circle">
                        <i class="fas fa-trash"></i>
                    </a>
                </div>
            </div>
            <div class="row">
                <ul class="list-ul-mod">
                  <li>#</li>
                  <li>Imagen</li>
                  <li>Titulo</li>
                  <li>Contenido</li>
                  <li>Acciones</li>
                </ul>
                <ul class="list-ul-cont">
                  <li class="flex-start">
                    <div>1</div>
                    <div>http://localhost/universalsoft/index.php</div>
                    <div>API</div>
                    <div>
                      Contamos con una integración compatible y elegible, permitiendo usar todos los navegadores de cualquier plataforma, También es adecuado para Windows, Mac OS, Linux, Android, Windows Phone e iOS.
                    </div>
                    <div class="mt-3">
                      <a href="#" id="btn-video" class="btn btn-success btn-circle">
                          <i class="fas fa-pen"></i>
                      </a>
                      <a href="#" id="btn-video-delete" class="btn btn-danger btn-circle">
                          <i class="fas fa-trash"></i>
                      </a>
                    </div>
                  </li>
                  <li class="flex-start">
                    <div>2</div>
                    <div>http://localhost/universalsoft/index.php</div>
                    <div>White Label</div>
                    <div>
                      Nuestra poderosa y amplia solución de marca blanca brinda la oportunidad de éxito en cada mercado exclusivo bajo su propia marca, adaptada a las necesidades de cada cliente.
                    </div>
                    <div class="mt-3">
                      <a href="#" id="btn-video" class="btn btn-success btn-circle">
                          <i class="fas fa-pen"></i>
                      </a>
                      <a href="#" id="btn-video-delete" class="btn btn-danger btn-circle">
                          <i class="fas fa-trash"></i>
                      </a>
                    </div>
                  </li>
                  <li class="flex-start">
                    <div>3</div>
                    <div>http://localhost/universalsoft/index.php</div>
                    <div>Gestión de Certificados</div>
                    <div>
                      Te asesoramos en la elección de la jurisdicción y en el proceso de licenciamiento de juegos en línea, Todas nuestras integraciones cuentan con los respectivos protocolos de seguridad y cifrado complejo (SSL y TSL).
                    </div>
                    <div class="mt-3">
                      <a href="#" id="btn-video" class="btn btn-success btn-circle">
                          <i class="fas fa-pen"></i>
                      </a>
                      <a href="#" id="btn-video-delete" class="btn btn-danger btn-circle">
                          <i class="fas fa-trash"></i>
                      </a>
                    </div>
                  </li>
                </ul>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success">Guardar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalProduct" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Nuestros Productos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col form-group">
                    <div class="form-group">
                        <input type="file" class="custom-file-input" id="inputGroupFile" aria-describedby="inputGroupFileAddon01">
                        <label class="custom-file-label" for="inputGroupFile">Coloca su imagen</label>
                    </div>
                    <div class="form-group">
                      <label for="formGroupExampleInput">Titulo</label>
                      <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Titulo">
                    </div>
                    <div class="form-group">
                      <label for="formGroupExampleInput">Subtitulo</label>
                      <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Titulo">
                    </div>
                    <div class="form-group">
                      <label for="formGroupExampleInput">Contenido</label>
                      <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                </div>
                
            </div>
            <div class="row">
                <div class="col-12 form-group text-right">
                    <a href="#" id="btn-servicios" class="btn btn-success btn-circle">
                        Crear
                    </a>
                    <a href="#" id="btn-servicios-delete" class="btn btn-danger btn-circle">
                        <i class="fas fa-trash"></i>
                    </a>
                </div>
            </div>
            <div class="row">
                <ul class="list-ul-mod2">
                  <li>#</li>
                  <li>Imagen</li>
                  <li>Titulo</li>
                  <li>Subtitulo</li>
                  <li>Contenido</li>
                  <li>Acciones</li>
                </ul>
                <ul class="list-ul-cont2">
                  <li class="flex-start">
                    <div>1</div>
                    <div>http://localhost/universalsoft/index.php</div>
                    <div>APUESTAS HÍPICAS</div>
                    <div>Variedad de Apuestas</div>
                    <div>
                      Software integral multifuncional de apuestas hípicas para su propio negocio.
                      La gran ventaja que ofrece nuestro servicio son los miles de eventos de todos los deportes en vivo para apostar obteniendo grandes ganancias respaldado con un sistema de riesgo muy agresivo.
                      No solo es personalizada de acuerdo a las necesidades del cliente si no también tiene una arquitectura altamente escalable y muy sencilla.
                      El software de apuestas deportivas de universal soft es una platforma eficiente y totalmente amigable con la administración que brinda las mejores probabilidades del mercado con un solo click.
                    </div>
                    <div class="mt-3">
                      <a href="#" id="btn-video" class="btn btn-success btn-circle">
                          <i class="fas fa-pen"></i>
                      </a>
                      <a href="#" id="btn-video-delete" class="btn btn-danger btn-circle">
                          <i class="fas fa-trash"></i>
                      </a>
                    </div>
                  </li>
                </ul>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success">Guardar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalOperador" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Nuestros Operadores</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col form-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="inputGroupFile" aria-describedby="inputGroupFileAddon01">
                        <label class="custom-file-label" for="inputGroupFile">Coloca su imagen</label>
                    </div>
                    <div class="form-group">
                      <label for="formGroupExampleInput">Titulo</label>
                      <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Titulo">
                    </div>
                    <div class="form-group">
                      <label for="formGroupExampleInput">Contenido</label>
                      <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 form-group text-right">
                    <a href="#" id="btn-servicios" class="btn btn-success btn-circle">
                      <i class="fas fa-check"></i>
                    </a>
                    <a href="#" id="btn-servicios-delete" class="btn btn-danger btn-circle">
                        <i class="fas fa-trash"></i>
                    </a>
                </div>
            </div>
            <div class="row">
                <ul class="list-ul-mod">
                  <li>#</li>
                  <li>Imagen</li>
                  <li>Titulo</li>
                  <li>Contenido</li>
                  <li>Acciones</li>
                </ul>
                <ul class="list-ul-cont">
                  <li class="flex-start">
                    <div>1</div>
                    <div>http://localhost/universalsoft/index.php</div>
                    <div>API</div>
                    <div>
                      Contamos con una integración compatible y elegible, permitiendo usar todos los navegadores de cualquier plataforma, También es adecuado para Windows, Mac OS, Linux, Android, Windows Phone e iOS.
                    </div>
                    <div class="mt-3">
                      <a href="#" id="btn-video" class="btn btn-success btn-circle">
                          <i class="fas fa-pen"></i>
                      </a>
                      <a href="#" id="btn-video-delete" class="btn btn-danger btn-circle">
                          <i class="fas fa-trash"></i>
                      </a>
                    </div>
                  </li>
                  <li class="flex-start">
                    <div>2</div>
                    <div>http://localhost/universalsoft/index.php</div>
                    <div>White Label</div>
                    <div>
                      Nuestra poderosa y amplia solución de marca blanca brinda la oportunidad de éxito en cada mercado exclusivo bajo su propia marca, adaptada a las necesidades de cada cliente.
                    </div>
                    <div class="mt-3">
                      <a href="#" id="btn-video" class="btn btn-success btn-circle">
                          <i class="fas fa-pen"></i>
                      </a>
                      <a href="#" id="btn-video-delete" class="btn btn-danger btn-circle">
                          <i class="fas fa-trash"></i>
                      </a>
                    </div>
                  </li>
                  <li class="flex-start">
                    <div>3</div>
                    <div>http://localhost/universalsoft/index.php</div>
                    <div>Gestión de Certificados</div>
                    <div>
                      Te asesoramos en la elección de la jurisdicción y en el proceso de licenciamiento de juegos en línea, Todas nuestras integraciones cuentan con los respectivos protocolos de seguridad y cifrado complejo (SSL y TSL).
                    </div>
                    <div class="mt-3">
                      <a href="#" id="btn-video" class="btn btn-success btn-circle">
                          <i class="fas fa-pen"></i>
                      </a>
                      <a href="#" id="btn-video-delete" class="btn btn-danger btn-circle">
                          <i class="fas fa-trash"></i>
                      </a>
                    </div>
                  </li>
                </ul>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success">Guardar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalClientes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Nuestros Exitos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col form-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="inputGroupFile" aria-describedby="inputGroupFileAddon01">
                        <label class="custom-file-label" for="inputGroupFile">Coloca su imagen</label>
                    </div>
                    <div class="form-group">
                      <label for="formGroupExampleInput">numero referencial</label>
                      <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Titulo">
                    </div>
                    <div class="form-group">
                      <label for="formGroupExampleInput">Titulo</label>
                      <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Titulo">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 form-group text-right">
                    <a href="#" id="btn-servicios" class="btn btn-success btn-circle">
                        Crear
                    </a>
                    <a href="#" id="btn-servicios-delete" class="btn btn-danger btn-circle">
                        <i class="fas fa-trash"></i>
                    </a>
                </div>
            </div>
            <div class="row">
                <ul class="list-ul-mod">
                  <li>#</li>
                  <li>Imagen</li>
                  <li>Numero</li>
                  <li>titulo</li>
                  <li>Acciones</li>
                </ul>
                <ul class="list-ul-cont">
                  <li class="flex-start">
                    <div>1</div>
                    <div>http://localhost/universalsoft/index.php</div>
                    <div>API</div>
                    <div>
                      Contamos con una integración compatible y elegible, permitiendo usar todos los navegadores de cualquier plataforma, También es adecuado para Windows, Mac OS, Linux, Android, Windows Phone e iOS.
                    </div>
                    <div class="mt-3">
                      <a href="#" id="btn-video" class="btn btn-success btn-circle">
                          <i class="fas fa-pen"></i>
                      </a>
                      <a href="#" id="btn-video-delete" class="btn btn-danger btn-circle">
                          <i class="fas fa-trash"></i>
                      </a>
                    </div>
                  </li>
                </ul>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success">Guardar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalNoticia" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Nuestras Noticias</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col-6 form-group">
                  <div class="custom-file">
                    <input type="file" class="custom-file-input" id="inputGroupFile" aria-describedby="inputGroupFileAddon01">
                    <label class="custom-file-label" for="inputGroupFile">Coloca su imagen</label>
                  </div>
                </div>
                <div class="form-group col-6">
                  <label for="formGroupExampleInput">Titulo</label>
                  <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Titulo">
                </div>
                <div class="form-group col-6">
                  <label for="formGroupExampleInput">Facebook</label>
                  <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Titulo">
                </div>
                <div class="form-group col-6">
                  <label for="formGroupExampleInput">Instagram</label>
                  <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Titulo">
                </div>
                <div class="form-group col-12">
                  <label for="formGroupExampleInput">Contenido</label>
                  <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                </div>
                
            </div>
            <div class="row">
                <div class="col-12 form-group text-right">
                    <a href="#" id="btn-servicios" class="btn btn-success btn-circle">
                        Crear
                    </a>
                    <a href="#" id="btn-servicios-delete" class="btn btn-danger btn-circle">
                        <i class="fas fa-trash"></i>
                    </a>
                </div>
            </div>
            <div class="row">
                <ul class="list-ul-mod">
                  <li>#</li>
                  <li>Imagen</li>
                  <li>Titulo</li>
                  <li>Contenido</li>
                  <li>Acciones</li>
                </ul>
                <ul class="list-ul-cont">
                  <li class="flex-start">
                    <div>1</div>
                    <div>http://localhost/universalsoft/index.php</div>
                    <div>API</div>
                    <div>
                      Contamos con una integración compatible y elegible, permitiendo usar todos los navegadores de cualquier plataforma, También es adecuado para Windows, Mac OS, Linux, Android, Windows Phone e iOS.
                    </div>
                    <div class="mt-3">
                      <a href="#" id="btn-video" class="btn btn-success btn-circle">
                          <i class="fas fa-pen"></i>
                      </a>
                      <a href="#" id="btn-video-delete" class="btn btn-danger btn-circle">
                          <i class="fas fa-trash"></i>
                      </a>
                    </div>
                  </li>
                </ul>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-success">Guardar</button>
      </div>
    </div>
  </div>
</div>


