  var owl = $('.carousel2');
  owl.owlCarousel({
    margin: 0,
    loop: false,
    items:1,
    autoplay:false,
    autoplayTimeout:10000,
    dots:false
  })
  var owl = $('.carousel3');
  owl.owlCarousel({
    margin: 0,
    loop: true,
    items:1,
    autoplay:true,
    autoplayTimeout:10000,
    dots:true,
    responsive:{
      900 : {
        items : 2
      },
    }
  })
  var owl = $('.carousel4');
  owl.owlCarousel({
    margin: 0,
    loop: false,
    items:1,
    autoplay:true,
    autoplayTimeout:10000,
    dots:true,
    responsive:{
      900 : {
        items : 3
      },
    }
  })
  // JavaScript for label effects only
	$(window).load(function(){
		$(".col-3 input").val("");
		
		$(".input-effect input").focusout(function(){
			if($(this).val() != ""){
				$(this).addClass("has-content");
			}else{
				$(this).removeClass("has-content");
			}
		})
  });

  function scrollWin(elementSelector, offset, time){
    if(typeof(offset)=="undefined")
        offset = 0;
    if(typeof(time)=="undefined")
        time = 1000;
    $('body,html').stop().animate({scrollTop: $(elementSelector).offset().top+offset}, time);
}

$viewport = $('body,html'); //use both body and html as firefox places overflow at the html level, and hence scrolls here
$viewport.bind("scroll mousedown DOMMouseScroll mousewheel keyup", function(e){
    if ( e.which > 0 || e.type === "mousedown" || e.type === "mousewheel"){
         $viewport.stop();
    }
});

$('.question1').tooltip({
  container: 'body',
  template: `<div class='tooltip fade bs-tooltip-right'>
                <div class='arrow' style='top: 8px;'></div>
                <div class='tooltip-inner2'>esto es un texto <br> y esto lo separa</div>
             </div>`
});
$('.question2').tooltip({
  container: 'body',
  template: `<div class='tooltip fade bs-tooltip-right'>
                <div class='arrow' style='top: 8px;'></div>
                <div class='tooltip-inner2'>esto es un segundo texto <br> y esto lo separa aaaaaa</div>
             </div>`
});
$('.question3').tooltip({
  container: 'body',
  template: `<div class='tooltip fade bs-tooltip-right'>
                <div class='arrow' style='top: 8px;'></div>
                <div class='tooltip-inner2'>esto es un Tercer texto <br> y esto lo separa bbbbbb</div>
             </div>`
});

/*Some stuff for the demo*/
//bind the scroll to some buttons
$(document).on("click", "#products", function(){
    scrollWin(".events-bg", 0, 1000);
  });

  $("a[href='#contact']").click(function() {
    scrollWin(".contact-bg", 0, 1000);
    return false;
  });
$("a[href='#scrolltop']").click(function() {
    scrollWin("body,html", 0, 1000);
    return false;
  });

$(".scroll-down").click(function() {
  scrollWin(".about-bg", -50, 1000);
  return false;
});


$("#serv1").hover(function(){
  $("#servimg").css("background-image","url('images/servi1.png')")
  $("#servimg").addClass("effectimg")
  $("#servimg").css("opacity","1")
  $("#servimg").css("transition","0.4s all")
},function(){
  $("#servimg").css("background-image","url('images/tablet.png')")
  $("#servimg").css("opacity","0")
})

$("#serv2").hover(function(){
  $("#servimg").css("background-image","url('images/servi2.png')")
 $("#servimg").addClass("effectimg")
  $("#servimg").css("opacity","1")
  $("#servimg").css("transition","0.4s all")
},function(){
  $("#servimg").css("background-image","url('images/tablet.png')")
  $("#servimg").css("opacity","0")
})

$("#serv3").hover(function(){
  $("#servimg").css("background-image","url('images/servi3.png')")
 $("#servimg").addClass("effectimg")
  $("#servimg").css("opacity","1")
  $("#servimg").css("transition","0.4s all")
},function(){
  $("#servimg").css("background-image","url('images/tablet.png')")
  $("#servimg").css("opacity","0")
})

$("#serv4").hover(function(){
  $("#servimg").css("background-image","url('images/servi4.png')")
 $("#servimg").addClass("effectimg")
  $("#servimg").css("opacity","1")
  $("#servimg").css("transition","0.4s all")
},function(){
  $("#servimg").css("background-image","url('images/tablet.png')")
  $("#servimg").css("opacity","0")
})

$("#serv5").hover(function(){
  $("#servimg").css("background-image","url('images/servi5.png')")
 $("#servimg").addClass("effectimg")
  $("#servimg").css("opacity","1")
  $("#servimg").css("transition","0.4s all")
},function(){
  $("#servimg").css("background-image","url('images/tablet.png')")
  $("#servimg").css("opacity","0")
})

$("#serv6").hover(function(){
  $("#servimg").css("background-image","url('images/servi6.png')")
 $("#servimg").addClass("effectimg")
  $("#servimg").css("opacity","1")
  $("#servimg").css("transition","0.4s all")
},function(){
  $("#servimg").css("background-image","url('images/tablet.png')")
  $("#servimg").css("opacity","0")
})
$("#serv7").hover(function(){
  $("#servimg").css("background-image","url('images/servi7.png')")
 $("#servimg").addClass("effectimg")
  $("#servimg").css("opacity","1")
  $("#servimg").css("transition","0.4s all")
},function(){
  $("#servimg").css("background-image","url('images/tablet.png')")
  $("#servimg").css("opacity","0")
})

var counter = function() {
		
  $('.num-clientes-div').waypoint( function( direction ) {

    if( direction === 'down' && !$(this.element).hasClass('ftco-animated') ) {

      var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',')
      $('.number').each(function(){
        var $this = $(this),
          num = $this.data('number');
        $this.animateNumber(
          {
            number: num,
            numberStep: comma_separator_number_step
          }, 7000
        );
      });
      
    }

  } , { offset: '95%' } );

}
counter();

var aboutus = function(){
  $('.about-bg').waypoint( function( direction ) {
    if( direction === 'down' && !$(this.element).hasClass('ftco-animated') ) {
      $(".about-bg").addClass("fadeInDown animated ")
    }
   } , { offset: '95%' },7000);
}
aboutus();

var servicesus = function(){
  $('.services-bg').waypoint( function( direction ) {
    if( direction === 'down' && !$(this.element).hasClass('ftco-animated') ) {
      $(".services-bg").addClass("fadeInDown animated ")
    }
   } , { offset: '95%' } );
}
servicesus();

var servicesus2 = function(){
  $('.servi-bg').waypoint( function( direction ) {
    if( direction === 'down' && !$(this.element).hasClass('ftco-animated') ) {
      $(".servi-bg").addClass("fadeInDown animated ")
    }
   } , { offset: '95%' } );
}
servicesus2();

var eventsus = function(){
  $('.events-bg').waypoint( function( direction ) {
    if( direction === 'down' && !$(this.element).hasClass('ftco-animated') ) {
      $(".events-bg").addClass("fadeInDown animated ")
      $(".item-image").css("opacity","0")
      $(".item-text").css("opacity","0")
      $(".item-text-left").css("opacity","0")
      setTimeout(function(){
        $(".item-image").css("opacity","1")
        $(".item-text").css("opacity","1")
        $(".item-text-left").css("opacity","1")
        $(".item-image").addClass("fadeInLeft animated ")
        $(".item-text").addClass("fadeInRight animated ")
        $(".item-text-left").addClass("fadeInLeft animated ")
      },500)
      
    }
   }, { offset: '95%' } );
}
eventsus();

var clientesus = function(){
  $('.clientes-bg').waypoint( function( direction ) {
    if( direction === 'down' && !$(this.element).hasClass('ftco-animated') ) {
      $(".clientes-bg").addClass("fadeInDown animated ")
    }
   } , { offset: '95%' } );
}
clientesus();

var clientenumsus = function(){
  $('.num-clientes-bg').waypoint( function( direction ) {
    if( direction === 'down' && !$(this.element).hasClass('ftco-animated') ) {
      $(".num-clientes-bg").addClass("fadeInDown animated ")
    }
   } , { offset: '95%' } );
}
clientenumsus();

var clientenumsus2 = function(){
  $('.new-bg').waypoint( function( direction ) {
    if( direction === 'down' && !$(this.element).hasClass('ftco-animated') ) {
      $(".new-bg").addClass("fadeInDown animated ")
    }
   } , { offset: '95%' } );
}
clientenumsus2();


$(".filters-1 .filters-li").click(function(){
  $(".filter .col-12").hide(200);
  $(".filter .col-12[data-filter=" + $(this).data('casino') + "]").delay(200).show(200);
});
$(".filters-2 .filters-li").click(function(){
  $(".filter-2 .col-12").hide(200);
  $(".filter-2 .col-12[data-filter=" + $(this).data('casinolive') + "]").delay(200).show(200);
});
// $(".filters-provee .promo-div-nav-all").click(function(){
//   var value = $(this).attr('data-filter');
  
//   if(value == "all"){
//     console.log(value);
//     $('.filter-all-img').show('1000');
//   }
// });


// EL CODIGO DE LOS VIDEOS COMIENZA ACA

var webm = null;

function init() {
    document._video = document.getElementById("video");

    webm = document.getElementById("webm");

}
document.addEventListener("DOMContentLoaded", init, false);

var videos =
[
  [
    "images/slider3.png",
    "images/video-1.mp4"
  ],
  [
    "images/slider3.png",
    "images/video-2.mp4"
  ],
  [
    "images/slider3.png",
    "images/video-3.mp4"
  ],
  [
    "images/slider3.png",
    "images/video-4.mp4"
  ],
  [
    "images/slider3.png",
    "images/video-5.mp4"
]
];

function resize() {
    document._video.width = document._video.videoWidth + 10;
    document._video.height = document._video.videoHeight + 10;
}

function getVideo() {
  $("#audio-mute").toggle()
  $("#audio-vol").toggle()
	return document._video;
}

function switchVideo(n) {
	if (n >= videos.length) n = 0;

	var mp4 = document.getElementById("mp4");
  var parent = mp4.parentNode;

	document._video.setAttribute("poster", videos[n][0]);
	mp4.setAttribute("src", videos[n][1]);

	if (videos[n][2]) {
		if (webm.parentNode == null) {
			parent.insertBefore(webm, mp4);
		}
		webm.setAttribute("src", videos[n][2]);
	} else {
		if (webm.parentNode != null) {
	    	parent.removeChild(webm);
		}
    }
    
    document._video.load();
}
// EL CODIGO DE LOS VIDEOS TERMINA ACA

// EL CODIGO DE SISTEMAS BANNER COMIENZA ACA
function obj_hover_rotate($hover_obj, $wrap_obj, $move_obj) {
  $($hover_obj).mousemove(function (ev) {

    var oEvent = ev || event;
    var cardWidth = parseInt($($wrap_obj).css("width"));
    var cardHeight = parseInt($($wrap_obj).css("height"));

    var cardLeft = parseInt($($wrap_obj).css("left"));
    var cardTop = parseInt($($wrap_obj).css("top"));

    var centerDisX = oEvent.clientX - cardLeft;
    var centerDisY = oEvent.clientY - cardTop;

    var degX = (Math.abs(centerDisY) / (cardHeight / 2)) * 10;
    var degY = (Math.abs(centerDisX) / (cardWidth / 2)) * 10;

    if (centerDisY < 0 && centerDisX < 0) {
      $($move_obj).css({
        transform:
          "translate(-50%, -50%) rotateX(" +
          degX +
          "deg) rotateY(-" +
          degY +
          "deg)"
      });
    }
    if (centerDisY < 0 && centerDisX > 0) {
      $($move_obj).css({
        transform:
          "translate(-50%, -50%) rotateX(" +
          degX +
          "deg) rotateY(" +
          degY +
          "deg)"
      });
    }
    if (centerDisY > 0 && centerDisX < 0) {
      $($move_obj).css({
        transform:
          "translate(-50%, -50%) rotateX(-" +
          degX +
          "deg) rotateY(-" +
          degY +
          "deg)"
      });
    }
    if (centerDisY > 0 && centerDisX > 0) {
      $($move_obj).css({
        transform:
          "translate(-50%, -50%) rotateX(-" +
          degX +
          "deg) rotateY(" +
          degY +
          "deg)"
      });
    }
  });
}

obj_hover_rotate(".container-product", ".cardBottom", ".card-effect");
// EL CODIGO DE SISTEMAS BANNER TERMINA ACA

// EL CODIGO DE ACERCA BANNER COMIENZA ACA
var words = document.getElementsByClassName('about-span-title');
var wordArray = [];
var currentWord = 0;

words[currentWord].style.opacity = 1;
for (var i = 0; i < words.length; i++) {
  splitLetters(words[i]);
}

function changeWord() {
  var cw = wordArray[currentWord];
  var nw = currentWord == words.length-1 ? wordArray[0] : wordArray[currentWord+1];
  for (var i = 0; i < cw.length; i++) {
    animateLetterOut(cw, i);
  }
  
  for (var i = 0; i < nw.length; i++) {
    nw[i].className = 'letter behind';
    nw[0].parentElement.style.opacity = 1;
    animateLetterIn(nw, i);
  }
  
  currentWord = (currentWord == wordArray.length-1) ? 0 : currentWord+1;
}

function animateLetterOut(cw, i) {
  setTimeout(function() {
		cw[i].className = 'letter out';
  }, i*80);
}

function animateLetterIn(nw, i) {
  setTimeout(function() {
		nw[i].className = 'letter in';
  }, 340+(i*80));
}

function splitLetters(word) {
  var content = word.innerHTML;
  word.innerHTML = '';
  var letters = [];
  for (var i = 0; i < content.length; i++) {
    var letter = document.createElement('span');
    letter.className = 'letter';
    letter.innerHTML = content.charAt(i);
    word.appendChild(letter);
    letters.push(letter);
  }
  
  wordArray.push(letters);
}

changeWord();
setInterval(changeWord, 4000);
// EL CODIGO DE ACERCA BANNER TERMINA ACA