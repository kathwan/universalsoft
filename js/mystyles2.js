$(document).ready(function(){
  
  $("a[href='#contactos']").click(function() {
    $('html, body').animate({ scrollTop: "0" }, "slow");
    return false;
  });
  $("a[href='#reglamentos']").click(function() {
    $("html, body").animate({ scrollTop: "0" }, "slow");
    return false;
  });
  $("a[href='#politicas']").click(function() {
    $("html, body").animate({ scrollTop: "0" }, "slow");
    return false;
  });
  $("a[href='#juego']").click(function() {
    $("html, body").animate({ scrollTop: "0" }, "slow");
    return false;
  });
  $("a[href='#reclamaciones']").click(function() {
    $("html, body").animate({ scrollTop: "0" }, "slow");
    return false;
  });
  $("a[href='#aml']").click(function() {
    $("html, body").animate({ scrollTop: "0" }, "slow");
    return false;
  });
});

// Display feedback after rating 
$('.rating__input').on('click', function() {
  var rating = this['value'];
		$('.rating__label').removeClass('active');
  $(this).siblings('.rating__label').addClass('active');
});

// cambiar de metodo de pago
$(".pagos-img").click(function(e){
  $(".pagos-img").removeClass("active");
  $(this).addClass("active");
})

// cambiar de metodo de pago
$(".pagos-cant").click(function(e){
  $(".pagos-cant").removeClass("active");
  $(this).addClass("active");
})

$(".race-btn").click(function(e){
  $(".race-btn").removeClass("active");
  $(this).addClass("active");
})

// cambiar de tipo de apuesta
$(".bar-top-border-2").click(function(e){
  $(".bar-top-border-2").removeClass("active");
  $(this).addClass("active");

  if ($("#casilla1").hasClass("active")){
    $(".img1").removeClass("active");
    $(".img2").addClass("active");
  }else{
    $(".img1").addClass("active");
    $(".img2").removeClass("active");
  }
  if ($("#casilla2").hasClass("active")){
    $(".img3").removeClass("active");
    $(".img4").addClass("active");
  }else{
    $(".img3").addClass("active");
    $(".img4").removeClass("active");
  }
  if ($("#casilla3").hasClass("active")){
    $(".img5").removeClass("active");
    $(".img6").addClass("active");
  }else{
    $(".img5").addClass("active");
    $(".img6").removeClass("active");
  }
  if ($("#casilla4").hasClass("active")){
    $(".img7").removeClass("active");
    $(".img8").addClass("active");
  }else{
    $(".img7").addClass("active");
    $(".img8").removeClass("active");
  }
})

$("#check1").on('click',function(){

  $(".progress-home").css({"width": "15%"});
})
$("#check2").on('click',function(){
  
  $(".progress-home").css({"width": "40%"});
})
$("#check3").on('click',function(){
  
  $(".progress-home").css({"width": "62%"});
})
$("#check4").on('click',function(){
  
  $(".progress-home").css({"width": "85%"});
})

var owl = $('.carousel1');
  owl.owlCarousel({
    loop: true,
    items:4,
    autoplay:false,
    dots:false
  })
  var owl = $('.carousel2');
  owl.owlCarousel({
    margin: 10,
    loop: true,
    items:1,
    autoplay:true,
    autoplayTimeout:5000,
    dots:false
  })
  var owl = $('.carousel3');
  owl.owlCarousel({
    loop: false,
    items:3,
    dots:false
  })

  var owl = $('.carousel4');
    owl.owlCarousel({
    loop: false,
    items:3,
    margin:10,
    dots:false

  })

  // promociones ....................................................................

  $(".item-promo .promo-div-nav").click(function(){
    $(".filter .col-12").hide(200);
    $(".filter .col-12[data-filter=" + $(this).data('menu') + "]").delay(200).show(200);
  });
  $(".item-promo .promo-div-nav-all").click(function(){
    var value = $(this).attr('data-filter');
    
    if(value == "all"){
      console.log(value);
      $('.filter-all-img').show('1000');
    }
  });

  $(".carousel4 .btn-program").click(function(){
    $(".program-filter .col-6").hide(200);
    $(".program-filter .col-6[data-filter=" + $(this).data('pro') + "]").delay(200).show(200);
  });
  $(".carousel4 .btn-program-all").click(function(){
    var value = $(this).attr('data-filter');
    
    if(value == "all-pro"){
      console.log(value);
      $('.pro-all').show('1000');
    }
  });

  // idioma...........................................................................

$(document).ready(function(){
  // funcion para cambiar las banderas del idioma

  $('#inputPais').on('change',function(){
    var inputpaiss =  $("#inputPais option:selected").val()
    $('#flat-img').attr('src','images/Banderas/'+ inputpaiss + '.png');  
    
    var lan = inputpaiss; 

    switch(lan){
      case 'es':
        console.log("español")
        $('.registro-btn').html("Registrate");
        $('.login-btn').html("Iniciar Sesion");
        $('.Fmejorar').html("¡Que Desea Mejorar!");
        $('.Fvideo1').html("<video class='slider-video' width='100%' preload='auto' loop autoplay muted style='visibility: visible; width: 100%;' poster='images/slider.png'> <source src='images/slider1.mp4' type='video/mp4'> </video>");
        $('.Finicio').html("Inicio");
        $('.Finicio2').html("Bienvenido a Universal Race");
        $('.Ftexto1').html("Los mejores en Apuesta Hípica");
        $('.Ftexto2').html("Nuestra plataforma esta diseñada para que puedas usarlo desde cualquier dispositivo, podrás hacer tus pronósticos, descargar la programación de carreras ,ver las jornadas , ver los resultados de todas las carreras y disfrutar de los videos en vivo haciendo tu apuesta.");
        $('.Ftexto3').html("Universal Race te ofrece Seguridad");
        $('.Ftexto4').html("Tus apuestas son importantes, por eso nos da gusto que lo puedas hacer de la forma más comoda y segura tal y como llevamos haciendo desde antes. Te permitimos hacer depósitos y retiros de forma fácil y dispones de un equipo profesional de asistencia al cliente que te ayudará con cualquier duda respecto a tu cuenta 24/7. Descubre el apasionante mundo de las carreras en vivo con los mejores del mercado y la mejor valorada por sus usuarios.");
        $('.Freglamentos').html("Reglamentos");
        $('.Fterminos').html("Términos Y Condiciones");
        $('.Fcontactanos').html("Contáctanos");
        $('.Fcontact1').html("Necesita Contactarnos");
        $('.Fcontact2').html("Nuestro servicio de atención al cliente esta disponible por email : ");
        $('.Freglament1').html("Reglamentos del sistema");
        $('.Freglament2').html("Sobre Las Apuestas");
        $('.Freglament3').html("<strong>1)</strong> Los pagos se basan en los resultados de las carreras oficiales de los diferentes hipódromos hasta los límites de la casa.<br> <strong>2)</strong> Las apuestas se consideraran ganadoras solo si es acertado de manera exacta los resultados en las carreras apostadas.<br> <strong>3)</strong> No se realizan pagos de consolación, solo serán premiadas las apuestas en las que las combinaciones seleccionadas sean las correctas para cada carrera implicada y serán pagadas en base a lo indicado por el hipódromo, en el caso de que hubiese una selección menor y el hipódromo decida pagar algún premio, se realizará el pago a la selección exacta de igual forma y en base al monto que indique el hipódromo para su selección sin importar que el número de aciertos pagados por el hipódromo sea inferior a selección realizada (apuesta).<br> <strong>4)</strong> Existe un máximo de ganancias netas en cada carrera basado en la categoría en que se encuentra el hipódromo. Las jugadas de Doble (Daily Double), Triple (Pick3), Pick4, Pick5 y Pick6, cuentan como la última carrera en la secuencia en el cálculo de pagos. La fórmula para calcular el pago será: El monto apostado entre la denominación de la apuesta por el monto pagado de la misma. En los casos Win-Place-Show la denominación siempre será 2.<br> <strong>5)</strong> Las apuestas exóticas, tomaran acción solo si el hipódromo ofrece monto de pago para la misma. Si no hay pagos establecidos por el hipódromo para una opción de apuesta estas serán nulas y reembolsadas.<br> <strong>6)</strong> Los caballos son identificados por el número que lleven a sus espaldas durante la carrera, no por el nombre. Los clientes son responsables de sus selecciones tanto del número del caballo como del número de la carrera.<br> <strong>7)</strong> Cuando se apuesta a un caballo que esta emparejado (ENTRY), su apuesta incluye todos los caballos corriendo bajo dicho número. Si una parte de la apuesta no participa, todas las apuestas siguen siendo válidas. No existe la posibilidad de especificar que cierto caballo debe de correr.<br> <strong>8)</strong> Cualquier apuesta aceptada después que una carrera haya sido iniciada, se anula.<br> <strong>9)</strong> Luego de ser realizada la apuesta no es posible cancelar la misma.<br> <strong>10)</strong> Es responsabilidad del usuario verificar su apuesta antes de confirmar y luego de haberlo hecho, y notificar de manera inmediata cualquier error presentado por el sistema, no nos hacemos responsables por errores al momento de la confirmación de ticket si no son notificados de forma inmediata.<br> <strong>11)</strong> La Empresa se reserva el derecho de suspender o modificar cualquier jugada en la que exista algún error referente a la colocación de premios y horas, teniendo como base las demás casas de apuestas y los datos oficiales.");
        $('.Fretirados1').html("De Los Scratches O Retirados");
        $('.Fretirados2').html("<strong>1)</strong> Si un caballo no corre (Scratched) todas las apuestas: Ganador, Segundo y Tercero (Win/Place/Show) serán anuladas y reembolsadas al cliente.<br> <strong>2)</strong> Para las Exacta/Trifecta/Superfecta/Quiniela, las combinaciones donde participe el ejemplar retirado también serán anuladas y reembolsadas, quedando en juego el resto de las combinaciones.<br> <strong>3)</strong> Para los Dobles (Daily Double), Triple (Pick3), Pick4 y Pick6. Si un caballo no corre antes del comienzo de cualquiera de las carreras, se retornará el dinero al usuario.<br>");
        $('.Fparticular1').html("Reglas Particulares");
        $('.Fparticular2').html("<strong>1)</strong> DAILY DOUBLE: La apuesta es ganadora si acierta el 1er lugar en dos carreras consecutivas, designadas como apuestas de Daily Doublé.<br> <strong>2)</strong> PICK 3 (3 aciertos): La apuesta es ganadora si acierta el 1er lugar en tres carreras consecutivas, designadas como apuestas de PICK 3.<br> <strong>3)</strong> PICK 4 (4 aciertos): La apuesta es ganadora si acierta el 1er lugar en cuatro carreras consecutivas, designadas como apuestas de PICK 4.<br> <strong>4)</strong> PICK 5 (5 aciertos): La apuesta es ganadora si acierta el 1er lugar en cinco carreras consecutivas, designadas como apuestas de PICK 5.<br> <strong>5)</strong> PICK 6 (6 aciertos): La apuesta es ganadora si acierta el 1er lugar en seis carreras consecutivas, designadas como apuestas de PICK 6.<br> <strong>6)</strong> PICK 9 (9 aciertos): La apuesta es ganadora si acierta el 1er lugar en nueve carreras consecutivas, designadas como apuestas de PICK 9.<br>");
        $('.Fminimo1').html("Mínimo De Participantes Por Tipo De Apuesta");
        $('.Fminimo2').html("Tipo de apuesta");
        $('.Fminimo3').html("Mínimo de participantes");
        $('.Fclasifica1').html("Clasificación Hipódromos y Límites de Apuestas<br>Límites de apuestas por clasificación");
        $('.Fclasifica2').html("Clasificación");
        $('.Fclasifica3').html("Mínimo Exóticas");
        $('.Fclasifica4').html("Mínimo win, place y show");
        $('.Fclasifica5').html("Límite de apuesta");
        $('.Fclasifica6').html("Límite de Pago");
        $('.Fhipodro1').html("Listado de Hipódromos y su Clasificación");
        $('.Fhipodro2').html("Hipódromo");
        $('.Fhipodro3').html("Clasificación");
        $('.Fregion1').html("Hipódromos por región");
        $('.Fpoliti1').html("Politica Y Privacidad");
        $('.Fpoliti2').html("Para depositar fondos y ponerlos en el servicio, se le pedirá que proporcione información sobre usted y su tarjeta de crédito o débito u otros métodos de pago ('Información del usuario'). Su información sólo será utilizada para este propósito. Cumplimos con las leyes y regulaciones apropiadas relacionadas con los datos personales, incluyendo la Ley de Protección de Datos. Universalrace utiliza 'cookies' para realizar un seguimiento de sus preferencias personales mientras utiliza el Servicio. Las 'cookies' ayudan a asegurar una mejor experiencia de juego. Una 'cookie' se almacena en el disco duro de su ordenador. Las cookies contienen información relevante para la sesión de juego que ha iniciado. Usted puede negarse a aceptar las cookies cambiando la configuración de su navegador. Sin embargo, si selecciona esta opción, es posible que el Servicio le resulte menos agradable. A menos que haya ajustado la configuración de su navegador para rechazar las cookies, éstas se emitirán en su sistema cuando utilice el sitio web. Al proporcionar su información, usted también puede ser capaz de mantener su servicio y puede ser mantenido por nosotros o por cualquier otra compañía que lo procese en nuestro nombre. Estos terceros están autorizados a utilizar los datos sólo de acuerdo con nuestras instrucciones. Podemos utilizar su información y servicios (incluyendo por correo, teléfono, correo electrónico, otros servicios de mensajería electrónica tales como sistemas de texto o de llamada automática o fax). También podemos proporcionarle información sobre nuestros productos y servicios. Su dirección de correo electrónico es necesaria en el momento del registro. Se ha registrado correctamente un correo electrónico. Usted puede optar por no recibir más comunicaciones por correo electrónico de Universalrace haciendo clic en el enlace para darse de baja en el pie de página de cada correo electrónico que reciba o poniéndose en contacto con el Servicio de Atención al Cliente. Su información no será transmitida a terceros para fines de marketing sin su consentimiento. Usted acepta que Universalrace y sus socios comerciales pueden utilizar su nombre, entrada, semejanza, posición y fotografía en relación con el desarrollo, producción, distribución y/o explotación (incluyendo marketing y promoción) del Servicio sin compensación adicional para usted (a menos que lo prohíba la ley), sin embargo Universalrace y sus socios comerciales no lo harán sin obtener primero el consentimiento. Aceptamos que en caso de reorganización, venta o adquisición, es posible que tengamos que revelar información personal a terceros involucrados en la adquisición de la empresa.");
        $('.Fresponsab1').html("Juego Responsable");
        $('.Fresponsab2').html("Para la mayoría de los jugadores, el juego en línea es una forma agradable y sencilla de entretenimiento. Sin embargo, como sitio de juegos de azar en línea, tenemos el deber de asegurarnos de que existan las protecciones y salvaguardas adecuadas para prevenir el juego de menores de edad y los problemas de juego. Le ofrecemos una gama de opciones para ayudarle a jugar de forma responsable. Si tiene alguna pregunta sobre el Juego Responsable, puede ponerse en contacto con nosotros en info@universalrace.net<br><br> <strong>Límites de depósito</strong><br><br> Le permite limitar la cantidad de dinero que puede depositar diariamente, semanalmente o mensualmente. Usted puede restringir este límite en cualquier momento y será efectivo inmediatamente. También puede aflojar el límite, pero no será efectivo hasta 24 horas después y tendrá que confirmar el cambio antes de que se aplique el nuevo límite. Nuestro equipo de soporte puede ayudarle a establecer estos límites.<br><br> <strong>Autoexclusión</strong><br><br> Esta función le permite suspender/cerrar su cuenta por un período específico de una semana, un mes, tres meses, seis meses o un año. Durante este período, no podrá iniciar sesión en su cuenta de Universalrace. Son otra opción si necesita un largo descanso, al contactar con nuestro servicio de atención al cliente puede cerrar su cuenta de forma permanente. Protección contra el uso de menores de edad La ley prohíbe a cualquier persona menor de 18 años abrir una cuenta o jugar en Universalrace. Se toman las medidas apropiadas para protegerse contra el uso de nuestro casino por parte de jugadores menores de edad. Por ejemplo, nuestro software impide el registro de cualquier persona menor de 18 años.<br> Sin embargo, si le preocupa que un menor de edad pueda estar utilizando su ordenador para jugar a nuestros juegos, le aconsejamos que lo haga:<br><br> -Monitorear a cualquier niño o menor que tenga acceso a su computadora cuando esté conectado a SlotsMillion.<br> -Utilizar aplicaciones especializadas de control parental, como Cyberpatrol, para bloquear el acceso de menores de edad a los sitios de juegos de azar.<br> -Mantenga toda la información sobre su cuenta de jugador y los datos de su tarjeta de crédito de forma confidencial.<br> -Crear diferentes perfiles para cualquier persona que acceda a su ordenador para que nadie tenga acceso directo a su información personal.<br> -Si usted sabe de un jugador menor de 18 años que usa Universalrace , por favor contacte a nuestro equipo de servicio al cliente inmediatamente.<br><br> Por favor, tenga en cuenta que nadie menor de 18 años es elegible para reclamar cualquier ganancia de Universalrace, y le pedimos una prueba de identidad al momento de su primer retiro.<br><br> <strong>Juega Seguro</strong><br><br> La mayoría de los jugadores apuestan dentro de sus posibilidades. Sin embargo, puede llegar un momento en el que un jugador sienta que ya no puede controlar su comportamiento de juego.<br> Para evitar que esta situación se convierta en una adicción, le aconsejamos que recuerde las siguientes reglas sencillas:<br> -Piense en los juegos como entretenimiento solamente, no como una forma de ganar dinero.<br> -Sus pérdidas potenciales son los costos involucrados en el juego, y sus ganancias son bonos.<br> -Fíjese un tiempo límite que no debe ser excedido antes de comenzar una sesión de juego.<br> -Fíjese un límite de pérdida y cúmplalo. Asigne una cantidad que pueda permitirse perder y colóquela en su vestíbulo.<br> -Recuerde que debe considerar sus posibles pérdidas. La mayoría de las veces, usted pagará por el disfrute del juego ya que las probabilidades están a favor del casino.<br> -Aprenda acerca de las señales de advertencia de un problema de adicción para que pueda evitarlo.<br> -Bajo ninguna circunstancia debe pedir dinero prestado para apostar.<br> -Mantener su vida social, familiar y laboral lo más rica posible.<br> -Nunca intente cubrir sus pérdidas.<br> -Tenga en cuenta que sus pérdidas son parte del juego y que tratar de recuperarlas a toda costa llevará a más pérdidas.<br>");
        $('.Freclama1').html("Reclamaciones Y Disputas");
        $('.Freclama2').html("<strong>1)</strong> En este apartado, cualquier consulta que se refiera a la realización general de las actividades, distinta de la resolución de la propia operación, quedará clasificada como una consulta a través del procedimiento interno de reclamaciones hasta que se produzca una escalada hasta el Jefe del Servicio de Atención al Cliente (o, en su caso, a otro miembro del personal superior), momento en el que el asunto se clasificará como 'Reclamación'. En última instancia, será la respuesta del Jefe del Servicio de Atención al Cliente, donde se escaló, la que representa la etapa final de este procedimiento.<br><br> <strong>2)</strong> Cuando la consulta se refiera a la resolución de una transacción de apuestas o juegos de azar, se convertirá en una 'Disputa' donde:<br> - 2.1 no se resuelve en la primera o segunda fase del procedimiento de reclamaciones y se eleva al Jefe del Servicio de Atención al Cliente;<br> - 2.2 tras la respuesta del Jefe del Servicio de Atención al Cliente, el reclamante queda insatisfecho y posteriormente remite el asunto al organismo externo competente para que se pronuncie al respecto.<br><br> <strong>3)</strong> Durante este proceso, todas las comunicaciones telefónicas y por correo electrónico con Usted y con cualquier otra persona podrán ser grabadas. Si el contenido de estas comunicaciones es cuestionado, entonces consultaremos estos registros y serán clave en nuestro proceso de toma de decisiones, a menos que se presenten pruebas adicionales.<br><br> <strong>4)</strong> Durante el proceso de resolución Usted debe aceptar no revelar la existencia o el detalle de ninguna investigación, Quejas o Disputas a terceros, lo que podría incluir discusiones en salas de chat o foros equivalentes y se considerará Información Confidencial. En caso de divulgación no autorizada de información confidencial, el proceso de resolución quedará en suspenso. En estas circunstancias, también tendremos la capacidad de congelar y potencialmente cerrar Su cuenta.<br><br> <strong>Procedimiento de Quejas</strong><br><br> <strong>5)</strong> El procedimiento de quejas es el siguiente:<br><br> <strong>Etapa 1</strong><br> En primer lugar, debe ponerse en contacto con nuestro equipo de atención al cliente y un asesor (el agente) investigará la consulta y responderá a la misma. El Agente registrará la llamada/mensaje y tomará nota de su respuesta.<br><br> <strong>Etapa 2</strong><br> En caso de que su consulta no se resuelva en esta fase, el asunto se remitirá al jefe de equipo/directivo del agente. El Equipo/Gerente de Línea revisará el asunto y le comunicará su decisión a usted. Intentaremos responderle en un plazo de 48 horas. Cualquier notificación que le enviemos será enviada a la dirección de correo electrónico que usted nos proporcione cuando registre su Cuenta. Es su responsabilidad avisarnos de cualquier cambio en esta dirección.<br><br> <strong>Etapa 3</strong> <br> Si aún así sigue insatisfecho, el asunto puede pasar al Director de Atención al Cliente, momento en el que su consulta se registrará como una Demanda o, cuando el asunto se relacione con una transacción de apuestas o juegos de azar, como una Disputa.<br><br> <strong>Política de Resolución de Quejas y Disputas</strong><br><br> <strong>6)</strong> La política de resolución de Quejas y Disputas es la siguiente:<br><br> Tras la recepción de una consulta escalonada, el Director de Atención al Cliente obtendrá un informe del Agente designado y del Director de Línea/Equipo, según corresponda, y llevará a cabo cualquier otra investigación necesaria y le responderá. La respuesta del Responsable de Atención al Cliente representa la fase final del procedimiento de Reclamaciones y Disputas Internas.<br><br> En caso de que no esté satisfecho con una Disputa, tiene la opción de escalar el asunto externamente a través de un proveedor de Resolución Alternativa de Disputa.<br><br> Al leer y aceptar estos términos y condiciones, el jugador tendrá la obligación de cumplir con todo lo estipulado.<br> <strong>1.</strong> Ante la postergación de un evento, Universalrace procederá a reprogramar la apuesta de acuerdo a la nueva fecha fijada.<br> <strong>2.</strong> Ante la cancelación de algún evento, Universalrace procederá a recalcular el monto apostado.<br> <strong>3.</strong> Universalrace Tiene el permiso de modificar o aumentar estos términos y condiciones, por ello, invitamos a los clientes a siempre mantenerse informado de los cambios mediante nuestras plataformas.<br> <strong>4.</strong> El cliente es consciente que debe ingresar a informarse periódicamente sobre los términos y condiciones, para estar siempre informado sobre alguna actualización hecha por Latinos bet.<br> <strong>5.</strong> Si existiera algún error en la probabilidad o cualquier otra información importante, por ejemplo: Nombres de los equipos,cuotas, resultados , eventos , ligas, valor especial en las apuestas Universalrace se reserva el derecho de dar por anulado el evento y en consecuencia anular las apuestas parcialmente o en su totalidad.<br> <strong>6.</strong> Ante cualquier apuesta que sea considerada parte de un evento deportivo arreglado o donde haya existido evidencia de manipulación, colusión con terceros, actuando como testaferro, usando testaferros, actuando por cuenta de terceros o usando herramientas de software no permitidos. Universalrace se reserva el derecho de reclamar las ganancias de las apuestas involucradas o cancelar/anular las mismas. El valor apostado será retornado en tales casos.<br> <strong>7.</strong> Si en algún evento se evidencia que los jugadores o clientes han actuado de forma fraudulenta o en colusión de terceros, Universalrace informará a las Federaciones responsables y proveerá la evidencia cotejada en este sentido.<br>");
        $('.faml1').html("Política De Aml/Kyc");
        $('.faml2').html("Procedimientos contra el lavado de dinero (AML)");
        $('.faml3').html("<strong>Detección de AML</strong><br><br> Se pueden solicitar retiros de cantidades que hayan sido devueltas al menos una vez. Esto protege a universalrace de ser abusada para propósitos de lavado de dinero y también protege nuestro sistema de depósitos gratuitos que pueden ser abusados en nuestra desventaja. Todas las solicitudes de retiro se comprueban inicialmente, lo que permite controlar todas las solicitudes de retiro y denota las que son sospechosas, basándose en cualquier antecedente de sospecha de fraude. Cada cliente sólo podrá depositar la cantidad máxima de 1.200 dólares por semana. Los jugadores que deseen depositar más fondos que el límite establecido inicialmente tendrán que pasar por un proceso de verificación suficiente con confirmación de voz, y serán monitoreados regularmente sobre la actividad de juego. Además, estos reproductores serán resaltados/etiquetados en nuestro software de back office, para facilitar su identificación, monitorización y control.<br><br> Aparte de los controles automáticos del sistema, todas las transacciones/cuentas de los clientes son revisadas para detectar múltiples cuentas, individuos que pueden haber sido incluidos en la lista negra de cualquier organización o compañía relevante, comportamiento de transacciones y apuestas, montos manejados, métodos de depósito/retiro y otras cosas que pueden llevar a cualquier actividad sospechosa basada en la experiencia a largo plazo de nuestros empleados relacionados.<br><br> Si no se encuentran tales problemas, este hallazgo será enviado a nuestro departamento de contabilidad para una última comprobación y el pago final puede tener lugar. Este proceso se completa en el plazo de un día laborable.<br><br> <strong>Reportes AML</strong><br><br> En todos los casos en los que Universalrace encuentre y verifique que se está llevando a cabo o se está intentando realizar alguna forma de anti-lavado de dinero, dicha actividad será reportada a las Autoridades de Juegos de Azar de Curaçao, según sea necesario.");
        $('.Fkyc1').html("Directivas KYC");
        $('.Fkyc2').html("<strong>Registro de Jugadores</strong><br><br> Para abrir una cuenta con Universalrace desde el portal web, el usuario debe primero completar el proceso de registro y depositar algunos fondos con los que pueda empezar a jugar. Los datos de registro que deben rellenar todos los nuevos clientes incluyen el nombre y apellidos, la dirección, el país de residencia, la fecha de nacimiento, la dirección de correo electrónico y la moneda de la cuenta. Todos los clientes deben tener al menos 18 años de edad y sólo pueden tener una cuenta de jugador a su nombre, como se especifica en el Reglamento de Juegos a Distancia de Curaçao. Por lo tanto, ningún menor de edad puede jugar en el sitio web de Universalrace. Los usuarios que no tengan una cuenta de jugador registrada no podrán jugar.<br><br> <strong>Due Diligence</strong><br><br> Los jugadores de países/estados restringidos no pueden registrarse debido a las estrictas regulaciones de no apostar en su país. Estos países también están incluidos en los Términos y Condiciones para que los jugadores puedan conocer dichas restricciones antes de registrarse. Antes de cualquier retiro, se le pedirá al cliente la siguiente documentación: comprobante de cuenta bancaria, tarjeta de crédito, factura de servicios públicos (no mayor de 3 meses) y tarjeta de identificación. Dicha documentación debe ser verificada antes de la aprobación del retiro. Además, la verificación de voz y número de teléfono se llevará a cabo en clientes de alto volumen, y en clientes con graves irregularidades en su comportamiento de apuestas. Toda la documentación se solicitará formalmente a través de correos electrónicos para ambos escenarios de diligencia debida, y se archivará en orden con los procedimientos internos de la compañía.<br><br> En caso de retirada acumulada por parte de cualquier cliente de 2.300usd o más, éste estará obligado a enviar una prueba de identidad en forma de documentos escaneados, como el carnet de identidad escaneado o el pasaporte, para confirmar su identidad, y en determinados casos, una factura reciente para confirmar su dirección.<br><br> No se procesarán retiros acumulativos de 2.300usd o más hasta que el cliente que solicita el retiro envíe estos documentos, y sean comprobados y confirmados por el personal de Gestión de Fraudes.<br><br> Cada vez que un nuevo cliente deposita dinero y comienza a jugar/apostar en el sitio web de Universalrace, sus transacciones y comportamiento de apuestas serán revisadas y monitoreadas por nuestro sistema diariamente. Este procedimiento nos permite analizar la forma en que cada cliente individual utiliza los productos de nuestros sitios web, lo que permite categorizar a los jugadores y establecer límites a los jugadores individuales de acuerdo con sus patrones de apuestas.<br><br> <strong>Verificación de correo electrónico</strong><br><br> Cuando se crea una nueva cuenta, se envía un enlace de verificación a la cuenta de correo electrónico especificada durante el proceso de registro. Cuando el usuario hace clic en el enlace, la cuenta se crea correctamente. Sólo en este punto el jugador puede depositar dinero y comenzar a hacer apuestas.");
        $('.Ffooter1').html("Universal Race apoya el <span class='color-red'>Juego Responsable.</span> Las apuestas en menores de edad se considera una ofensa.");
        $('.Ffooter2').html("juego seguro");
        $('.Ffooter3').html("Al acceder, continuar usando, o navegar en esta web, aceptas que utilicemos cookies para mejorar tu experiencia en nuestra web. Universal Raceno utilizará ninguna cookie que interfiera con tu privacidad, solo aquellas que mejoren la experiencia de uso de nuestra web.<br><br> Utilizamos cookies <span class='color-red'>Esta Politica</span> describe el uso que hacemos de ellas");
        $('.Flogin1').html("Usuario o Correo Electronico");
        $('.Flogin2').html("Contraseña");
        $('.Flogin3').html("Recordar nombre de usuario");
        $('.Flogin4').html("Registrate Ahora");
        $('.Flogin5').html("Entrar");
        $('.Flogin6').html("Tambien ingresa con");
        $('.Flogin7').html("¿Olvido su contraseña?");
        $('.Flregistro1').attr('placeholder','Usuario');
        $('.Flregistro2').attr('placeholder','Nombre y Apellido');
        $('.Flregistro3').attr('placeholder','Confirmar Contraseña');
        $('.Flregistro4').attr('placeholder','Correo Electronico');
        $('.Flregistro5').attr('placeholder','Contraseña');
        $('.Flregistro6').attr('placeholder','Telefono');
        $('.Flregistro7').attr('placeholder','Fecha de nacimiento');
        $('.Flregistro8').html("Moneda");
        $('.Flregistro9').html("CONFIRMO QUE TENGO AL MENOS 18 AÑOS");
        $('.Flregistro10').html("ACEPTO LOS T&C Y LA PRIVACIDAD <a href='term.html'>(TÉRMINOS Y CONDICIONES)</a>");
        $('.Flregistro11').html("MANTENERME ACTUALIZADO SOBRE PROMOCIONES");
        $('.Flregistro12').html("Registrate");
        $('.Fhelp1').html("¡Trabajamos para mejorar!");
        $('.Fhelp2').html("¿Qué opinas sobre la pagina?");
        $('.Fhelp3').html("Por favor selecciona un tema");
        $('.Fhelp4').html("- Selecciona un tema -");
        $('.Fhelp5').html("Sugerencia");
        $('.Fhelp6').html("Cumplido");
        $('.Fhelp7').html("Preguntar");
        $('.Fhelp8').html("Error");
        $('.Fhelp9').html("¿Qué te gustaría compartir con nosotros?");
        $('.Fhelp10').html("Enviar");
        $('.Fhelp11').html("Desarrollado por <span class='small-img'>UNIVERSAL SOFT</span>");
        break;
      case 'eu' :
        console.log("ingles")
        $('.registro-btn').html("Sign up");
        $('.login-btn').html("Log In");
        $('.Fmejorar').html("What you want to improve!");
        $('.Fvideo1').html("<video class='slider-video' width='100%' preload='auto' loop autoplay muted style='visibility: visible; width: 100%;' poster='images/slider.png'> <source src='images/slider2.mp4' type='video/mp4'> </video>");
        $('.Finicio').html("Home");
        $('.Finicio2').html("Welcome to Universal Race");
        $('.Freglamentos').html("Reglaments");
        $('.Fterminos').html("Terms and Conditions");
        $('.Fcontactanos').html("Contact us");

        $('.Ftexto1').html("THE BEST RACING BETS ");
        $('.Ftexto2').html("Our platform is designed so that you can use it from any device, you can make your predictions, download the race schedule, see the results of all the races and enjoy the live video feed.");
        $('.Ftexto3').html("UNIVERSAL RACE OFFERS YOU SECURITY");
        $('.Ftexto4').html("Your bets are important, that's why we are glad that you can do it in the most comfortable and safe way. We allow you to make deposits and withdrawals easily. A professional customer support team that will help you with any questions regarding your account 24/7. Discover the exciting world of live racing with the best in the market and the best valued by its users.");
        
        $('.Fcontact1').html("Do you need to contact us");
        $('.Fcontact2').html("Our customer service is available by email : ");
       
        $('.Freglament1').html("System Rules");
        $('.Freglament2').html("About The Betting");
        $('.Freglament3').html("<strong>1)</strong> Payments are based on the results of the official races of the different racetracks with house limits.<br> <strong>2)</strong> Bets will be considered winners only if the results of the races wagered are exactly correct.<br> <strong>3)</strong> No consolation payments are made, only bets will be rewarded in which the selected combinations are correct for each race involved and will be paid based on what is indicated by the racetrack, in the event that there is a smaller selection and the racetrack decides to pay some prize, the payment will be made to the exact selection in the same way and based on the amount indicated by the racetrack for its selection regardless of the number of hits paid by the racetrack being less than the selection made (bet).<br> <strong>4)</strong> There is a maximum net profit in each race based on the category in which the racetrack is located. The moves of Double (Daily Double), Triple (Pick3), Pick4, Pick5 and Pick6, count as the last race in the sequence in the calculation of payments. The formula to calculate the payment will be: The amount bet between the denomination of the bet for the amount paid of the same. In Win-Place-Show cases the denomination will always be 2.<br> <strong>5)</strong> Exotic bets will take action only if the racetrack offers payment amount for it. If there are no established racetrack payments for a bet option they will be void and refunded.<br> <strong>6)</strong> Horses are identified by the number they carry on them during the race, not by name. Clients are responsible for their selections for both horse number and race number.<br> <strong>7)</strong> When you bet on a horse that is paired (ENTRY), your bet includes all the horses running under that number. If a part of the bet does not participate, all bets remain valid. There is no possibility to specify that a certain horse must run.<br> <strong>8)</strong> Any bet accepted after a race has started is void.<br> <strong>9)</strong> After the bet is made, it is not possible to cancel it.<br> <strong>10)</strong> It is the responsibility of the user to verify your bet before confirming and after having done so, and immediately notify of any error presented by the system, we are not responsible for errors at the time of the ticket confirmation if they are not notified immediately .<br> <strong>11)</strong> The Company reserves the right to suspend or modify any play in which there is an error regarding the placement of prizes and hours, based on the other betting houses and official data. ");
       
        $('.Fretirados1').html("Scratches Or Retired");
        $('.Fretirados2').html("<strong>1)</strong> If a horse does not run (Scratched) all bets: Winner, Second and Third (Win / Place / Show) will be void and refunded to the client.<br> <strong>2)</strong> For Exacta / Trifecta / Superfecta / Quiniela, the combinations where the withdrawn horse participates will also be canceled and reimbursed, leaving the rest of the combinations at stake.<br> <strong>3)</strong> For Doubles (Daily Double), Triple (Pick3), Pick4 and Pick6. If a horse does not run before the start of any of the races, the money will be returned to the user.<br>");
        
        $('.Fparticular1').html("Special Rules");
        $('.Fparticular2').html("<strong>1)</strong> DAILY DOUBLE: The bet is won if you hit 1st place in two consecutive races, designated as Daily Doublé bets.<br> <strong>2)</strong> PICK 3 (3 hits): The bet is won if you hit 1st place in three consecutive races, designated as PICK 3 bets.<br> <strong>3)</strong> PICK 4 (4 hits): The bet is won if you hit 1st place in four consecutive races, designated as PICK 4 bets.<br> <strong>4)</strong> PICK 5 (5 hits): The bet is won if you hit 1st place in five consecutive races, designated as PICK 5 bets.<br> <strong>5)</strong> PICK 6 (6 hits): The bet is won if you hit 1st place in six consecutive races, designated as PICK 6 bets.<br> <strong>6)</strong> PICK 9 (9 hits): The bet is won if you hit 1st place in nine consecutive races, designated as PICK 9 bets.<br> ");
       
        $('.Fminimo1').html("Minimum Participants By Bet Type");
        $('.Fminimo2').html("Type of Bet");
        $('.Fminimo3').html("Minimum of participants");

        $('.Fclasifica1').html("Racetrack Classification and Betting Limits <br><br>Ranking Betting Limits");
        $('.Fclasifica2').html("Clasification");
        $('.Fclasifica3').html("Minimum Exotics");
        $('.Fclasifica4').html("Minimum win, place and show");
        $('.Fclasifica5').html("Bet limit");
        $('.Fclasifica6').html("Payment limit");

        $('.Fhipodro1').html("List of Racecourses and their Classification");
        $('.Fhipodro2').html("Race Track");
        $('.Fhipodro3').html("Classification");

        $('.Fregion1').html("Racetracks by region");

        $('.Fpoliti1').html("Policy And Privacy");
        $('.Fpoliti2').html("To deposit funds and put them into service, request information about yourself and your credit or debit card or other payment methods ('User Information'). Your information will only be used for this purpose. We comply with related laws and regulations related to personal data, including the Data Protection Law. Universalrace uses 'cookies' to track your personal preferences while using the Service. Cookies help ensure a better gaming experience. A 'cookie' is stored on your computer's hard drive. Cookies contain information relevant to the game session you have started. You can refuse to accept cookies by changing your browser settings. However, if you select this option, the Service may be less pleasant to you. Unless you have adjusted your browser settings to reject cookies, it will be issued on your system when you use the website. By providing your information, you may also be able to maintain your service and it may be maintained by us or by any other company that processes it on our behalf. These third parties are located to use the data only in accordance with our instructions. We may use your information and services (including by mail, telephone, email, other electronic messaging services, stories such as text or automatic call systems, or fax). We can also provide you with information about our products and services. Your email address is required at the time of registration. An email has been successfully registered. You may opt out of receiving further email communications from Universalrace by clicking the unsubscribe link in the footer of each email you receive or by contacting Customer Service. Your information will not be passed on to third parties for marketing fines without your consent. You agree that Universalrace and its business partners may use your name, entry, likeness, position and photograph in connection with the development, production, distribution and / or exploitation (including marketing and promotion) of the Service without further compensation to you (unless prohibited by law), however Universalrace and its business partners will not do so without first obtaining consent. We accept that in case of reorganization, sale or acquisition, we may have to disclose personal information to third parties involved in the acquisition of the company.");

        $('.Fresponsab1').html("Responsible gaming");
        $('.Fresponsab2').html("For most players, online gaming is a pleasant and simple form of entertainment. However, as an online gambling site, we have a duty to ensure that adequate safeguards and safeguards exist to prevent underage gambling and gambling problems. We offer you a range of options to help you play responsibly. If you have any questions about Responsible Gaming, you can contact us at info@universalrace.net<br><br> <strong>Deposit limits</strong><br><br> It allows you to limit the amount of money you can deposit daily, weekly or monthly. You can restrict this limit at any time and it will be effective immediately. You can also loosen the limit, but it will not be effective until 24 hours later and you will have to confirm the change before the new limit applies. Our support team can help you set these limits.<br><br> <strong>Self exclusion</strong><br><br> Most players bet within their means. However, there may come a time when a player feels that they can no longer control their gaming behavior.<br><br> - Monitor any child or minor who has access to your computer when connected to SlotsMillion.<br> - Use specialized parental control applications, such as Cyberpatrol, to block minors' access to gambling sites.<br> - Keep all the information about your player account and your credit card details confidential.<br> - Create different profiles to accesses your computer so that no one has direct access to your personal information.<br> Please note that no one under the age of 18 is eligible to claim any Universalrace winnigs and we ask for proof of identity at time of your first withdrawal.<br><br> <strong>Play Safe</strong><br><br> LMost players bet within their means. However, there may come a time when a player feels that they can no longer control their gaming behavior.<br> To prevent this situation from becoming an addiction, we advise you to remember the following simple rules:<br> -Think of games as entertainment only, not as a way to earn money.<br> -Your potential losses are the costs involved in the game, and your winnings are bonuses.<br> -Set a time limit that should not be exceeded before starting a game session.<br> -Set a loss limit and stick to it. Assign an amount you can afford to lose and place it in your lobby.<br> -Remember that you should consider your possible losses. Most of the time, you will pay for the enjoyment of the game as the odds are in the casino's favor.<br> -Learn about the warning signs of an addiction problem so you can avoid it.<br> -Under no circumstances should you borrow money to gamble.<br> -Keep your social, family and work life as rich as possible.<br> -Never try to cover your losses.<br> -Please note that your losses are part of the game and that trying to recover them at all costs will lead to more losses.<br>");
        
        $('.Freclama1').html("Claims and Disputes");
        $('.Freclama2').html("<strong>1)</strong> In this section, any query that refers to the general performance of the activities, other than the resolution of the operation itself, will be classified as a query through the internal claims procedure until there is an lifted to Customer Service (or, where appropriate, other senior staff member), at which time the matter will be classified as a 'Claim'. Ultimately, it will be the of the Head of the Customer Service, where the final stage of this procedure is resolved.<br><br> <strong>2)</strong> When the query refers to the resolution of a betting or gambling transaction, it will become a 'Dispute' when:<br> - 2.1 Issue is not resolved in the first or second phase of the claims procedure and is submitted to the Head of the Customer Service Department;<br> - 2.2 After the response of the Head of the Customer Service Department, if the customer is dissatisfied and subsequently refers the matter to the governing external body for a ruling on it.<br><br> <strong>3)</strong> During this process, all telephone and email communications with you and any other person may be recorded. If the content of these communications is questioned, then we will consult these records and they will be key in our decision-making process, unless additional evidence is presented.<br><br> <strong>4)</strong> During the resolution process You must agree not to disclose the existence or detail of any investigation, Complaints or Disputes to third parties, which could include discussions in chat rooms or equivalent forums and will be considered Confidential Information. In the event of unauthorized disclosure of confidential information, the resolution process will be suspended. In these circumstances, we will also have the rite to freeze and potentially close Your account.<br><br> <strong>Claim Procedure</strong><br><br> <strong>5)</strong> The claim procedure is as follows:<br><br> <strong>Stage 1</strong><br> First, you should contact our customer service team and a consultant (the agent) will investigate and respond to your inquiry. The Agent will record the call / message and note your response.<br><br> <strong>Stage 2</strong><br> In the event that your claim is not resolved at this stage, the matter will be referred to the agent's team leader / manager. The Team / Line Manager will review the matter and communicate its decision to you. We will reply to you within 48 hours. Any notifications we send you will be sent to the email address you provide us when you register your Account. It is your responsibility to notify us of any changes to this address.<br><br> <strong>Stage 3</strong> <br> If you are still unsatisfied, the matter may be referred to the Director of Customer Service, at which time your inquiry will be recorded as a Demand or, when the matter relates to a betting or gambling transaction, as a Dispute.<br><br> <strong>Complaint and Dispute Resolution Policy</strong><br><br> <strong>6)</strong> The complaint and dispute resolution policy is as follows:<br><br> Upon receipt of a phased inquiry, the Customer Service Director will obtain a report from the Designated Agent and the Line / Team Director, as appropriate, and will conduct any further necessary inquiries and respond to you. The response of the Customer Service Manager represents the final phase of the Internal Claims and Disputes procedure.<br><br> In the event that you are not satisfied with a Dispute, you have the option to escalate the matter externally through an Alternative Dispute Resolution provider.<br><br> By reading and accepting these terms and conditions, the player will have the obligation to comply with what is stipulated.<br> <strong>1.</strong> Before the postponement of an event, Universalrace will proceed to reprogram the bet according to the new date set.<br> <strong>2.</strong> Upon cancellation of any event, Universalrace will recalculate the amount wagered.<br> <strong>3.</strong> Universalrace may modify or increase these terms and conditions, therefore, we invite customers to always stay informed of changes to our platforms.<br> <strong>4.</strong> The client is aware that he must periodically inform himself about the terms and conditions, to always be informed about any update made by Latinos bet.<br> <strong>5.</strong> If there is an error in the probability or any other important information, for example: Team names, odds, results, events, leagues, special value in betting Universalrace reserves the right to cancel the event and consequently void bets partially or in full.<br> <strong>6.</strong> Before any bet that is considered part of an arranged sporting event or where there has been evidence of manipulation, collusion with third parties, acting as figurehead, using figureheads, acting on behalf of third parties or using non-permitted software tools. Universalrace reserves the right to claim the winnings of the bets involved or cancel / cancel them. The value bet will be returned in such cases.<br> <strong>7.</strong> If in any event it is evident that the players or clients have acted fraudulently or in collusion with third parties, Universalrace will inform the responsible Federations and provide the collated evidence in this regard.<br>");
        
        $('.faml1').html("Aml / Kyc Policy");
        $('.faml2').html("Anti-money laundering procedures (AML)");
        $('.faml3').html("<strong>AML detection</strong><br><br> Withdrawals of amounts that have been returned at least once can be requested. This protects universalrace from being used for money laundering purposes and also protects our system of free deposits that can be abused to our disadvantage. All withdrawal requests are initially checked, allowing you to monitor all withdrawal requests and denote those that are suspicious, based on any history of suspected fraud. Each client can only deposit the maximum amount of $ 1,200 per week. Players who wish to deposit more funds than the initially set limit will have to go through a sufficient verification process with voice confirmation, and will be regularly monitored for gaming activity. Furthermore, these players will be highlighted / tagged in our back office software, to facilitate their identification, monitoring and control.<br><br> Aside from automatic system controls, all customer transactions / accounts are screened for multiple accounts, individuals who may have been blacklisted by any relevant organization or company, transaction and betting behavior, amounts handled, methods deposit / withdrawal and other things that can lead to any suspicious activity based on the long-term experience of our related employees.<br><br> If no such problems are found, this finding will be sent to our accounting department for a final check and final payment can take place. This process is completed within one business day.<br><br> <strong>AML Reporting</strong><br><br> In all cases in which Universalrace finds and verifies that some form of money laundering is being carried out or is trying to be carried out, said activity will be reported to the Curaçao Gaming Authorities, as necessary. </em>");
        
        $('.Fkyc1').html("KYC Directives");
        $('.Fkyc2').html("<strong>Player Registration</strong><br><br> To open an account with Universalrace from the web portal, the user must first complete the registration process and deposit some funds with which they can start playing. The registration data that must be filled in by all new clients include the name and surname, the address, the country of residence, the date of birth, the email address and the currency of the account. All clients must be at least 18 years of age and can only have one player account in their name, as specified in the Curaçao Gaming Regulations. Therefore, no minor can play on the Universalrace website. Users without a registered player account will not be able to parcipitate.<br><br> <strong>Due Diligence</strong><br><br> Players from restricted countries / states cannot register due to strict no-gambling regulations in their country. These countries are also included in the Terms and Conditions so that players can know these restrictions before registering. Before any withdrawal, the client will be asked for the following documentation: proof of bank account, credit card, utility bill (not older than 3 months) and identification card. Such documentation must be verified before the withdrawal is approved. In addition, voice and phone number verification will be carried out on high volume clients, and on clients with serious irregularities in their betting behavior. All documentation will be formally requested through emails for both due diligence scenarios, and will be filed in order with the company's internal procedures.<br><br> In case of accumulated withdrawal by any client of 2,300usd or more, he will be obliged to send proof of identity in the form of scanned documents, such as the scanned identity card or passport, to confirm his identity, and in certain cases , a recent utility bill to confirm your address.<br><br> No cumulative withdrawals of 2,300usd or more will not be processed until the client requesting the withdrawal submits these documents, and they are verified and confirmed by the Fraud Management staff.<br><br> Every time a new customer deposits money and starts playing / betting on the Universalrace website, their transactions and betting behavior will be reviewed and monitored by our system on a daily basis. This procedure allows us to analyze how each individual customer uses the products on our websites, allowing players to be categorized and individual players to be set according to their betting patterns.<br><br> <strong>Email verification</strong><br><br> When a new account is created, a verification link is sent to the email account specified during the registration process. When the user clicks on the link, the account is created successfully. Only at this point the player can deposit money and start making bets.");
        
        $('.Ffooter1').html("Universal Race supports <span class='color-red'>Responsible Gaming.</span> Gambling with minors is considered a criminal offense.");
        $('.Ffooter2').html("Play Safe");
        $('.Ffooter3').html("By accessing, continuing to use, or browsing this website, you agree that we use cookies to improve your experience on our website. Universal Race will not use any cookie that interferes with your privacy, only those that improve the experience of using our website<br><br> Please read our <span class='color-red'>FAQ</span> for more information Universalrace.net");
        
        $('.Flogin1').html("Username or Email");
        $('.Flogin2').html("Password");
        $('.Flogin3').html("Remember username");
        $('.Flogin4').html("Register now");
        $('.Flogin5').html("Log in");
        $('.Flogin6').html("Also enter with");
        $('.Flogin7').html("Forgot your password?");

        $('.Flregistro1').attr('placeholder','Username');
        $('.Flregistro2').attr('placeholder','Name and Lastname');
        $('.Flregistro3').attr('placeholder','Confirg password');
        $('.Flregistro4').attr('placeholder','E-mail');
        $('.Flregistro5').attr('placeholder','Password');
        $('.Flregistro6').attr('placeholder','Phone');
        $('.Flregistro7').attr('placeholder','Birthdate');
        $('.Flregistro8').html("Currency");
        $('.Flregistro9').html("I CONFIRM THAT I AM OVER THE AGE OF 18 ");
        $('.Flregistro10').html("I ACCEPT TERMS OF PRIVACY AND T&C <a href='term.html'>(TERMS AND CONDITIONS)</a>");
        $('.Flregistro11').html("KEEP UP-TO-DATE ON PROMOTIONS");
        $('.Flregistro12').html("Register now");

        $('.Fhelp1').html("We work to improve!");
        $('.Fhelp2').html("What do you think about the page?");
        $('.Fhelp3').html("Please select a topic");
        $('.Fhelp4').html("- Select a topic -");
        $('.Fhelp5').html("Suggestion");
        $('.Fhelp6').html("Compliment");
        $('.Fhelp7').html("Questions");
        $('.Fhelp8').html("Error");
        $('.Fhelp9').html("What would you like to share with us?");
        $('.Fhelp10').html("Send");
        $('.Fhelp11').html("Developed by : <span class='small-img'>UNIVERSAL SOFT</span>");
        break;
      case 'fran' : 
        console.log("frances")
        $('.registro-btn').html("S'inscrire");
        $('.login-btn').html("Connexion");
        $('.Fmejorar').html("Que voulez-vous améliorer!");
        $('.Finicio').html("Début");
        $('.Finicio2').html("Bienvenue sur Universal Race");
        $('.Freglamentos').html("Règlements");
        $('.Fterminos').html("Termes et Conditions");
        $('.Fcontactanos').html("Nous contacter");
        
        $('.Ftexto1').html("LE MEILLEUR SITE DE PARIS HIPPIQUE ");
        $('.Ftexto2').html("Notre plateforme est conçue pour que vous puissiez l'utiliser à partir de n'importe quel dispositif, vous pouvez faire vos pronostiques, télécharger le calendrier des courses, voir les jours et voir les résultats de toutes les courses, profiter des vidéos en direct tout en realisant votre pari.");
        $('.Ftexto3').html("LA COURSE UNIVERSALRACE VOUS OFFRE LA SÉCURITÉ");
        $('.Ftexto4').html("Vos paris sont importants et c'est pourquoi nous prenons soin d'assurer une expérience de jeu sûre et confortable pour nos clients. Déposer et retirer des gains est facile et totalement sécurisé. Notre service clientèle est de plus disponible tous les jours 24/7 pour répondre à vos questions. Decouvrez dès maintenant la meilleure expérience de jeu en ligne du marché et profitez-en !");
        
        $('.Fcontact1').html("Une Question? Contactez-nous");
        $('.Fcontact2').html("Notre service client: ");
        
        $('.Freglament1').html("Règlementation du système");
        $('.Freglament2').html("Règles Des Paris");
        $('.Freglament3').html("<strong>1)</strong> Les paiements sont basés sur les résultats des courses officielles des différents hippodromes jusqu'aux limites de la maison.<br> <strong>2)</strong> Les paris ne seront considérés comme gagnants que si les résultats des courses pariées sont exacts.<br> <strong>3)</strong> Aucun paiement de consolation n'est effectué, seuls les paris seront récompensés dans lesquels les combinaisons sélectionnées sont correctes pour chaque course impliquée et seront payés en fonction de ce qui est indiqué par l'hippodrome, dans le cas où il y a une sélection plus petite et l'hippodrome décide de payer un prix, le paiement sera effectué à la sélection exacte de la même manière et en fonction du montant indiqué par l'hippodrome pour sa sélection quel que soit le nombre de coups payés par l'hippodrome étant inférieur à la sélection effectuée (pari).<br> <strong>4)</strong> Il y a un bénéfice net maximum dans chaque course en fonction de la catégorie dans laquelle se trouve l'hippodrome. Les jeux de Double (Daily Double), Triple (Pick3), Pick4, Pick5 et Pick6, comptent comme la dernière course de la séquence dans le calcul des paiements. La formule pour calculer le paiement sera: Le montant parié entre la dénomination du pari pour le montant payé du même. Dans les cas Win-Place-Show, la dénomination sera toujours 2.<br> <strong>5)</strong> 5) Les paris exotiques n'interviendront que si l'hippodrome propose le montant du paiement. S'il n'y a pas de paiement établi pour une option de pari, ils seront annulés et remboursés.<br> <strong>6)</strong> Les chevaux sont identifiés par le numéro qu'ils portent sur leur gilet pendant la course, pas par leur nom. Les clients sont responsables de leurs sélections pour le numéro de cheval et le numéro de course.<br> <strong>7)</strong> Lorsque vous pariez sur un cheval jumelé (ENTRY), votre pari inclut tous les chevaux qui courent sous ce numéro. Si une partie du pari ne participe pas, tous les paris restent valables. Il n'est pas possible de spécifier qu'un certain cheval doit courir.<br> <strong>8)</strong> Tout pari accepté après le début d'une course commencé, sera annulé.<br> <strong>9)</strong> Une fois le pari effectué, il n'est pas possible de l'annuler.<br> <strong>10)</strong> Il est de la responsabilité de l'utilisateur de vérifier votre pari avant de confirmer et après l'avoir valider, et de notifier immédiatement toute erreur présentée par le système, nous ne nous responsabilisons pas des erreurs au moment de la confirmation du ticket si elles ne sont pas notifiées immédiatement.<br> <strong>11)</strong> La Société se réserve le droit de suspendre ou de modifier tout jeu dans lequel il y a une erreur concernant le placement des prix et des heures, sur la base des autres maisons de paris et des données officielles.");
        
        $('.Fretirados1').html("De Los Scratches O Retirados");
        $('.Fretirados2').html("<strong>1)</strong> Si un caballo no corre (Scratched) todas las apuestas: Ganador, Segundo y Tercero (Win/Place/Show) serán anuladas y reembolsadas al cliente.<br> <strong>2)</strong> Para las Exacta/Trifecta/Superfecta/Quiniela, las combinaciones donde participe el ejemplar retirado también serán anuladas y reembolsadas, quedando en juego el resto de las combinaciones.<br> <strong>3)</strong> Para los Dobles (Daily Double), Triple (Pick3), Pick4 y Pick6. Si un caballo no corre antes del comienzo de cualquiera de las carreras, se retornará el dinero al usuario.<br>");
        
        $('.Fretirados1').html("Scratches ou Retiré ");
        $('.Fretirados2').html("<strong>1)</strong> Si un cheval ne court pas (Scratched), tous les paris: gagnant, deuxième et troisième (gagnant / placer / montrer) seront annulés et remboursés au client.<br> <strong>2)</strong> Pour Exacta / Trifecta / Superfecta / Quiniela, les combinaisons auxquelles participe le cheval retiré seront également annulées et remboursées, laissant le reste des combinaisons en jeu.<br> <strong>3)</strong> Pour les doubles (Daily double), Triple (Pick3), Pick4 et Pick6. Si un cheval ne court pas avant le début de l'une des courses, l'argent sera restitué à l'utilisateur.<br> ");
      
        $('.Fparticular1').html("Règles particulières");
        $('.Fparticular2').html("<strong>1)</strong> DAILY DOUBLE: Le pari est gagné si vous atteignez la 1ère place dans deux courses consécutives, désignées comme paris Daily double.<br> <strong>2)</strong> PICK 3 (3 coups): Le pari est gagné si vous atteignez la 1ère place dans trois courses consécutives, désignées comme paris PICK 3.<br> <strong>3)</strong> PICK 4 (4 coups): Le pari est gagné si vous atteignez la 1ère place dans quatres courses consécutives, désignées comme paris PICK 4.<br> <strong>4)</strong> PICK 5 (5 coups): Le pari est gagné si vous atteignez la 1ère place dans cinq courses consécutives, désignées comme paris PICK 5.<br> <strong>5)</strong> PICK 6 (6 coups): Le pari est gagné si vous atteignez la 1ère place dans six courses consécutives, désignées comme paris PICK 6.<br> <strong>6)</strong> PICK 9 (9 coups): Le pari est gagné si vous atteignez la 1ère place dans neuf courses consécutives, désignées comme paris PICK 9.<br>");
        
        $('.Fminimo1').html("Nombre minimum de participants par type de pari");
        $('.Fminimo2').html("Type de pari");
        $('.Fminimo3').html("Minimum de participants");

        $('.Fclasifica1').html("Classification de l'hippodrome et limites de paris");
        $('.Fclasifica2').html("Classification");
        $('.Fclasifica3').html("Exotiques minimes");
        $('.Fclasifica4').html("Gain minimum, place et spectacle");
        $('.Fclasifica5').html("Limite de mise");
        $('.Fclasifica6').html("Limite de paiement");

        $('.Fhipodro1').html("Liste des hippodromes et leur classification");
        $('.Fhipodro2').html("Hippodrome");
        $('.Fhipodro3').html("Classification");
        $('.Fregion1').html("Hippodromes par région");

        $('.Fpoliti1').html("Politique et confidentialité");
        $('.Fpoliti2').html("Pour déposer des fonds et les mettre en service, il vous sera demandé de fournir des informations sur vous-même et votre carte de crédit ou de débit ou d'autres méthodes de paiement («Informations utilisateur»). Vos informations ne seront utilisées qu'à cette fin. Nous respectons les lois et réglementations appropriées relatives aux données personnelles, y compris la loi sur la protection des données. Universalrace utilise des «cookies» pour suivre vos préférences personnelles lors de l'utilisation du Service. Les cookies aident à assurer une meilleure expérience de jeu. Un 'cookie' est stocké sur le disque dur de votre ordinateur. Les cookies contiennent des informations pertinentes pour la session de jeu que vous avez commencée. Vous pouvez refuser d'accepter les cookies en modifiant les paramètres de votre navigateur. Cependant, si vous sélectionnez cette option, le Service peut être moins agréable pour vous. Sauf si vous avez ajusté les paramètres de votre navigateur pour refuser les cookies, ils seront émis sur votre système lorsque vous utilisez le site Web. En fournissant vos informations, vous pouvez également être en mesure de maintenir votre service et il peut être maintenu par nous ou par toute autre société qui les traite en notre nom. Ces tiers sont autorisés à utiliser les données uniquement conformément à nos instructions. Nous pouvons utiliser vos informations et services (y compris par courrier, téléphone, e-mail, d'autres services de messagerie électronique tels que les systèmes de messagerie texte ou automatique ou de télécopie). Nous pouvons également vous fournir des informations sur nos produits et services. Votre adresse e-mail est requise au moment de l'inscription. Un e-mail a été enregistré avec succès. Vous pouvez refuser de recevoir d'autres communications par e-mail de Universalrace en cliquant sur le lien de désabonnement dans le pied de page de chaque e-mail que vous recevez ou en contactant le service client. Vos informations ne seront pas transmises à des tiers à des fins de marketing sans votre consentement. Vous acceptez que Universalrace et ses partenaires commerciaux puissent utiliser votre nom, votre entrée, votre ressemblance, votre position et votre photographie dans le cadre du développement, de la production, de la distribution et / ou de l'exploitation (y compris le marketing et la promotion) du Service sans autre compensation pour vous (sauf si interdit par la loi), mais Universalrace et ses partenaires commerciaux ne le feront pas sans avoir obtenu au préalable le consentement. Nous acceptons qu'en cas de réorganisation, vente ou acquisition, nous pouvons être amenés à divulguer des informations personnelles à des tiers impliqués dans l'acquisition de la société.");
        
        $('.Fresponsab1').html("Jeu responsable");
        $('.Fresponsab2').html("Pour la plupart des joueurs, le jeu en ligne est une forme de divertissement agréable et simple. Cependant, en tant que site de jeu en ligne, nous avons le devoir de veiller à ce que des garanties adéquates et des garanties soient en place pour prévenir les joueurs qui ont des problèmes avec le jeu et nous assurer que vous avez bien 18 ans minimum. Nous vous proposons une gamme d'options pour vous aider à jouer de manière responsable. Si vous avez des questions sur le jeu responsable, vous pouvez nous contacter à info@universalrace.net<br><br> <strong>Limite de dépôt</strong><br><br> Il vous permet de limiter le montant d'argent que vous pouvez déposer quotidiennement, hebdomadairement ou mensuellement. Vous pouvez restreindre cette limite à tout moment et elle entrera en vigueur immédiatement. Vous pouvez également assouplir la limite, mais elle ne sera effective que 24 heures plus tard et vous devrez confirmer la modification avant l'application de la nouvelle limite. Notre équipe d'assistance peut vous aider à définir ces limites.<br><br> <strong>Total Auto-exclusion</strong><br><br> Cette fonctionnalité vous permet de suspendre / fermer votre compte pour une période spécifique d'une semaine, d'un mois, de trois mois, de six mois ou d'un an. Pendant cette période, vous ne pourrez pas vous connecter à votre compte Universalrace. Une autre option si vous avez besoin d'une longue pause, en contactant notre service client, vous pouvez fermer votre compte de façon permanente. Protection contre l'utilisation de mineurs La loi interdit à toute personne de moins de 18 ans d'ouvrir un compte ou de jouer sur Universalrace. Des mesures appropriées sont prises pour se protéger contre l'utilisation de notre casino par des joueurs mineurs. Par exemple, notre logiciel empêche l'enregistrement de toute personne de moins de 18 ans.<br> Cependant, si vous craignez qu'un mineur utilise votre ordinateur pour jouer à nos jeux, nous vous conseillons de le faire:<br><br> - Surveillez tout enfant ou mineur qui a accès à votre ordinateur lorsqu'il est connecté à SlotsMillion.<br> - Utiliser des applications de contrôle parental spécialisées, telles que Cyberpatrol, pour bloquer l'accès des mineurs aux sites de jeux.<br> - Utiliser des applications de contrôle parental spécialisées, telles que Cyberpatrol, pour bloquer l'accès des mineurs aux sites de jeux.<br> - Créez des profils différents pour toute personne qui accède à votre ordinateur afin que personne n'ait un accès direct à vos informations personnelles.<br> - Si vous connaissez un joueur de moins de 18 ans utilisant Universalrace, veuillez contacter immédiatement notre service client.<br><br> Veuillez noter que personne de moins de 18 ans ne peut prétendre à un gain Universalrace, et nous demandons une preuve d'identité au moment de votre premier retrait.<br><br> <strong>jouer en toute sécurité</strong><br><br> La plupart des joueurs parient selon leurs moyens. Cependant, il peut arriver un moment où un joueur sent qu'il ne peut plus contrôler son comportement de jeu.<br> Pour éviter que cette situation ne devienne une dépendance, nous vous conseillons de vous souvenir des règles simples suivantes:<br> - Pensez aux jeux uniquement comme divertissement, pas comme un moyen de gagner de l'argent.<br> - Vos pertes potentielles sont les coûts impliqués dans le jeu, et vos gains sont des bonus.<br> - Définissez une limite de temps qui ne doit pas être dépassée avant de commencer une session de jeu.<br> - Définissez une limite de perte et respectez-la. Attribuez un montant que vous pouvez vous permettre de perdre et placez-le dans votre hall.<br> - N'oubliez pas que vous devriez considérer vos pertes possibles. La plupart du temps, vous paierez pour le plaisir du jeu car les chances sont en faveur du casino.<br> - Renseignez-vous sur les signes avertissant d'un problème de dépendance afin de pouvoir l'éviter.<br> - En aucun cas, vous ne devez emprunter de l'argent pour jouer.<br> - Gardez votre vie sociale, familiale et professionnelle aussi riche que possible.<br> - N'essayez jamais de couvrir vos pertes.<br> - Veuillez noter que Vos pertes font partie du jeu et que tenter de les récupérer à tout prix entraînera plus de pertes.<br> ");
        
        $('.Freclama1').html("Réclamations et litiges");
        $('.Freclama2').html("<strong>1)</strong> Dans cette section, toute requête qui se réfère à la performance générale des activités, autre que la résolution de l'opération elle-même, sera classée comme une requête via la procédure de réclamation interne jusqu'à ce qu'il y ait une escalade vers le chef de la Service à la clientèle (ou, le cas échéant, un autre membre du personnel superieur), auquel cas l'affaire sera classée comme une 'réclamation'. Au final, ce sera la réponse du responsable du service client, là où elle a été escaladée, ce qui représente la dernière étape de cette procédure. <br><br> <strong>2)</strong> Lorsque la requête fait référence à la résolution d'une transaction de pari ou de jeu, elle deviendra un 'litige' où bien:<br> - 2.1 n'est pas résolu dans la première ou la deuxième phase de la procédure de réclamation et est soumis au Chef du Service Clientèle;<br> - 2.2 après la réponse du chef du service client, le plaignant n'est pas satisfait et soumet par la suite l'affaire à l'organe externe compétent pour décision.<br><br> <strong>3)</strong> Au cours de ce processus, toutes les communications téléphoniques et électroniques avec vous et toute autre personne peuvent être enregistrées. Si le contenu de ces communications est remis en question, nous consulterons ces dossiers et ils seront essentiels dans notre processus décisionnel, à moins que des preuves supplémentaires ne soient présentées.<br><br> <strong>4)</strong> Pendant le processus de résolution, vous devez accepter de ne pas divulguer l'existence ou le détail d'une enquête, d'une plainte ou d'un litige à des tiers, ce qui pourrait inclure des discussions dans des salons de discussion ou des forums équivalents et sera considéré comme une information confidentielle. En cas de divulgation non autorisée d'informations confidentielles, le processus de résolution sera suspendu. Dans ces circonstances, nous aurons également la possibilité de geler et potentiellement fermer votre compte.<br><br> <strong>Procédure de plainte</strong><br><br> <strong>5)</strong> La procédure de plainte est la suivante:<br><br> <strong>Étape 1</strong><br> Tout d'abord, vous devez contacter notre service client et un consultant (l'agent) enquêtera et répondra à votre demande. L'agent enregistrera l'appel / le message et notera votre réponse.<br><br> <strong>Étape 2</strong><br> Si votre demande n'est pas résolue à ce stade, l'affaire sera renvoyée au chef d'équipe / gestionnaire de l'agent. Le Team / Line Manager examinera la question et vous communiquera sa décision. Nous essaierons de vous répondre dans les 48 heures. Toutes les notifications que nous vous enverrons seront envoyées à l'adresse e-mail que vous nous fournissez lorsque vous enregistrez votre compte. Il est de votre responsabilité de nous informer de tout changement à cette adresse.<br><br> <strong>Étape 3</strong> <br> Si vous n'êtes toujours pas satisfait, l'affaire peut être renvoyée au directeur du service à la clientèle, auquel cas votre demande sera enregistrée comme une demande ou, lorsque l'affaire se rapporte à une transaction de pari ou de jeu, comme un différend.<br><br> <strong>Politique de règlement des plaintes et des différends</strong><br><br> <strong>6)</strong> La politique de règlement des plaintes et des différends est la suivante:<br><br> À la réception d'une demande par étapes, le directeur du service à la clientèle obtiendra un rapport de l'agent désigné et du directeur de ligne / d'équipe, selon le cas, et mènera toute autre enquête nécessaire et vous répondra. La réponse du responsable du service client représente la phase finale de la procédure de règlement des litiges et litiges internes.<br><br> Dans le cas où vous n'êtes pas satisfait d'un litige, vous avez la possibilité de soumettre le problème à l'extérieur via un autre fournisseur de règlement des litiges.<br><br> En lisant et en acceptant ces termes et conditions, le joueur aura l'obligation de se conformer à tout ce qui est stipulé.<br> <strong>1.</strong> Avant le report d'un événement, Universal Race procédera à la reprogrammation du pari en fonction de la nouvelle date fixe.<br> <strong>2.</strong> En cas d'annulation de tout événement, Universalrace recalculera le montant misé.<br> <strong>3.</strong> Universalrace dispose des autorizations de modifier ou d'augmenter ces termes et conditions, par conséquent, nous invitons les clients à toujours rester informés des changements via nos plateformes.<br> <strong>4.</strong> Le client est conscient qu'il doit s'informer périodiquement des termes et conditions, afin d'être toujours informé de toute mise à jour effectuée par Latinos bet.<br> <strong>5.</strong> S'il y a une erreur dans la probabilité ou toute autre information importante, par exemple: noms des équipes, cotes, résultats, événements, ligues, valeur spéciale en pariant Universalrace se réserve le droit d'annuler l'événement et par conséquent annuler les paris partiellement ou intégralement.<br> <strong>6.</strong> Avant tout pari considéré comme faisant partie d'un événement sportif organisé ou lorsqu'il y a eu des preuves de manipulation, de collusion avec des tiers, agissant en tant que (TESTAFERROS), en utilisant des (TESTAFERROS), en agissant pour le compte de tiers ou en utilisant des outils logiciels non autorisés. Universalrace se réserve le droit de réclamer les gains des paris concernés ou de les annuler / annuler. Le pari de valeur sera retourné dans de tels cas.<br> <strong>7.</strong> Si, en tout état de cause, il est évident que les joueurs ou clients ont agi frauduleusement ou en collusion avec des tiers, Universalrace informera les fédérations responsables et fournira les preuves rassemblées à cet égard.<br>");
        
        $('.faml1').html("Politique De LBC/CGV");
        $('.faml2').html("Procédures de lutte contre le blanchiment d'argent (LBC)");
        $('.faml3').html("<strong>Détection de LBC</strong><br><br> Les retraits de montants remboursés au moins une fois peuvent être demandés. Cela protège universalrace contre les abus à des fins de blanchiment d'argent et protège également notre système de dépôts gratuits qui peuvent être utilisés à notre détriment. Toutes les demandes de retrait sont initialement vérifiées, ce qui vous permet de surveiller toutes les demandes de retrait et d'indiquer celles qui sont suspectes, en fonction de tout historique de suspicion de fraude. Chaque client ne peut déposer qu'un montant maximum de US$ 1 200 par semaine. Les joueurs qui souhaitent déposer plus de fonds que la limite initialement fixée devront passer par un processus de vérification suffisant avec confirmation vocale, et seront régulièrement contrôlés pour l'activité de jeu. De plus, ces acteurs seront mis en évidence / tagués dans notre logiciel de back office, pour faciliter leur identification, leur suivi et leur contrôle.<br><br> Mis à part les contrôles automatiques du système, toutes les transactions / comptes des clients sont filtrés pour detecter les multiple comptes, les personnes qui peuvent avoir été mises sur liste noire par toute organisation ou entreprise concernée, le comportement des transactions et des paris, les montants traités, les méthodes dépôt / retrait et autres choses qui peuvent conduire à une activité suspecte basée sur l'expérience à long terme de nos employés liés.<br><br> Si aucun problème de ce type n'est constaté, cette constatation sera transmise à notre service comptable pour un dernier contrôle et le paiement final pourra avoir lieu. Ce processus est terminé en un jour ouvrable.<br><br> <strong>Rapport LBC</strong><br><br> Dans tous les cas où Universalrace détecte et vérifie qu'une certaine forme de blanchiment d'argent est en cours ou tentée, cette activité sera signalée aux autorités des jeux de Curaçao, si nécessaire.");
        
        $('.Fkyc1').html("Directives CGV");
        $('.Fkyc2').html("<strong>Inscription des joueurs</strong><br><br> Pour ouvrir un compte avec Universalrace à partir du portail Web, l'utilisateur doit d'abord terminer le processus d'inscription et déposer des fonds avec lesquels il peut commencer à jouer. Les données d'enregistrement à remplir par tous les nouveaux clients comprennent le nom et le prénom, l'adresse, le pays de résidence, la date de naissance, l'adresse e-mail et la devise du compte. Tous les clients doivent être âgés d'au moins 18 ans et ne peuvent avoir qu'un seul compte de joueur à leur nom, comme spécifié dans le Règlement des Jeux de Distance de Curaçao. Par conséquent, aucun mineur ne peut jouer sur le site Web Universalrace. Les utilisateurs sans compte de joueur enregistré ne pourront pas jouer.<br><br> <strong>Vérifications nécessaires</strong><br><br> Les joueurs des pays / états restreints ne peuvent pas s'inscrire en raison des réglementations strictes de non-jeu dans leur pays. Ces pays sont également inclus dans les conditions générales afin que les joueurs puissent connaître ces restrictions avant de s'inscrire. Avant tout retrait, le client sera invité à fournir les documents suivants: preuve de compte bancaire, carte de crédit, facture de services publics (datant de moins de 3 mois) et carte d'identité. Cette documentation doit être vérifiée avant l'approbation du retrait. De plus, la vérification de la voix et du numéro de téléphone sera effectuée sur les clients à volume élevé et les clients présentant de graves irrégularités de jeu. Tous les documents seront officiellement demandés par e-mail pour les deux scénarios de diligence raisonnable, et seront classés conformément aux procédures internes de l'entreprise.<br><br> En cas de retrait cumulé par tout client de 2 300 USD ou plus, il sera obligé d'envoyer une preuve d'identité sous forme de documents scannés, tels que la carte d'identité scannée ou le passeport, pour confirmer son identité, et dans certains cas , une facture récente pour confirmer votre adresse.<br><br> Les retraits cumulés de 2 300 USD ou plus ne seront traités que lorsque le client demandant le retrait soumettra ces documents, et ils seront vérifiés et confirmés par le personnel de gestion de la fraude.<br><br> Chaque fois qu'un nouveau client dépose de l'argent et commence à jouer / parier sur le site Web Universalrace, ses transactions et son comportement de pari seront examinés et surveillés quotidiennement par notre système. Cette procédure nous permet d'analyser la façon dont chaque client utilise les produits sur nos sites Web, permettant aux joueurs d'être classés et les limites individuelles des joueurs à définir en fonction de leurs habitudes de paris.<br><br> <strong>Vérification de l'E-mail</strong><br><br> 'Lorsqu'un nouveau compte est créé, un lien de vérification est envoyé au compte de messagerie spécifié lors du processus d'inscription. Lorsque l'utilisateur clique sur le lien, le compte est créé avec succès. Ce n'est qu'à ce stade que le joueur peut déposer de l'argent et commencer à faire des paris.'");
        
        $('.Ffooter1').html("Universal Race est engagé à soutenir le <span class='color-red'>Jeu Responsable.</span> Parier avant 18 ans est un délit.");
        $('.Ffooter2').html("jouer en toute sécurité");
        $('.Ffooter3').html("En naviguant et tout en continuant à utiliser ce site Web, vous acceptez que nous utilisions des cookies pour améliorer votre expérience lors de votre visite sur notre site. Universal Race n'utilisera aucun cookie qui interfère avec votre vie privée, seulement ceux qui améliorent l'expérience d'utilisation de notre site Web.<br><br> Veuillez lire <span class='color-red'>notre FAQ </span> pour plus d'informations Universalrace.net");
        
        $('.Flogin1').html("Utilisateur ou e-mail");
        $('.Flogin2').html("mot de passe");
        $('.Flogin3').html("Afficher le mot de passe");
        $('.Flogin4').html("Inscrivez-vous");
        $('.Flogin5').html("Se connecter");
        $('.Flogin6').html("Entrez également avec");
        $('.Flogin7').html("mot de passe oublié ?");

        $('.Flregistro1').attr('placeholder','Utilisateur');
        $('.Flregistro2').attr('placeholder','Nom et prénom');
        $('.Flregistro3').attr('placeholder','Confirmer mot de passe');
        $('.Flregistro4').attr('placeholder','E-mail');
        $('.Flregistro5').attr('placeholder','mot de passe');
        $('.Flregistro6').attr('placeholder','Téléphone');
        $('.Flregistro7').attr('placeholder','Date de naissance');
        $('.Flregistro8').html("Devise");
        $('.Flregistro9').html("JE CONFIRME QUE J'AI AU MOINS 18 ANS");
        $('.Flregistro10').html("J'ACCEPTE LES CGV ET LA CONFIDENTIALITÉ <a href='term.html'>(TERMES ET CONDITIONS)</a>");
        $('.Flregistro11').html("RESTEZ À JOUR SUR LES PROMOTIONS");
        $('.Flregistro12').html("S'inscrire");

        $('.Fhelp1').html("nous travaillons pour nous améliorer!");
        $('.Fhelp2').html("Que pensez-vous de la page?");
        $('.Fhelp3').html("veuillez sélectionner un sujet");
        $('.Fhelp4').html("- Sélectionner un sujet -");
        $('.Fhelp5').html("suggestion");
        $('.Fhelp6').html("Accompli");
        $('.Fhelp7').html("Des questions");
        $('.Fhelp8').html("Erreur");
        $('.Fhelp9').html("Qu'aimeriez-vous partager avec nous?");
        $('.Fhelp10').html("Envoyer");
        $('.Fhelp11').html("Développé par : <span class='small-img'>UNIVERSAL SOFT</span>");
        break;

      case 'bra' : 
        console.log("Brasil")
        $('.registro-btn').html("Cadastre-se");
        $('.login-btn').html("Iniciar sessão");
        $('.Fmejorar').html("O que você quer melhorar!");
        $('.Finicio').html("começar");
        $('.Finicio2').html("Bem-vindo ao Universal Race");
        $('.Freglamentos').html("Regulamentos");
        $('.Fterminos').html("Termos e Condições");
        $('.Fcontactanos').html("Contate-Nos");
        
        $('.Ftexto1').html("AS MELHORES APOSTAS EM JOCKEY ");
        $('.Ftexto2').html("Nossa plataforma foi projetada para que você possa usá-la em qualquer dispositivo, fazer suas previsões, baixar o cronograma da corridas, ver os dias, ver os resultados de todas as corridas e aproveitar os vídeos ao vivo e fazer sua aposta.");
        $('.Ftexto3').html("A UNIVERSAL RACE OFERECE SEGURANÇA");
        $('.Ftexto4').html("Suas apostas são importantes, é por isso que estamos felizes em poder fazê-lo da maneira mais confortável e segura, como temos feito sempre. Voce pode fazer depósitos e retiradas facilmente, temos ademas, uma equipe de suporte ao cliente profissional que o ajudará com qualquer dúvida sobre sua conta 24 horas por dia, 7 dias por semana. Descubra o mundo emocionante das corridas ao vivo com os melhores do mercado e quem mas valoriza seus usuários.");
        
        $('.Fcontact1').html("Precisa entrar em contato conosco");
        $('.Fcontact2').html("Nosso serviço ao cliente está disponível por e-mail: ");
        
        $('.Freglament1').html("Regulamentos do sistema");
        $('.Freglament2').html("Sobre as Apostas");
        $('.Freglament3').html("<strong>1)</strong> Os pagamentos são baseados nos resultados das corridas oficiais das diferentes pistas de corridas até os limites da casa.<br> <strong>2)</strong> As apostas serão consideradas vencedoras apenas se os resultados das corridas apostadas forem exatamente precisos.<br> <strong>3)</strong> Nenhum pagamento de consolação é feito, apenas as apostas serão premiadas nas combinações corretas para cada corrida envolvida e serão pagas com base no que é indicado pela hipica, no caso de haver uma seleção menor e o valor, Se a hipica decidir pagar um prêmio, o pagamento será feito com a seleção exata da mesma maneira e com base no valor indicado pela hipica para sua seleção, independentemente do número de acertos pagos pela hipica ser menor que a seleção feita (aposta).<br> <strong>4)</strong> Existe um lucro líquido máximo em cada corrida com base na categoria em que a pista de corrida está localizada. Os movimentos de Dobro (Daily Double), Triplo (Pick 3), Pick 4, Pick 5 e Pick 6, contam como a última corrida na sequência no cálculo de pagamentos. A fórmula para calcular o pagamento será: O valor apostado entre a denominação da aposta e o valor pago da mesma. Nos casos Win-Place-Show, a denominação será sempre 2.<br> <strong>5)</strong> As apostas exóticas tomarão medidas somente se o Jockey Clube oferecer um valor de pagamento por ela. Se não houver pagamentos da hipica estabelecidos para uma opção de aposta, eles serão cancelados e reembolsados.<br> <strong>6)</strong> Os cavalos são identificados pelo número que eles carregam atrás deles durante a corrida, não pelo nome. Os clientes são responsáveis ​​por suas escolhas de número de cavalos e número de corrida.<br> <strong>7)</strong> Quando você aposta em um cavalo que está emparelhado (ENTRY), sua aposta inclui todos os cavalos correndo com esse número. Se uma parte da aposta não participar, todas as apostas permanecem válidas. Não há possibilidade de especificar que um determinado cavalo deve correr.<br> <strong>8)</strong> Qualquer aposta aceita após o início de uma corrida é anulada.<br> <strong>9)</strong> Após a aposta, não é possível cancelá-la.<br> <strong>10)</strong> É de responsabilidade do usuário verificar sua aposta antes de confirmar e depois de fazê-lo e notificar imediatamente qualquer erro apresentado pelo sistema, não somos responsáveis ​​por erros no momento da confirmação do bilhete, se não forem notificados imediatamente .<br> <strong>11)</strong> A Companhia reserva-se o direito de suspender ou modificar qualquer jogada em que haja um erro em relação à colocação de prêmios e horas, com base em outras casas de apostas e dados oficiais.");
       
        $('.Fretirados1').html("SCRATCHES ou RETIRADOS");
        $('.Fretirados2').html("<strong>1)</strong> Se um cavalo não correr (Scratches), todas as apostas: Vencedor, Segundo e Terceiro (Win / Place/ Show) serão canceladas e devolvidas ao cliente.<br> <strong>2)</strong> Para Exacta / Trifecta / Superfecta / Quiniela, as combinações em que o cavalo retirado participa também serão canceladas e reembolsadas, deixando o resto das combinações em jogo.<br> <strong>3)</strong> Para Duplas (Daily Double), Triplo (Pick 3), Pick 4 e Pick 6, se um cavalo não correr antes do início de qualquer uma das corridas, o dinheiro será devolvido ao usuário.<br>");
       
        $('.Fparticular1').html("Regras particulares");
        $('.Fparticular2').html("<strong>1)</strong> Daily Double: A aposta é ganha se você atingir o 1º lugar em duas corridas consecutivas, designadas como apostas Daily Doublé.<br> <strong>2)</strong> PICK 3 (3 hits): A aposta é vencedora se você atingir o 1º lugar em três corridas consecutivas, designadas como apostas PICK 3.<br> <strong>3)</strong> PICK 4 (4 hits): A aposta é ganha se você atingir o 1º lugar em quatro corridas consecutivas, designadas como apostas PICK 4.<br> <strong>4)</strong> PICK 5 (5 hits): A aposta é ganha se você atingir o 1º lugar em cinco corridas consecutivas, designadas como apostas PICK 5.<br> <strong>5)</strong> PICK 6 (6 hits): A aposta é ganha se você atingir o 1º lugar em seis corridas consecutivas, designadas como apostas PICK 6.<br> <strong>6)</strong> PICK 9 (9 hits): A aposta é ganha se você atingir o 1º lugar em nove corridas consecutivas, designadas como apostas PICK 9.<br>");
        
        $('.Fminimo1').html("Participantes mínimos por tipo de aposta");
        $('.Fminimo2').html("Tipo de aposta");
        $('.Fminimo3').html("Mínimo de participantes");
        
        $('.Fclasifica1').html("Classificação da pista de corrida e limites de apostas<br>Limites de apostas no ranking");
        $('.Fclasifica2').html("Classificação");
        $('.Fclasifica3').html("Mínimo Exóticas");
        $('.Fclasifica4').html("Mínimo win, place y show");
        $('.Fclasifica5').html("Limite de apostas");
        $('.Fclasifica6').html("Limite de pagamento");
        
        $('.Fhipodro1').html("Lista de pistas de corridas e sua classificação");
        $('.Fhipodro2').html("Hipódromo");
        $('.Fhipodro3').html("Classificação");
        $('.Fregion1').html("Pistas de corrida por região");
       
        $('.Fpoliti1').html("Política e Privacidade");
        $('.Fpoliti2').html("Para depositar fundos e colocá-los emserviço, você será solicitado a fornecerinformações sobre você e seu cartão decrédito ou débito ou outras formas depagamento ('Informações do usuário').Suas informações serão usadas apenaspara esse fim.Cumprimos as leis e regulamentosapropriados relacionados a dados pessoais,incluindo a Lei de Proteção de Dados.A Universal Race usa 'cookies' pararastrear suas preferências pessoaisenquanto usa o Serviço.Os cookies ajudam a garantir uma melhorexperiência de jogo.Um 'cookie' é armazenado no disco rígidodo seu computador.Os cookies contêm informações relevantespara a sessão do jogo que você iniciou.Você pode se recusar a aceitar cookiesalterando as configurações do navegador.No entanto, se você selecionar esta opção,o Serviço poderá ser menos agradável paravocê. A menos que você tenha ajustado asconfigurações do navegador para rejeitarcookies, eles serão emitidos no seu sistemaquando você usar o site.Ao fornecer suas informações, vocêtambém poderá manter seu serviço e elepoderá ser mantido por nós ou por qualqueroutra empresa que as processe em nossonome.Esses terceiros estão autorizados a usar osdados somente de acordo com nossasinstruções. Podemos usar suas informaçõese serviços (incluindo correio, telefone, email,outros serviços de mensagens eletrônicas,como texto ou sistemas de chamada ou faxautomático).Também podemos fornecer informaçõessobre nossos produtos e serviços.Seu endereço de e-mail é necessário nomomento do registro.Um email foi registrado com sucesso.Você pode optar por não receber maiscomunicações por email da Universal Raceclicando no link de cancelamento deinscrição no rodapé de cada e-mail recebidoou entrando em contato com o Atendimentoao Cliente.Suas informações não serão repassadas aterceiros para fins de marketing sem o seuconsentimento.Você concorda que a Universal Race e seusparceiros de negócios possam usar seunome, entrada, imagem, posição efotografia em relação ao desenvolvimento,produção, distribuição e / ou exploração(incluindo marketing e promoção) doServiço sem compensação adicional a você(a menos que proibido por lei), no entanto, aUniversal Race e seus parceiros denegócios não o farão sem primeiro obter oconsentimento.Aceitamos que, em caso de reorganização,venda ou aquisição, talvez seja necessáriodivulgar informações pessoais a terceirosenvolvidos na aquisição da empresa.");
       
        $('.Fresponsab1').html("Jogo responsável");
        $('.Fresponsab2').html("Para a maioria dos jogadores, os jogos online são uma forma agradável e simples de entretenimento. No entanto, como site de jogos de azar on-line, temos o dever de garantir a existência de salvaguardas e salvaguardas adequadas para evitar jogos de azar de menores e problemas de jogo. Oferecemos-lhe uma gama de opções para ajudá-lo a jogar com responsabilidade. Se você tiver alguma dúvida sobre o Jogo Responsável, entre em contato conosco em info@universalrace.net<br><br> <strong>Limites de depósito</strong><br><br> Permite limitar a quantidade de dinheiro que você pode depositar diariamente, semanalmente ou mensalmente. Você pode restringir esse limite a qualquer momento e ele entrará em vigor imediatamente. Você também pode afrouxar o limite, mas ele não entrará em vigor até 24 horas depois e precisará confirmar a alteração antes que o novo limite se aplique. Nossa equipe de suporte pode ajudá-lo a definir esses limites.<br><br> <strong>Auto-exclusão</strong><br><br> Esse recurso permite suspender / fechar sua conta por um período específico de uma semana, um mês, três meses, seis meses ou um ano. Durante esse período, você não poderá fazer login na sua conta Universal Race. Eles são outra opção, se você precisar de uma longa pausa, entrando em contato com o nosso serviço ao cliente para fechar sua conta permanentemente. Proteção contra o uso de menores: A lei proíbe qualquer pessoa com menos de 18 anos de abrir uma conta ou jogar na Universal Race. Medidas apropriadas são tomadas para proteger contra o uso de nosso cassino por jogadores menores de idade. Por exemplo, nosso software impede o registro de menores de 18 anos.<br> No entanto, se você estiver preocupado com o fato de um menor estar usando seu computador para jogar nossos jogos, recomendamos que você faça isso:<br><br> -Monitorize qualquer criança ou menor que tenha acesso ao seu computador quando conectado ao Slots Million.<br> -Use aplicativos especializados de controle parental, como o Cyberpatrol, para bloquear o acesso de menores a sites de apostas.<br> -Manter todas as informações sobre sua conta de jogador e detalhes do seu cartão de crédito em sigilo.<br> -Crie perfis diferentes para quem acessa seu computador, para que ninguém tenha acesso direto às suas informações pessoais.<br> --Se você conhece um jogador com menos de 18 anos usando o Universal Race, entre em contato com nossa equipe de atendimento ao cliente imediatamente.<br><br> Observe que ninguém com menos de 18 anos é elegível para reivindicar qualquer vitória da Universal Race e solicitamos prova de identidade no momento de seu primeiro saque.<br><br> <strong>JOGO SEGURO</strong><br><br> A maioria dos jogadores aposta dentro de suas possibilidades. No entanto, pode chegar um momento em que um jogador sinta que não pode mais controlar seu comportamento no jogo.<br> Para evitar que essa situação se torne um vício, recomendamos que você lembre-se das seguintes regras simples:<br> -Pense nos jogos apenas como entretenimento, não como uma maneira de ganhar dinheiro.<br> -Suas perdas potenciais são os custos envolvidos no jogo e seus ganhos são bônus.<br> Defina um limite de tempo que não deve ser excedido antes de iniciar uma sessão de jogo.<br> Defina um limite de perda e cumpra-o. Atribua um valor que você pode perder e coloque-o no seu lobby.<br> -Lembre-se de que você deve considerar suas possíveis perdas. Na maioria das vezes, você pagará pela diversão do jogo, pois as probabilidades estão a favor do cassino.<br> - Aprenda sobre os sinais de alerta de um problema de dependência, para que você possa evitá-lo.<br> - Sob nenhuma circunstância você deve pedir dinheiro emprestado para jogar.<br> -Manter sua vida social, familiar e profissional o mais rica possível.<br> -Nunca tente cobrir suas perdas.<br> -Por favor, note que suas perdas fazem parte do jogo e que tentar recuperá-las a todo custo levará a mais perdas.<br>");
       
        $('.Freclama1').html("Reivindicações e disputas");
        $('.Freclama2').html("<strong>1)</strong> Nesta seção, qualquer consulta que se refira ao desempenho geral das atividades, exceto a resolução da operação em si, será classificada como uma consulta através do procedimento de reclamações internas até que haja uma escalação para o Chefe da Atendimento ao Cliente (ou, se for o caso, outro membro sênior da equipe), quando o assunto será classificado como 'Reivindicação'. Por fim, será a resposta do Chefe do Atendimento ao Cliente, onde foi encaminhado, o que representa a etapa final deste procedimento.<br><br> <strong>2)</strong> Quando a consulta se refere à resolução de uma transação de apostas ou apostas, ela se tornará uma 'disputa', onde:<br> - 2.1 não é resolvido na primeira ou segunda fase do processo de reclamações e é submetido ao chefe do departamento de atendimento ao cliente;<br> - 2.2 após a resposta do chefe do departamento de atendimento ao cliente, o reclamante está insatisfeito e, posteriormente, remete o assunto para o órgão externo competente para uma decisão.<br><br> <strong>3)</strong> Durante esse processo, todas as comunicações por telefone e e-mail com você e qualquer outra pessoa podem ser gravadas. Se o conteúdo dessas comunicações for questionado, consultaremos esses registros e eles serão fundamentais em nosso processo de tomada de decisão, a menos que sejam apresentadas evidências adicionais.<br><br> <strong>4)</strong> Durante o processo de resolução, você deve concordar em não divulgar a existência ou os detalhes de qualquer investigação, reclamação ou controvérsia a terceiros, que podem incluir discussões em salas de bate- papo ou fóruns equivalentes e serão consideradas informações confidenciais. No caso de divulgação não autorizada de informações confidenciais, o processo de resolução será suspenso. Nessas circunstâncias, também poderemos congelar e potencialmente fechar sua conta.<br><br> <strong>Procedimento de reclamação</strong><br><br> <strong>5)</strong> O procedimento de reclamações é o seguinte:<br><br> <strong>Estágio 1</strong><br> Primeiro, você deve entrar em contato com nossa equipe de atendimento ao cliente e um consultor (o agente) investigará e responderá à sua pergunta. O agente gravará a chamada / mensagem e anotará sua resposta.<br><br> <strong>Estágio 2</strong><br> Caso sua consulta não seja resolvida nesse estágio, o assunto será encaminhado ao líder / gerente da equipe do agente. O Gerente de Equipe / Linha analisará o assunto e comunicará sua decisão a você. Tentaremos responder dentro de 48 horas. Todas as notificações que enviarmos serão enviadas para o endereço de e-mail que você nos fornece ao registrar sua conta. É de sua responsabilidade nos notificar sobre quaisquer alterações neste endereço.<br><br> <strong>Estágio 3</strong> <br> Se você ainda estiver insatisfeito, o assunto poderá ser encaminhado ao Diretor de Atendimento ao Cliente, quando sua solicitação será registrada como uma Demanda ou, quando o assunto estiver relacionado a uma transação de jogo ou jogo, como uma Disputa.<br><br> <strong>Política de reclamação e resolução de disputas</strong><br><br> <strong>6)</strong> A política de resolução de reclamações e disputas é a seguinte: Após o recebimento de uma consulta em fases, o Diretor de Atendimento ao Cliente obterá um relatório do Agente Designado e do Diretor de Linha / Equipe, conforme apropriado, e conduzirá quaisquer outras consultas necessárias e responderá a você. A resposta do gerente de atendimento ao cliente representa a fase final do procedimento de reclamações e disputas internas. No caso de você estar insatisfeito com uma disputa, você tem a opção de encaminhar o assunto externamente através de um provedor de resolução alternativa de disputas.<br><br> Ao ler e aceitar estes termos e condições, o jogador terá a obrigação de cumprir tudo o que estipulado.<br> <strong>1.</strong> Antes do adiamento de um evento, a Universalrace continuará a reprogramar a aposta de acordo com a nova data definida.<br> <strong>2.</strong> Após o cancelamento de qualquer evento, a Universalrace recalculará o valor apostado.<br> <strong>3.</strong> Universalrace Você tem permissão para modificar ou aumentar estes termos e condições; portanto, convidamos os clientes a manterem-se sempre informados sobre as mudanças em nossas plataformas.<br> <strong>4.</strong> O cliente está ciente de que deve se informar periodicamente sobre os termos e condições, para estar sempre informado sobre qualquer atualização feita pela aposta nos latinos.<br> <strong>5.</strong> Se houver um erro na probabilidade ou em qualquer outra informação importante, por exemplo: Nomes de equipes, probabilidades, resultados, eventos, ligas, valor especial em apostas A Universalrace se reserva o direito de cancelar o evento e, consequentemente, cancelar o Aposte parcial ou totalmente.<br> <strong>6.</strong> Antes de qualquer aposta que seja considerada parte de um evento esportivo organizado ou onde tenha havido evidência de manipulação, conluio com terceiros, atuando como figura de proa, usando figuras, atuando em nome de terceiros ou usando ferramentas de software não permitidas. A Universalrace reserva-se o direito de reivindicar os ganhos das apostas envolvidas ou de os cancelar / cancelar. A aposta em valor será devolvida nesses casos.<br> <strong>7.</strong> Se, em qualquer caso, for evidente que os jogadores ou clientes agiram de forma fraudulenta ou em conluio com terceiros, a Universalrace informará as Federações responsáveis ​​e fornecerá as evidências reunidas a esse respeito.<br>");
       
        $('.faml1').html("Política De Aml/Kyc");
        $('.faml2').html("Procedimentos de combate à lavagem de dinheiro (LBC)");
        $('.faml3').html("<strong>Detección de AML</strong><br><br> Retiradas de valores que foram devolvidos pelo menos uma vez podem ser solicitadas. Isso protege a universalrace de ser abusada para fins de lavagem de dinheiro e também protege nosso sistema de depósitos gratuitos que podem ser abusados ​​em nossa desvantagem. Todas as solicitações de retirada são verificadas inicialmente, permitindo monitorar todas as solicitações de retirada e denotar suspeitas, com base em qualquer histórico de suspeita de fraude. Cada cliente pode depositar apenas o valor máximo de US $ 1.200 por semana. Os jogadores que desejam depositar mais fundos do que o limite inicialmente definido terão que passar por um processo de verificação suficiente com confirmação por voz e serão monitorados regularmente quanto à atividade de jogo. Além disso, esses players serão destacados / identificados em nosso software de back office, para facilitar sua identificação, monitoramento e controle.<br><br> Além dos controles automáticos do sistema, todas as transações / contas de clientes são rastreadas para várias contas, indivíduos que podem estar na lista negra de qualquer organização ou empresa relevante, comportamento de transações e apostas, valores manipulados, métodos depósito / retirada e outras coisas que podem levar a qualquer atividade suspeita com base na experiência de longo prazo de nossos funcionários relacionados.<br><br> Se nenhum desses problemas for encontrado, essa descoberta será enviada ao nosso departamento de contabilidade para uma verificação final e o pagamento final poderá ocorrer. Esse processo é concluído em um dia útil.<br><br> <strong>Relatórios AML</strong><br><br> Em todos os casos em que a Universal Race encontre e verifique se alguma forma de lavagem de dinheiro está sendo realizada ou está sendo tentada, essa atividade será relatada às autoridades de jogo de Curaçao, conforme necessário.");
        
        $('.Fkyc1').html("Diretivas KYC");
        $('.Fkyc2').html("<strong>Registo do Jogador</strong><br><br> Para abrir uma conta no Universal Race a partir do portal da web, o usuário deve primeiro concluir o processo de registro e depositar alguns fundos com os quais pode começar a jogar. Os dados de registro a serem preenchidos por todos os novos clientes incluem o nome e sobrenome, o endereço, o país de residência, a data de nascimento, o endereço de e-mail e a moeda da conta. Todos os clientes devem ter pelo menos 18 anos de idade e podem ter apenas uma conta de jogador em seu nome, conforme especificado no Regulamento de jogos de Curaçao. Portanto, nenhum menor pode jogar no site da Universalrace. Usuários sem uma conta de jogador registrada não poderão jogar.<br><br> <strong>Diligência devida</strong><br><br> Jogadores de países / estados restritos não podem se registrar devido a regulamentos estritos de não-jogo em seu país. Esses países também estão incluídos nos Termos e Condições para que os jogadores possam conhecer essas restrições antes de se registrar. Antes de qualquer saque, o cliente deverá solicitar a seguinte documentação: comprovante de conta bancária, cartão de crédito, conta de serviços públicos (não mais de 3 meses) e cartão de identificação. Essa documentação deve ser verificada antes da retirada ser aprovada. Além disso, a verificação de números de telefone e voz será realizada em clientes de alto volume e em clientes com graves irregularidades no comportamento das apostas. Toda a documentação será formalmente solicitada por e-mail para os dois cenários de due diligence e será arquivada de acordo com os procedimentos internos da empresa.<br><br> Em caso de retirada acumulada por qualquer cliente de 2.300 usd ou mais, ele será obrigado a enviar uma prova de identidade na forma de documentos digitalizados, como o cartão de identidade ou passaporte digitalizado, para confirmar sua identidade e, em certos casos, , uma fatura recente para confirmar seu endereço.<br><br> As retiradas cumulativas de US $ 2.300 ou mais não serão processadas até que o cliente que solicita a retirada envie esses documentos, e eles são verificados e confirmados pela equipe de Gerenciamento de fraudes.<br><br> Sempre que um novo cliente deposita dinheiro e começa a jogar / apostar no site da Universal Race, suas transações e comportamento de apostas serão revisados ​​e monitorados diariamente pelo nosso sistema. Este procedimento nos permite analisar como cada cliente usa os produtos em nossos sites, permitindo que os jogadores sejam categorizados e que os limites de cada jogador sejam definidos de acordo com seus padrões de apostas.<br><br> <strong>Verificação de e-mail</strong><br><br> Quando uma nova conta é criada, um link de verificação é enviado para a conta de email especificada durante o processo de registro. Quando o usuário clica no link, a conta é criada com sucesso. Somente neste momento o jogador pode depositar dinheiro e começar a fazer apostas.");
        
        $('.Ffooter1').html("A Universal Race apoia o <span class='color-red'>Jogo Responsável.</span> Estao prohibidas as apostas para menores de 18 anos.");
        $('.Ffooter2').html("Jogo seguro");
        $('.Ffooter3').html("Ao acessar, continuar a usar ou navegar neste site, você concorda que usamos cookies para melhorar sua experiência em nosso site. A Universal Race não usará nenhum cookie que interfira na sua privacidade; somente aqueles que melhoram a experiência de uso do nosso site. <br><br>por favor, leia nossas <span class='color-red'>Perguntas frequentes</span> para obter mais informações Universalrace.net ");
        
        $('.Flogin1').html("Usuário ou E-mail");
        $('.Flogin2').html("Senha");
        $('.Flogin3').html("Lembrar nome de usuário");
        $('.Flogin4').html("Registro ahora");
        $('.Flogin5').html("Entrar");
        $('.Flogin6').html("Entre também com");
        $('.Flogin7').html("Esqueceu sua senha?");

        $('.Flregistro1').attr('placeholder','Do utilizador');
        $('.Flregistro2').attr('placeholder','Nome e sobrenome');
        $('.Flregistro3').attr('placeholder','Confirmar senha');
        $('.Flregistro4').attr('placeholder','O email');
        $('.Flregistro5').attr('placeholder','Senha');
        $('.Flregistro6').attr('placeholder','telefone');
        $('.Flregistro7').attr('placeholder','Data de nascimento');
        $('.Flregistro8').html("Moeda");
        $('.Flregistro9').html("CONFIRMO QUE TENHO PELO MENOS 18 ANOS");
        $('.Flregistro10').html("ACEITO OS T&C E A PRIVACIDADE <a href='term.html'>(TERMOS E CONDIÇÕES)</a>");
        $('.Flregistro11').html("MANTENHA-SE ATUALIZADO EM PROMOÇÕES");
        $('.Flregistro12').html("Registro");

        $('.Fhelp1').html("Trabalhamos para melhorar!");
        $('.Fhelp2').html("O que você acha da página?");
        $('.Fhelp3').html("Por favor selecione um tópico");
        $('.Fhelp4').html("- Selecione um topico -");
        $('.Fhelp5').html("Sugestão");
        $('.Fhelp6').html("Elogio");
        $('.Fhelp7').html("Ask");
        $('.Fhelp8').html("Erro");
        $('.Fhelp9').html("O que você gostaria de compartilhar conosco?");
        $('.Fhelp10').html("Enviar");
        $('.Fhelp11').html("Desenvolvido por <span class='small-img'>UNIVERSAL SOFT</span>");
        break;
    }
    
  });

});
