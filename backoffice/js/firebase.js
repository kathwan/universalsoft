  // Your web app's Firebase configuration
  var firebaseConfig = {
    apiKey: "AIzaSyC_49owAzRSL5RNnr6z5ud5WOT7F0Km8QY",
    authDomain: "admin-universal.firebaseapp.com",
    databaseURL: "https://admin-universal.firebaseio.com",
    projectId: "admin-universal",
    storageBucket: "admin-universal.appspot.com",
    messagingSenderId: "539212963515",
    appId: "1:539212963515:web:f51451adddbb9173c2d39e",
    measurementId: "G-QJXPYTDDZ6"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  
  var db = firebase.firestore();
  var storageRef = firebase.storage().ref();

    // leer datos
    var data_video = document.getElementById('index-video')

    db.collection("video_banner").onSnapshot((querySnapshot) => {
        data_video.innerHTML = '';
        querySnapshot.forEach((doc) => {
            console.log(`${doc.id} => ${doc.data()}`);
            data_video.innerHTML += `
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Video Banner</div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="video1" aria-describedby="videoinput">
                            <label class="custom-file-label" for="video1">${doc.data().Video}</label>
                            <input type="text" class="hidden" id="video2" value="${doc.data().Video}">
                        </div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-play-circle fa-2x text-gray-300"></i>
                    </div>
                    <div class="col-12 mt-3">
                        <video class="slider-video" width="100%" preload="auto" loop="false" autoplay="false" controls style="visibility: visible; width: 100%;" poster="img/400x400.png">
                            <source src="${doc.data().Video}" type="video/mp4">
                        </video>
                    </div>
                </div>
            `
        });
    });

    var data_nosotros = document.getElementById('index-nosotros')

    db.collection("nosotros").onSnapshot((querySnapshot) => {
        data_nosotros.innerHTML = '';
        querySnapshot.forEach((doc) => {
            console.log(`${doc.id} => ${doc.data()}`);
            data_nosotros.innerHTML += `
                <label for="nosotros1">Cambiar imagen</label>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="nosotros1" aria-describedby="inputfile" lang="es">
                    <label id="label-nosotros" class="custom-file-label" for="nosotros1">${doc.data().Imagen}</label>
                    <input type="text" class="hidden" id="nosotros2" value="${doc.data().Imagen}">
                </div>
                <div class="form-group">
                    <label for="formGroupExampleInput">Coloque un titulo</label>
                    <input type="text" class="form-control" id="nosotros-titulo" placeholder="Titulo" value="${doc.data().Titulo}">
                </div>
                <div class="form-group">
                    <label for="nosotros-textarea">Coloque el contenido</label>
                    <textarea class="form-control" id="nosotros-textarea" rows="3">${doc.data().Contenido}</textarea>
                </div>
            `
        });
    });

    var data_servicios = document.getElementById('index-servicios')

    db.collection("servicios").onSnapshot((querySnapshot) => {
        data_servicios.innerHTML = '';
        querySnapshot.forEach((doc) => {
            console.log(`${doc.id} => ${doc.data()}`);
            data_servicios.innerHTML += `
            <div class="row">
                <div class="col-xl-3 col-lg-3 form-group">
                    <label for="servicio-titulo">Titulo Principal</label>
                    <input type="text" class="form-control" id="servicio-titulo" placeholder="Titulo" value="${doc.data().Titulo}">
                </div>
                <div class="w-100"><hr></div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-lg-3 form-group">
                    <input type="file" class="custom-file-input" id="servicios-bg1" lang="es">
                    <label class="custom-file-label" for="servicios-bg1">${doc.data().Servicio_imagen1}</label>
                    <input type="text" class="hidden" id="servicios-in1" value="${doc.data().Servicio_imagen1}">
                </div>
                <div class="col-xl-3 col-lg-3 form-group">
                    <input type="file" class="custom-file-input" id="servicios-bg2" lang="es">
                    <label class="custom-file-label" for="servicios-bg2">${doc.data().Servicio_imagen2}</label>
                    <input type="text" class="hidden" id="servicios-in2" value="${doc.data().Servicio_imagen2}">
                </div>
                <div class="col-xl-3 col-lg-3 form-group">
                    <input type="file" class="custom-file-input" id="servicios-bg3" lang="es">
                    <label class="custom-file-label" for="servicios-bg3">${doc.data().Servicio_imagen3}</label>
                    <input type="text" class="hidden" id="servicios-in3" value="${doc.data().Servicio_imagen3}">
                </div>
                <div class="col-xl-3 col-lg-3 form-group">
                    <input type="file" class="custom-file-input" id="servicios-bg4" lang="es">
                    <label class="custom-file-label" for="servicios-bg4">${doc.data().Servicio_imagen4}</label>
                    <input type="text" class="hidden" id="servicios-in4" value="${doc.data().Servicio_imagen4}">
                </div>
                <div class="col-xl-3 col-lg-3 form-group">
                    <input type="file" class="custom-file-input" id="servicios-bg5" lang="es">
                    <label class="custom-file-label" for="servicios-bg5">${doc.data().Servicio_imagen5}</label>
                    <input type="text" class="hidden" id="servicios-in5" value="${doc.data().Servicio_imagen5}">
                </div>
                <div class="col-xl-3 col-lg-3 form-group">
                    <input type="file" class="custom-file-input" id="servicios-bg6" lang="es">
                    <label class="custom-file-label" for="servicios-bg6">${doc.data().Servicio_imagen6}</label>
                    <input type="text" class="hidden" id="servicios-in6" value="${doc.data().Servicio_imagen6}">
                </div>
                <div class="col-xl-3 col-lg-3 form-group">
                    <input type="file" class="custom-file-input" id="servicios-bg7" lang="es">
                    <label class="custom-file-label" for="servicios-bg7">${doc.data().Servicio_imagen7}</label>
                    <input type="text" class="hidden" id="servicios-in7" value="${doc.data().Servicio_imagen7}">
                </div>
                <div class="col-xl-3 col-lg-3 form-group">
                    <input type="file" class="custom-file-input" id="servicios-bg8" lang="es">
                    <label class="custom-file-label" for="servicios-bg8">${doc.data().Servicio_imagen8}</label>
                    <input type="text" class="hidden" id="servicios-in8" value="${doc.data().Servicio_imagen8}">
                </div>
                <div class="w-100"><hr></div>
                <div class="col-xl-3 col-lg-3 form-group">
                    <label for="titulo-servi1">Titulo Servicio 1</label>
                    <input type="text" class="form-control" id="titulo-servi1" placeholder="Titulo" value="${doc.data().Servicio_text1}">
                </div>
                <div class="col-xl-3 col-lg-3 form-group">
                    <label for="titulo-servi2">Titulo Servicio 2</label>
                    <input type="text" class="form-control" id="titulo-servi2" placeholder="Titulo" value="${doc.data().Servicio_text2}">
                </div>
                <div class="col-xl-3 col-lg-3 form-group">
                    <label for="titulo-servi3">Titulo Servicio 3</label>
                    <input type="text" class="form-control" id="titulo-servi3" placeholder="Titulo" value="${doc.data().Servicio_text3}">
                </div>
                <div class="col-xl-3 col-lg-3 form-group">
                    <label for="titulo-servi4">Titulo Servicio 4</label>
                    <input type="text" class="form-control" id="titulo-servi4" placeholder="Titulo" value="${doc.data().Servicio_text4}">
                </div>
                <div class="col-xl-3 col-lg-3 form-group">
                    <label for="titulo-servi5">Titulo Servicio 5</label>
                    <input type="text" class="form-control" id="titulo-servi5" placeholder="Titulo" value="${doc.data().Servicio_text5}">
                </div>
                <div class="col-xl-3 col-lg-3 form-group">
                    <label for="titulo-servi6">Titulo Servicio 6</label>
                    <input type="text" class="form-control" id="titulo-servi6" placeholder="Titulo" value="${doc.data().Servicio_text6}">
                </div>
                <div class="col-xl-3 col-lg-3 form-group">
                    <label for="titulo-servi7">Titulo Servicio 7</label>
                    <input type="text" class="form-control" id="titulo-servi7" placeholder="Titulo" value="${doc.data().Servicio_text7}">
                </div>
                <div class="col-xl-3 col-lg-3 form-group">
                    <label for="titulo-servi8">Titulo Servicio 8</label>
                    <input type="text" class="form-control" id="titulo-servi8" placeholder="Titulo" value="${doc.data().Servicio_text8}">
                </div>
                
            </div>
            `
        });
    });

    var data_productos = document.getElementById('index-productos')

    db.collection("productos").onSnapshot((querySnapshot) => {
        data_productos.innerHTML = '';
        querySnapshot.forEach((doc) => {
            console.log(`${doc.id} => ${doc.data()}`);
            data_productos.innerHTML += `
            <div class="row">
                <div class="col-xl-4 col-lg-4 form-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="productos-bg1">
                        <label class="custom-file-label" for="productos-bg1">${doc.data().Proimg1}</label>
                        <input type="text" class="hidden" id="productos-in1" value="${doc.data().Proimg1}">
                    </div>
                    <div class="form-group mt-3 text-center">
                        <img class="img-fluid" src="${doc.data().Proimg1}" alt="">
                    </div>
                    <label for="producto-titulo1">Titulo</label>
                    <input type="text" class="form-control" id="producto-titulo1" placeholder="Titulo" value="${doc.data().ProTitle1}">
                    <label for="producto-subtitulo1">Sub-titulo</label>
                    <input type="text" class="form-control" id="producto-subtitulo1" placeholder="Sub-titulo" value="${doc.data().ProSubtitle1}">
                    <label for="producto-textarea1">Contenido</label>
                    <textarea class="form-control" id="producto-textarea1" rows="3">${doc.data().ProContent1}</textarea>
                </div>

                <div class="col-xl-4 col-lg-4 form-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="productos-bg2">
                        <label class="custom-file-label" for="productos-bg2">${doc.data().Proimg2}</label>
                        <input type="text" class="hidden" id="productos-in2" value="${doc.data().Proimg2}">
                    </div>
                    <div class="form-group mt-3 text-center">
                        <img class="img-fluid" src="${doc.data().Proimg2}" alt="">
                    </div>
                    <label for="producto-titulo2">Titulo</label>
                    <input type="text" class="form-control" id="producto-titulo2" placeholder="Titulo" value="${doc.data().ProTitle2}">
                    <label for="producto-subtitulo2">Sub-titulo</label>
                    <input type="text" class="form-control" id="producto-subtitulo2" placeholder="Titulo" value="${doc.data().ProSubtitle2}">
                    <label for="producto-textarea2">Contenido</label>
                    <textarea class="form-control" id="producto-textarea2" rows="3">${doc.data().ProContent2}</textarea>
                </div>

                <div class="col-xl-4 col-lg-4 form-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="productos-bg3" aria-describedby="inputGroupFileAddon01">
                        <label class="custom-file-label" for="productos-bg3">${doc.data().Proimg3}</label>
                        <input type="text" class="hidden" id="productos-in3" value="${doc.data().Proimg3}">
                    </div>
                    <div class="form-group mt-3 text-center">
                        <img class="img-fluid" src="${doc.data().Proimg3}" alt="">
                    </div>
                    <label for="producto-titulo3">Titulo</label>
                    <input type="text" class="form-control" id="producto-titulo3" placeholder="Titulo" value="${doc.data().ProTitle3}">
                    <label for="producto-subtitulo3">Sub-titulo</label>
                    <input type="text" class="form-control" id="producto-subtitulo3" placeholder="Titulo" value="${doc.data().ProSubtitle3}">
                    <label for="producto-textarea3">Contenido</label>
                    <textarea class="form-control" id="producto-textarea3" rows="3">${doc.data().ProContent3}</textarea>
                </div>
            </div>
            `
        });
    });
    var data_footer = document.getElementById('index-footer')

    db.collection("footers").onSnapshot((querySnapshot) => {
        data_footer.innerHTML = '';
        querySnapshot.forEach((doc) => {
            console.log(`${doc.id} => ${doc.data()}`);
            data_footer.innerHTML += `
            <div class="row">
                <div class="col-12 form-group">
                <label for="footer_titulo">Titulo de terminos y condiciones</label>
                <input type="text" class="form-control" id="footer_titulo" placeholder="Titulo" value="${doc.data().title}">

                <label for="footer_link1">Facebook</label>
                <input type="text" class="form-control" id="footer_link1" placeholder="Facebook" value="${doc.data().facebook}">

                <label for="footer_link2">Instagram</label>
                <input type="text" class="form-control" id="footer_link2" placeholder="Instagram" value="${doc.data().instagram}">

                <label for="footer_link3">Linkedin</label>
                <input type="text" class="form-control" id="footer_link3" placeholder="Linkedin" value="${doc.data().linkedin}">

                <label for="footer_link4">Issuu</label>
                <input type="text" class="form-control" id="footer_link4" placeholder="Issuu" value="${doc.data().issuu}">
                </div>
            </div>
            `
        });
    });
//   agregar datos

// $('#btn-productos').click(function(){
//     var pro_img1 = document.getElementById('productos-bg1').value;
//     var pro_img2 = document.getElementById('productos-bg2').value;
//     var pro_img3 = document.getElementById('productos-bg3').value;

//     var pro_title1 = document.getElementById('producto-titulo1').value;
//     var pro_title2 = document.getElementById('producto-titulo2').value;
//     var pro_title3 = document.getElementById('producto-titulo3').value;

//     var pro_subtitle1 = document.getElementById('producto-subtitulo1').value;
//     var pro_subtitle2 = document.getElementById('producto-subtitulo2').value;
//     var pro_subtitle3 = document.getElementById('producto-subtitulo3').value;

//     var pro_content1 = document.getElementById('producto-textarea1').value;
//     var pro_content2 = document.getElementById('producto-textarea2').value;
//     var pro_content3 = document.getElementById('producto-textarea3').value;


//     db.collection("productos").add({
//         Proimg1: pro_img1,
//         Proimg2: pro_img2,
//         Proimg3: pro_img3,
//         ProTitle1: pro_title1,
//         ProTitle2: pro_title2,
//         ProTitle3: pro_title3,
//         ProSubtitle1: pro_subtitle1,
//         ProSubtitle2: pro_subtitle2,
//         ProSubtitle3: pro_subtitle3,
//         ProContent1: pro_content1,
//         ProContent2: pro_content2,
//         ProContent3: pro_content3
//     })
//     .then(function(docRef) {
//         console.log("Document written with ID: ", docRef.id);
//     })
//     .catch(function(error) {
//         console.error("Error adding document: ", error);
//     });
// });

$('#btn-video').click(function(){

    var editar_video = db.collection("video_banner").doc("Zo1rmSVJqT5Sn5DHOX2S");
    var video_guardada = document.getElementById('video2').value;
    var video_nuevo = document.getElementById('video1').files[0];

    if(video_nuevo == undefined){
        var video = video_guardada;
        var videoUrl = video;
        console.log("aca 1", videoUrl , "aca 2" , video);
    }else{
        var video = video_nuevo;
        var videoName = video.name;
        var videoUrl = "http://localhost/sistema-universalsoft/img/"+videoName;
    }

    return editar_video.update({
        Video: videoUrl
    })
    .then(function() {
        console.log("Document successfully updated!");
    })
    .catch(function(error) {
        // The document probably doesn't exist.
        console.error("Error updating document: ", error);
    });
});

$('#btn-nosotros').click(function(){

    var editar_nosotros = db.collection("nosotros").doc("OO6KBes70eADfuCezTLd");
    var imagen_guardada = document.getElementById('nosotros2').value;
    var imagen_nueva = document.getElementById('nosotros1').files[0];
    
    if(imagen_nueva == undefined){
        var image = imagen_guardada;
        var imageUrl = image;
        console.log("aca 1", imageUrl , "aca 2" , image);
    }else{
        var image = imagen_nueva;
        var imageName = image.name;
        var imageUrl = "http://localhost/sistema-universalsoft/img/"+imageName;
    }
    var titulo = document.getElementById('nosotros-titulo').value;
    var contenido = document.getElementById('nosotros-textarea').value;
    return editar_nosotros.update({
        Imagen: imageUrl,
        Titulo: titulo,
        Contenido: contenido
    })
    .then(function() {
        console.log("Document successfully updated!");
    })
    .catch(function(error) {
        // The document probably doesn't exist.
        console.error("Error updating document: ", error);
    });
})

$('#btn-servicios').click(function(){
    var editar_servicios = db.collection("servicios").doc("0mkl0IltfpLuLFWpIsrh");

    var servicios1 = document.getElementById('servicios-bg1').files[0];
    var servicios2 = document.getElementById('servicios-bg2').files[0];
    var servicios3 = document.getElementById('servicios-bg3').files[0];
    var servicios4 = document.getElementById('servicios-bg4').files[0];
    var servicios5 = document.getElementById('servicios-bg5').files[0];
    var servicios6 = document.getElementById('servicios-bg6').files[0];
    var servicios7 = document.getElementById('servicios-bg7').files[0];
    var servicios8 = document.getElementById('servicios-bg8').files[0];

    var nuevo_servicios1 = document.getElementById('servicios-in1').value;
    var nuevo_servicios2 = document.getElementById('servicios-in2').value;
    var nuevo_servicios3 = document.getElementById('servicios-in3').value;
    var nuevo_servicios4 = document.getElementById('servicios-in4').value;
    var nuevo_servicios5 = document.getElementById('servicios-in5').value;
    var nuevo_servicios6 = document.getElementById('servicios-in6').value;
    var nuevo_servicios7 = document.getElementById('servicios-in7').value;
    var nuevo_servicios8 = document.getElementById('servicios-in8').value;

    if(servicios1 == undefined){
        var servi1 = nuevo_servicios1;
        var serviUrl1 = servi1;
    }else{
        var servi1 = servicios1;
        var serviName1 = servi1.name;
        var serviUrl1 = "http://localhost/sistema-universalsoft/img/"+serviName1;
    }

    if(servicios2 == undefined){
        var servi2 = nuevo_servicios2;
        var serviUrl2 = servi2;
    }else{
        var servi2 = servicios2;
        var serviName2 = servi2.name;
        var serviUrl2 = "http://localhost/sistema-universalsoft/img/"+serviName2;
    }

    if(servicios3 == undefined){
        var servi3 = nuevo_servicios3;
        var serviUrl3 = servi3;
    }else{
        var servi3 = servicios3;
        var serviName3 = servi3.name;
        var serviUrl3 = "http://localhost/sistema-universalsoft/img/"+serviName3;
    }

    if(servicios4 == undefined){
        var servi4 = nuevo_servicios4;
        var serviUrl4 = servi4;
    }else{
        var servi4 = servicios4;
        var serviName4 = servi4.name;
        var serviUrl4 = "http://localhost/sistema-universalsoft/img/"+serviName4;
    }

    if(servicios5 == undefined){
        var servi5 = nuevo_servicios5;
        var serviUrl5 = servi5;
    }else{
        var servi5 = servicios5;
        var serviName5 = servi5.name;
        var serviUrl5 = "http://localhost/sistema-universalsoft/img/"+serviName5;
    }

    if(servicios6 == undefined){
        var servi6 = nuevo_servicios6;
        var serviUrl6 = servi6;
    }else{
        var servi6 = servicios6;
        var serviName6 = servi6.name;
        var serviUrl6 = "http://localhost/sistema-universalsoft/img/"+serviName6;
    }

    if(servicios7 == undefined){
        var servi7 = nuevo_servicios7;
        var serviUrl7 = servi7;
    }else{
        var servi7 = servicios7;
        var serviName7 = servi7.name;
        var serviUrl7 = "http://localhost/sistema-universalsoft/img/"+serviName7;
    }

    if(servicios8 == undefined){
        var servi8 = nuevo_servicios8;
        var serviUrl8 = servi8;
    }else{
        var servi8 = servicios8;
        var serviName8 = servi8.name;
        var serviUrl8 = "http://localhost/sistema-universalsoft/img/"+serviName8;
    }

    var serviciostext1 = document.getElementById('titulo-servi1').value;
    var serviciostext2 = document.getElementById('titulo-servi2').value;
    var serviciostext3 = document.getElementById('titulo-servi3').value;
    var serviciostext4 = document.getElementById('titulo-servi4').value;
    var serviciostext5 = document.getElementById('titulo-servi5').value;
    var serviciostext6 = document.getElementById('titulo-servi6').value;
    var serviciostext7 = document.getElementById('titulo-servi7').value;
    var serviciostext8 = document.getElementById('titulo-servi8').value;

    var titulo = document.getElementById('servicio-titulo').value;
    
    return editar_servicios.update({
        Titulo: titulo,
        Servicio_imagen1: serviUrl1,
        Servicio_imagen2: serviUrl2,
        Servicio_imagen3: serviUrl3,
        Servicio_imagen4: serviUrl4,
        Servicio_imagen5: serviUrl5,
        Servicio_imagen6: serviUrl6,
        Servicio_imagen7: serviUrl7,
        Servicio_imagen8: serviUrl8,
        Servicio_text1: serviciostext1,
        Servicio_text2: serviciostext2,
        Servicio_text3: serviciostext3,
        Servicio_text4: serviciostext4,
        Servicio_text5: serviciostext5,
        Servicio_text6: serviciostext6,
        Servicio_text7: serviciostext7,
        Servicio_text8: serviciostext8
    })
    .then(function() {
        console.log("Document successfully updated!");
    })
    .catch(function(error) {
        // The document probably doesn't exist.
        console.error("Error updating document: ", error);
    });
});

$('#btn-productos').click(function(){
    var editar_pro = db.collection("productos").doc("BOQwIqslUlCSW5HfHBG6");

    var pro_img1 = document.getElementById('productos-bg1').files[0];
    var pro_img2 = document.getElementById('productos-bg2').files[0];
    var pro_img3 = document.getElementById('productos-bg3').files[0];

    var pro_img1_new = document.getElementById('productos-in1').value;
    var pro_img2_new = document.getElementById('productos-in2').value;
    var pro_img3_new = document.getElementById('productos-in3').value;

    if(pro_img1 == undefined){
        var pro1 = pro_img1_new;
        var proUrl1 = pro1;
    }else{
        var pro1 = pro_img1;
        var proName1 = pro1.name;
        var proUrl1 = "http://localhost/sistema-universalsoft/img/"+proName1;
    }

    if(pro_img2 == undefined){
        var pro2 = pro_img2_new;
        var proUrl2 = pro2;
    }else{
        var pro2 = pro_img2;
        var proName2 = pro2.name;
        var proUrl2 = "http://localhost/sistema-universalsoft/img/"+proName2;
    }

    if(pro_img3 == undefined){
        var pro3 = pro_img3_new;
        var proUrl3 = pro3;
    }else{
        var pro3 = pro_img3;
        var proName3 = pro3.name;
        var proUrl3 = "http://localhost/sistema-universalsoft/img/"+proName3;
    }

    var pro_title1 = document.getElementById('producto-titulo1').value;
    var pro_title2 = document.getElementById('producto-titulo2').value;
    var pro_title3 = document.getElementById('producto-titulo3').value;

    var pro_subtitle1 = document.getElementById('producto-subtitulo1').value;
    var pro_subtitle2 = document.getElementById('producto-subtitulo2').value;
    var pro_subtitle3 = document.getElementById('producto-subtitulo3').value;

    var pro_content1 = document.getElementById('producto-textarea1').value;
    var pro_content2 = document.getElementById('producto-textarea2').value;
    var pro_content3 = document.getElementById('producto-textarea3').value;

    return editar_pro.update({
        Proimg1: proUrl1,
        Proimg2: proUrl2,
        Proimg3: proUrl3,
        ProTitle1: pro_title1,
        ProTitle2: pro_title2,
        ProTitle3: pro_title3,
        ProSubtitle1: pro_subtitle1,
        ProSubtitle2: pro_subtitle2,
        ProSubtitle3: pro_subtitle3,
        ProContent1: pro_content1,
        ProContent2: pro_content2,
        ProContent3: pro_content3
    })
    .then(function() {
        console.log("Document successfully updated!");
    })
    .catch(function(error) {
        // The document probably doesn't exist.
        console.error("Error updating document: ", error);
    });
});

$('#btn-news').click(function(){
    
    var nueva_noticia = document.getElementById("noticiaFile").files[0];

    var uploadTask = storageRef.child('imagenes/' + nueva_noticia.name).put(nueva_noticia);
    
    var new_title = document.getElementById('new-title').value;
    var new_content = document.getElementById('new-content').value;
    var new_fecha = document.getElementById('new-date').value;
    var facebook = document.getElementById('new-link1').value;
    var instagram = document.getElementById('new-link2').value;
   
    db.collection("noticias").add({
        New_title: new_title,
        New_content: new_content,
        New_fecha: new_fecha,
        Facebook: facebook,
        Instagram: instagram,
        Imagen: nueva_noticia.name
    })
    .then(function(docRef) {
        console.log("Document written with ID: ", docRef.id);

        document.getElementById('new-title').value = '';
        document.getElementById('new-content').value = '';
        document.getElementById('new-date').value = '';
        document.getElementById('new-link1').value = '';
        document.getElementById('new-link2').value = '';

        Swal.fire({
            icon: 'success',
            title: 'Tu noticia Fue creada',
            showConfirmButton: false,
            timer: 2500
          });
    })
    .catch(function(error) {
        console.error("Error adding document: ", error);
    });
});

$('#btn-footer').click(function(){
    var editar_footer = db.collection("footers").doc("JCn29CYMjX236zdKslyN");

    var footer_title = document.getElementById('footer_titulo').value;
    var footer_facebook = document.getElementById('footer_link1').value;
    var footer_instagram = document.getElementById('footer_link2').value;
    var footer_linkedin = document.getElementById('footer_link3').value;
    var footer_issuu = document.getElementById('footer_link4').value;

    return editar_footer.update({
        title: footer_title,
        facebook: footer_facebook,
        instagram : footer_instagram,
        linkedin : footer_linkedin,
        issuu : footer_issuu
    })
    .then(function() {
        console.log("Document successfully updated!");
    })
    .catch(function(error) {
        // The document probably doesn't exist.
        console.error("Error updating document: ", error);
    });
});

// borrar datos
$('#btn-video-delete').click(function(){
    var cityRef = db.collection("video_banner").doc("Zo1rmSVJqT5Sn5DHOX2S");
    // Remove the 'capital' field from the document
    var remove = cityRef.update({
        Video: '',
    });
})

$('#btn-nosotros-delete').click(function(){
    var cityRef = db.collection('nosotros').doc('OO6KBes70eADfuCezTLd');
    // Remove the 'capital' field from the document
    var remove = cityRef.update({
        Imagen: '',
        Titulo: '',
        Contenido: ''
    });
})

$('#btn-servicios-delete').click(function(){
    var cityRef = db.collection('servicios').doc('0mkl0IltfpLuLFWpIsrh');
    // Remove the 'capital' field from the document
    var remove = cityRef.update({
        Titulo: titulo,
        Servicio_imagen1: '',
        Servicio_imagen2: '',
        Servicio_imagen3: '',
        Servicio_imagen4: '',
        Servicio_imagen5: '',
        Servicio_imagen6: '',
        Servicio_imagen7: '',
        Servicio_imagen8: '',
        Servicio_text1: '',
        Servicio_text2: '',
        Servicio_text3: '',
        Servicio_text4: '',
        Servicio_text5: '',
        Servicio_text6: '',
        Servicio_text7: '',
        Servicio_text8: ''
    });
})

$('#btn-footer-delete').click(function(){
    var cityRef = db.collection("footers").doc("JCn29CYMjX236zdKslyN");
    // Remove the 'capital' field from the document
    var remove = cityRef.update({
        title: '',
        facebook: '',
        instagram : '',
        linkedin : '',
        issuu : ''
    });
})