(function($) {
  "use strict"; // Start of use strict

  // Toggle the side navigation
  $("#sidebarToggle, #sidebarToggleTop").on('click', function(e) {
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
    if ($(".sidebar").hasClass("toggled")) {
      $('.sidebar .collapse').collapse('hide');
    };
  });

  // Close any open menu accordions when window is resized below 768px
  $(window).resize(function() {
    if ($(window).width() < 768) {
      $('.sidebar .collapse').collapse('hide');
    };
    
    // Toggle the side navigation when window is resized below 480px
    if ($(window).width() < 480 && !$(".sidebar").hasClass("toggled")) {
      $("body").addClass("sidebar-toggled");
      $(".sidebar").addClass("toggled");
      $('.sidebar .collapse').collapse('hide');
    };
  });

  // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
  $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function(e) {
    if ($(window).width() > 768) {
      var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;
      this.scrollTop += (delta < 0 ? 1 : -1) * 30;
      e.preventDefault();
    }
  });

  // Scroll to top button appear
  $(document).on('scroll', function() {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });

  $(document).on('scroll', function() {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 200) {
      $('.topbar').addClass("navarfixe");
      $(".code-bg").addClass("code-fixe");
    } else {
      $('.topbar').removeClass("navarfixe");
      $(".code-bg").removeClass("code-fixe");
    }
  });

  // Smooth scrolling using jQuery easing
  $(document).on('click', 'a.scroll-to-top', function(e) {
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: ($($anchor.attr('href')).offset().top)
    }, 1000, 'easeInOutExpo');
    e.preventDefault();
  });

   // Smooth scrolling using jQuery easing
   $("#inputDicc").on('change', function(e) {
    var input =  $("#inputDicc option:selected").val()
    var valor = "#"+input;
    $('html, body').stop().animate({
      scrollTop: ($(valor).offset().top-200)
    }, 1000, 'easeInOutExpo');
    e.preventDefault();
  });
  
  $(document).on('scroll', function() {
    console.log("aca ", window.scrollY);
    var scrollVertical = window.scrollY
    
    if(100 >=scrollVertical){
      scrollVertical = 1;
    }else if(900 >= scrollVertical && 800 < scrollVertical){
      scrollVertical = 2;
    }else if(1720 >= scrollVertical && 1620 < scrollVertical){
      scrollVertical = 3;
    }else if (2560 >= scrollVertical && 2460 < scrollVertical){
      scrollVertical = 4;
    }else if (3100 >= scrollVertical && 3000 < scrollVertical){
      scrollVertical = 5;
    }else if (3500 >= scrollVertical && 3400 < scrollVertical){
      scrollVertical = 6;
    }else if (3900 >= scrollVertical && 3800 < scrollVertical){
      scrollVertical = 7;
    }else if (4600 >= scrollVertical && 4500 < scrollVertical){
      scrollVertical = 8;
    }else if (5100 >= scrollVertical && 5000 < scrollVertical){
      scrollVertical = 9;
    }else if (5900 >= scrollVertical && 5800 < scrollVertical){
      scrollVertical = 10;
    }else if (6400 >= scrollVertical && 6300 < scrollVertical){
      scrollVertical = 11;
    }else if (6900 >= scrollVertical && 6800 < scrollVertical){
      scrollVertical = 12;
    }else if (7500 >= scrollVertical && 7400 < scrollVertical){
      scrollVertical = 13;
    }else if (8300 >= scrollVertical && 8200 < scrollVertical){
      scrollVertical = 14;
    }
    switch (scrollVertical) {
      case 1:
        $(".code-div").hide();
        $("#code1").show();
        break;
      case 2:
        $(".code-div").hide();
        $("#code2").show();
        break;
      case 3:
        $(".code-div").hide();
        $("#code3").show();
      break;
      case 4:
        $(".code-div").hide()
        $("#code4").show()
      break;
      case 5:
        console.log("case 2560")
        $(".code-div").hide()
        $("#code5").show()
      break;
      case 6:
        console.log("case 2560")
        $(".code-div").hide()
        $("#code6").show()
      break;
      case 7:
        console.log("case 2560")
        $(".code-div").hide()
        $("#code7").show()
      break;
      case 8:
        console.log("case 2560")
        $(".code-div").hide()
        $("#code8").show()
      break;
      case 9:
        console.log("case 2560")
        $(".code-div").hide()
        $("#code9").show()
      break;
      case 10:
        console.log("case 2560")
        $(".code-div").hide()
        $("#code10").show()
      break;
      case 11:
        console.log("case 2560")
        $(".code-div").hide()
        $("#code11").show()
      break;
      case 12:
        console.log("case 2560")
        $(".code-div").hide()
        $("#code12").show()
      break;
      case 13:
        console.log("case 2560")
        $(".code-div").hide()
        $("#code13").show()
      break;
      case 14:
        console.log("case 2560")
        $(".code-div").hide()
        $("#code14").show()
      break;
    }
  });

  let animationEvent = 'animationend';

  $(".item-list-btn").click(function(){
    $(".ocultar").addClass("desocultar");
    $(".ocultar").css("opacity","1");
    $(".ocultar").css("visibility","visible");
    $(".ocultar").one(animationEvent, function(event) {
      $(".ocultar").removeClass("desocultar");
    });
  });

})(jQuery); // End of use strict
