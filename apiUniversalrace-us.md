## Acciones Preliminares
_First and foremost you must provide the IP address of the origin server
Example_

```

UniversalDomain=https://api.universalrace.net/
IP=xxx.xxx.xxx.xxx 
```
### Check available currencies and get the id of each currency
```
POST	https://[universalDomain]/bet_api.php
Body Params: {
 accion:'monedas_API'
}

Response:
{ 
 id:numeirc,  --> required to create the user
 abre:string,  --> Currency abbreviation
 moneda:string, --> name or description of the coin
 pais, -->  country to whom the currency belongs
 simbolo, --> currency symbol or identifier
 cod_pais --> country code
}

```
### Create User
_It is necessary to create the user, before performing any operation_
```
POST	https://[universalDomain]/bet_api.php
Body Params: {
 accion:'API_crear_usuario',
 user_n:string(20),
 nombre: string(20),
 pais: int (international phone number, ejm 58 venezuela,51 Peru)
 moneda: integer
 id:string(50)  (It is a unique number or password for each user, it must contain at least 15 alphanumeric characters )
}

//User was created
Response:
{ 
 resp:'ok', 
 id:numeirc,  
 Msg:'User Created'
}

//If the User already exists
{ 
 resp:'Err', 
 id:integer,  (1=User exite) (3=Ip No registered)
 Msg: string ("User already exists")(Ip no Found)
}
```

### Log in
```
POST	https://[universalDomain]/ betapi/bet_api.php
Body Params: {
 accion:'API_Login',
 user_n:string(20),
 id:string(50)   (Id or password with which I create the user)
}
Response:
Respuesta Afirmativa
{
  resp:'OK',
  token: string,
}
//Open link:

token= is the token provided at login
saldo= is the current balance of the customer
*** The link must open inside an iframe otherwise it will show a blank page
https://universalrace.net/new/cliente_api/a_home.php?tk={token}&ba={saldo}


//Make Play

//When the player makes a move, a request is made to the (cliente_Api) 

POST	https://[yourserver_ip]/[you_end_point]

Body Params	{
 accion:' venta_tck_API',
 token:string,
 id_tran: string,
 monto: float
}
Response:
Negativa (The player has no balance)
{
resp:'fo',
id:'fo', 
Mgs:string (*Reason for rejection)
}

//Afirmativa
{
resp:'ok',
balance:float, //The current balance of the client
saldo:float,   //The Balance you had before making the move
idtx: string  //Unique identifier or Id of the process by the API client for its control, this Value will be returned to the api client so that the process ends * OPTIONAL
}
//When the answer is affirmative, the move made is saved and the Api client is informed of the completion of the process

POST	https://[yourserver_ip]/[you_end_point]

Body Params	{
 accion:'final_tck_API',
 serial:string,
 monto:float 
 id_tran: string,
 hora: format unix o epoch
 id_us=numeric
 idtx:string (the idtx provided by the client) 
}

Response:
{
resp:'ok',
serial:string, 
idtx:string,  
id_us: numeric
}
```

### Notification Change of status Tickets
// 1.0 Notification is sent to the End Point that you have new tickets

GET	https://[yourserver_ip]/[you_end_point]?accion=status_tck&id=XXXXXXXXXXXXX

// 1.1 The Api client must make a Request to the server

```
POST	https://[universalDomain]/ betapi/bet_api.php
Body Params: {
 accion:"status_tck_API",
 id_operacion:id   (The id that was sent in the step 1.0 )
}
Response:

Affirmative answer
{
  resp:'OK',
  data:[ 
        {
            id: Numeric,
            id_us:numeric,  (Id User UniversalHorse)
            monto:float,     
            estatus: Character ("G" Winner, "D" Return Play, "A" Void),
            fecha:numeric (Format Unix o Epoch),
            serial:string (Serial the ticket) 
        }
      ]
}

// 1.2 Api client confirms receipt 

```
POST	https://[universalDomain]/ betapi/bet_api.php
Body Params: {
 accion:"confirma_status_API",
 id_operacion:id   (The id that was sent in the step 1.0 )
}

Response:
{ 
 resp:'ok', 
 id:numeirc,  
 Msg:'Confirmado'
}


### See detail of a Ticket

```
POST	https://[universalDomain]/betapi/bet_api.php
Body Params: {
 accion:"ver_ticket_API",
 serial:string   
}

Response:
{ 
 resp:'ok', 
 serial:string,  
 total:float
 moneda:int
 tck:[{
     apuesta:character, ("w" win, "p" place,"s" show,"ex" exacta,"tr" trifecta,"su" superfecta,"Pk2" Pick2, "Pk3" Pick3
     pista:string,
     monto:float,
     tipo_estatus: chracter ("G" Winner, "P" Lost, "D" Return, "A" Void, "J" In Play)
     dividendo:float ("If it is an SP bet, or Fixed Dividend),
     carrera:integer,
     caballo: string (Horse or Horses involved in the bet)
     }]   
}

### Types of modes available

```
POST	https://[universalDomain]/betapi/bet_api.php
Body Params: {
 accion:"monedas_API"
}

Response:
[{ 
 id:integer,  Coin id
 abre:string,  Abbreviation of modena
 moneda:string, Description of the type of currency
 simbolo:string  Symbol representing the currency
 pais:string  Country abbreviation
 cod_pais:numeric cod country according to the international telephone code
}]

### Additional Information About Betting

* The Api client is the one who validates whether or not the player has the balance to make the play.

* The consultation and completion of the play is done in two steps:
* 1.- A call is made to the client's endpoint with the amount of the play to validate if it has enough balance for the play.
* 2.- If so, the tck is saved and another request is sent to the end point to confirm that the tck was generated.

```
//Respuesta 
//The response time by the client must be less than 1.5 seconds, otherwise the play is canceled
//It must return whether or not the client has a balance to execute or complete the play.
//The validation process of whether the client who makes the play has a balance or not, is left to the client_api

//Solicitud POST al end point del cliente
accion = "venta_tck_API"   //Action to be executed
id_tran =  "XXXXXXXXXXX"   //Unique ID of the UniversalHorse
monto =  0000000000        //Total amount of the play

//Responses to be provided by the client_API

//Affirmative response json (The client has a balance, The API client should have already discounted the balance)

"resp":"ok"               //The client has a balance to make the move
"balance":""              //The current balance of the client
"saldo_ant":""            //The Balance you had before making the move
"idtx":"alkks87238493"    //Unique identifier or ID of the process by the API client for its control, this Value will be returned to the api client to complete the process * OPTIONAL

// When affirmative, the tck is saved and the confirmation response is sent to the client by API

// POST request to customer end point 

accion="final_tck_API" //The action being taken or worked
serial = "XXXXXXXXX"  //The unique Serial of each tck
monto=0000000.00      //total tck
id_tran="XXXXX"       //Transaction id or UniversalHorse id_tck sent in the previous query
hora = 1234567899     // The time in UNIX or epoch format
id_us = 0000000       //The Id of the user registered in UniversalHorse
idtx = "XXXXXXXXX"    //The ID sent by the API client for its control

//JSON Confirmation Response
"resp":"ok"              
"serial":""              
"idtx":""            
"id_us":""

//Negative Response or No json balance
"resp":"fo"                 //fo  the customer does not have enough balance
"Id":"fo"                   //The id or identifier of the error is fo
"Msg":"Saldo Insuficiente"  //It is optional, you can send any text  
//As the answer is negative, the tck is not saved and the play is canceled, no confirmation is sent
```
